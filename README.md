# Thesis

[![made-with-latex](https://img.shields.io/badge/Made%20with-LaTeX-1f425f.svg)](https://www.latex-project.org/)
[![Build Status](https://gitlab.com/jbkze/bachelorarbeit/badges/master/pipeline.svg)](https://gitlab.com/jbkze/bachelorarbeit/-/jobs/artifacts/master/file/thesis.pdf?job=build)

You can download the latest pdf version of my thesis [here](https://gitlab.com/jbkze/bachelorarbeit/-/jobs/artifacts/master/raw/thesis.pdf?job=build).