\chapter{Introduction}
\label{ch:intro}


These days, digital services play a major role in nearly all areas of life. We entrust companies with sensitive data and even conduct banking transactions over the Internet. It is therefore not surprising that attacks on computer networks are becoming increasingly sophisticated and are racing head-to-head with countermeasures being devised in industry and research. To date, the security measures in most companies involve filtering possible attacks with the help of rules defined by security analysts. However, this approach begins to reach its limits. According to the German Federal Office for Information Security, in 2020 over 300,000 new malware variants were discovered daily in Germany alone \cite{BSI2020}. Given this amount of emerging threats, it seems like a Sisyphean task to constantly respond with new rule definitions. For this reason, the application of anomaly detection in network security is becoming more and more important. Anomaly detection does not seek known attacks, but patterns that deviate from usual behavior.


\section{Motivation}
\label{sec:intro:motivation}
%\graffito{Note: The content of this chapter is just some dummy text. It is not a real language.}

Considering the potential use of anomaly detection techniques in network security, it is remarkable that systems using this approach have hardly been adopted in the industry so far \cite{Sommer2010Jan}. This highlights the importance of research and industry going hand in hand. For this reason, the Darmstadt University of Applied Sciences and the network specialist company DICOS GmbH \cite{DICOS2021} have joined forces in the ANEMO (Automatisiertes Netzwerkmonitoring) research project. It is funded by the LOEWE3 \cite{LOEWE2021} program of the Hessen Agentur GmbH. This thesis is part of the ANEMO project.


\section{Goal of the Thesis}
\label{sec:intro:goal}

The research effort in the field of network anomaly
detection focuses primarily on machine learning approaches. Different algorithms have been compared with each other many times regarding their detection performance using artificial benchmark data sets.

However, the significance of these studies is limited as these benchmark data sets typically have little in common with reality and are often outdated \cite{Sommer2010Jan}. Besides, to ensure the comparability of the results, labeled data is usually used, i.e. data where it is known for each data point whether it is normal or not. In practice, however, such labeled data is not available for enterprise networks. There is no practical way to test the performance of these algorithms in real network environments.

Therefore, this thesis attempts to design a framework that allows machine learning algorithms to be tested and compared in real network environments for their ability to detect anomalies in network traffic data.


\section{Thesis organization}
\label{sec:intro:structure}
Chapter \ref{ch:foundation} provides necessary background information and reflects on related work. Besides an introduction to the topic of network traffic, the term \textit{anomaly} is discussed as well as challenges and approaches in machine learning. In Chapter \ref{ch:requirement_analysis} we collect requirements for our system by first taking a high-level look at what basic functions our system needs to provide before going into more detail and gathering specific requirements. In Chapter \ref{ch:framework_design} we design our framework. In Section \ref{sec:framework_design:general_concept}, this is done on a general level without committing to specific technologies, so that the design retains its value in the future. In Section \ref{sec:framework_design:technology_choices}, we then select suitable state-of-the-art technologies for critical components. In Chapter \ref{ch:evaluation}, we evaluate our design by presenting a proof-of-concept implementation, deploying it in a real network and demonstrating its operation in an experiment. Chapter \ref{ch:conclusion} concludes and gives an outlook on future work.
