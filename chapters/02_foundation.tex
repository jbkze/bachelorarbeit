\chapter{Foundation}
\label{ch:foundation}


\section{Anomaly}
\label{sec:foundation:anomaly}
While there is no exact definition for the term \textit{anomaly} it is commonly used to describe patterns that do not conform to normal or expected behavior \cite{Ahmed2016Jan}.  Anomalies are also referred to as outliers, novelties, deviants, or discords and can occur in a variety of application domains, such as medicine, physics, or economics. Anomalies are assumed to be both rare and different compared to the rest of the data \cite{Foorthuis2020Jul}. They share this characteristic with noise. However, the difference is that anomalies, unlike noise, are of interest to the analyst a priori \cite{Chandola2009Jul}. Whether an instance qualifies as an anomaly may also depend on the dimensionality of the data. For example, in a univariate feature space, a person with a height of 170cm would usually not be considered anomalous. However, if age is added as an additional dimension, the data point may in fact be an anomaly, for example, if the person is only 6 years old. Due to the large number of application domains and correspondingly different appearing anomalies, a categorization is useful.  While Foorthuis identified over 70 anomaly types and subtypes, in the following we concentrate on the three most common kinds of anomalies \cite{Ahmed2016Jan, Fernandes2019Mar}. Figure \ref{fig:anomalies} illustrates two clusters, annotated as N1 and N2. Given their bunched occurrence, one would assume in the context of anomaly detection that these represent the normal instances. In the following, the three types of anomalies are explained using Figures \ref{fig:anomalies} and \ref{fig:contextual_anomaly}.

\begin{description}
  \item[Point anomalies] are particular instances that deviate from the normal pattern of the dataset in regards to their features \cite{Ahmed2016Jan}. In the network domain this could be a network packet with an unusually big payload for example. Two point anomalies are illustrated in Figure \ref{fig:anomalies} by points P1 and P2. When looking at the figure, it becomes apparent why point anomalies are considered to be less challenging to detect. They occur isolated in the feature space and are thus easier to separate \cite{Foorthuis2020Jul, Fernandes2019Mar}.
  \item[Collective Anomalies] occur when multiple instances, while not necessarily a point anomaly on their own, appear anomalous in their combination \cite{Fernandes2019Mar}. A collective anomaly can be seen in Figure \ref{fig:anomalies} annotated as C1. It is not always obvious whether an accumulation of instances represents a new normal cluster or actually a collective anomaly. An example from the network domain would be a flood of ordinary requests that could constitute an attack.
  \item[Contextual Anomalies] are instances that are anomalous only in a specific context \cite{Ahmed2016Jan}. In Figure \ref{fig:contextual_anomaly} a univariate time series is illustrated. When looking only at day one, the outlier annotated as A1 would most certainly be classified as a point anomaly. However, when we add the context, i.e. the rest of the week, we notice that the same outlier occurs at 12PM on all days with the exception of day 4. From this perspective, one would now presumably regard point A2, which appears normal when viewed in isolation, as an anomaly.
\end{description}


\begin{figure*}[htb]
  \centering
	\includegraphics[width=\linewidth]{images/anomalies.pdf}
	\caption{Examples for different types of anomalies in a bivariate feature space.}
	\label{fig:anomalies}
\end{figure*}

\begin{figure*}[htb!]
  \centering
	\includegraphics[width=\linewidth]{images/contextual_anomaly.pdf}
	\caption{Example for a supposed point anomaly (A1) and a contextual anomaly (A2) in a univariate feature space.}
	\label{fig:contextual_anomaly}
\end{figure*}


\section{Network Fundamentals}
\label{sec:foundation:network_basics}
A computer network can be defined as "a distributed system consisting of loosely coupled computers and other devices" \cite{Kizza2020Jun}. Nowadays, there is a wide range of network-enabled devices, such as smartphones, notebooks, or servers.

As with any successful communication, communication in computer networks requires at least two entities that want to share a message and some medium through which the message can be transported. Some rules, so-called protocols, must be followed to enable the involved network elements to communicate with each other  \cite{Kizza2020Jun}. This section covers the fundamentals of networking to help understand the challenges in anomaly detection inherent to this domain.

\subsection{Traffic}
\label{sec:foundation:network_basics:traffic}
The amount of data moving across a network at a given point in time is described as network traffic. Each message is fragmented into packets as it enters the network and reassembled into a message as it leaves. Every packet comprises several layers and is structured following the TCP/IP reference model. The TCP/IP model has been developed since the 1970s and is the basis technology of the Internet today. Figure \ref{fig:tcp_ip} shows the four layers of the TCP/IP model \cite{Baun2019}:

\begin{description}
  \item [Application Layer] This is the top layer of the TCP/IP model and serves as an interface to the end user. Its main purpose is to take data from the applications and hand it down to the underlying layers or collect data from these layers and deliver it to the correct applications \cite{Alani2014}.
  \item [Transport Layer] The Transport Layer is responsible for the end-to-end connection and, depending on the protocol, helps ensure that packets arrive at their destination error-free and in the correct order.
  \item [Internet Layer] The Internet Layer adds address information to the data passed from the Transport Layer and selects the best path for the data to travel through the internet. This process if called routing \cite{Alani2014}.
  \item [Link Layer] The Link Layer defines how the data is sent physically through the network. Here, the logical IP addresses of the transport layer are mapped to the physical addresses of the end devices, the so-called MAC addresses.
\end{description}

\begin{figure*}[htb]
  \centering
	\includegraphics[width=\linewidth]{images/osi_colored.pdf}
	\caption{TCP/IP Reference Model \cite{Baun2019}.}
	\label{fig:tcp_ip}
\end{figure*}

When a packet enters the network, each of these layers adds certain information to the actual message. Details of what this information is are defined in the network protocols, each of which are located at one of the four layers in the TCP/IP reference model. A schematic illustration of this process is shown in Figure \ref{fig:datagram}.

\begin{figure*}[htb]
  \centering
	\includegraphics[width=\linewidth]{images/datagram_colored.pdf}
	\caption{The four layers comprising a packet \cite{Baun2019}.}
	\label{fig:datagram}
\end{figure*}

\renewcommand{\arraystretch}{1.2}
\setlength{\tabcolsep}{10pt}

There are hundreds, if not thousands, of different network protocols. While there are some established standards (see Table \ref{tab:protocols}), many of them are also rather exotic, often developed and used only by one particular manufacturer. As a result, the network traffic looks highly heterogeneous. Packet headers contain different fields and the usual size and frequency of the packets differs greatly between protocols. This presents security analysts with the problem that, in theory, one and the same kind of attack or incident can appear quite differently due to the protocol used. Furthermore, older protocols dating back to the time when the Internet was created. Many of them no longer meet today's security standards and pose a particular threat today \cite{Rossow2014Jan}.

\begin{table}[htb]
\centering
\scalebox{1.0}{
\begin{tabular}{ll}
\hline
\textbf{Layer} & \textbf{Protocols}   \\ \hline
Application    & HTTP, FTP, DNS, POP3 \\
Transport      & TCP, UDP             \\
Internet       & IP, ICMP, ARP        \\
Link           & Ethernet, WiFi (IEEE 802.11)       \\ \hline
\end{tabular}
}
\caption{Prominent network protocols}
\label{tab:protocols}
\end{table}

Another factor that makes the analysis of network traffic more challenging compared to other domains is its dynamic nature. Even within a single network basic characteristics such as bandwidth or the event duration can vary significantly. The difficulty is that these variations can certainly be considered normal, which is unusual for other time series data such as sensor readings. One way to address this problem is to aggregate the data, i.e. to observe longer time intervals over which certain metrics can level out \cite{Sommer2010Jan}.


\subsection{Anomalies in network traffic}
\label{sec:foundation:network_basics:anomalies}
The general description of anomalies from Section \ref{fig:anomalies} also applies to anomalies in networks. The issue with anomalies in network data, however, is that many events that are actually anomalous according to the definition "rare compared to the rest of the data" are not of interest to the analyst since they result from legitimate activities. As a result, an anomaly detection system runs the risk of generating a large number of false positives, i.e. classifying many instances as anomalies that the analyst does not consider to be a real threat. The potential consequences of network anomalies can be either performance or security related \cite{Bhuyan2014}. When network anomalies are categorized by causal aspects rather than by their nature as in Section \ref{fig:anomalies}, four categories emerge \cite{Fernandes2019Mar}:

\begin{description}
  \item[Operational Events] are non-malicious incidents that can occur during operational use. These include technical faults, the transfer of unusually large files, the addition of new network elements, or incorrect device configuration. This can result in abrupt changes in various metrics, such as bit rate or network load \cite{Fernandes2019Mar}.
  \item[Flash crowd] refers to a high number of requests for an Internet resource over a short period of time. The requests themselves are legitimate but in their accumulation they represent an anomaly. Therefore, they also qualify as a collective anomaly. A flash crowd can occur, for example, during a video conference meeting of all employees of a large company \cite{Fernandes2019Mar}. Flash crowds are sometimes hard to separate from similar appearing malicious incidents such as DDoS attacks \cite{Li2009Jan}.
  \item[Measurement Anomalies] result from erroneous data ingestion. Reasons for this could be the overload of the recording software, faulty data processing or inconsistencies in the storage solution.
  \item[Network attacks] aim to cause damage to a system. Attacks can threaten the confidentiality, integrity, and availability of computer networks \cite{Gogoi2011Apr}. These are the three main information security qualities as demanded e.g. by the German Information Security Law \cite{BSIGESETZ2021}. Confidentiality requires that sensitive data be accessible only to an authorized group of persons. Integrity means that data arrives correctly and completely and cannot be altered or damaged by an unauthorized third party. Availability relates to both information technology systems and the data processed in them and means that the systems are ready for operation at all times and that the data can be accessed as intended \cite{Bedner2010May}. There are passive and active ways to attack a network. A popular example of an active attack is the Distributed Denial of Services (DDoS) attack. This involves several network nodes sending a large number of requests to the target in order to exhaust its resources. Requests from legitimate participants can then no longer be answered. In this case, the availability of the system would suffer in particular. Recording or snooping on communications on a network would be a passive attack on the confidentiality of a system, called Eavesdropping \cite{Pawar2015May}. Fernandes et al. provide a comprehensive overview of the broad field of network attacks \cite{Fernandes2019Mar}.
\end{description}



\section{Anomaly Detection}
\label{sec:foundation:anomaly_detection}
Anomaly detection is the process of analyzing data to identify anomalies. Although outlier detection has a history dating back to the 18th century, it was not until the 1980s that the potential intrinsic value of anomalies was recognized. From then on, anomalies were sought not only with the intent of removing unwanted instances from data, but to gain insight from the anomalies themselves \cite{Foorthuis2020Jul}.


\subsection{Challenges}
\label{sec:foundation:anomaly_detection:challenges}
The principle behind anomaly detection is fairly simple. Fernandes et al. formulate the central question in the attempt to build a network anomaly detection system as follows: "How to create a precise idea of normality?" \cite{Fernandes2019Mar}. Once this idea of normality has been grasped, the degree of deviation from this known behavior can be measured. However, in reality and especially in the domain of network anomaly detection, a number of challenges arise in the effort to define the normal behavior of a computer network.

Key challenges are the heterogeneity and dynamics of networks, as mentioned in Section \ref{sec:foundation:network_basics:traffic}. These dynamics translate into three dimensions:

\begin{enumerate}
  \item \textbf{Inter-network dynamics:} The topologies, applications, protocols, user behaviors, and many other network characteristics can differ, sometimes massively, from network to network. The concept of normal behavior observed on one network can therefore hardly be transferred to other networks. Sommer et al. therefore state that "[...] conclusions drawn from analyzing a small environment cannot be generalized to settings of larger scale." \cite{Sommer2010Jan}.
  \item \textbf{Short-term dynamics:} Network traffic is subject to high volatility. Even in short time intervals, the metrics of a network can undergo fundamental variations. Especially with metrics such as bytes transferred per second, massive changes can occur depending on the content. Many common methods for anomaly detection in time series, which detect exactly such abrupt changes, can therefore hardly be used in this domain \cite{Ahmed2016Jan, Sommer2010Jan}.
  \item \textbf{Long-term dynamics:} The internet and its applications are evolving rapidly. The appearance of network traffic in a few years may already be fundamentally different from today's traffic. In addition, numerous, better masked attack vectors may emerge. As a result, on the one hand, the value of past research conducted on eventually outdated data is losing more and more of its significance and, on the other hand, existing solutions have to be constantly re-evaluated \cite{Fernandes2019Mar, Ahmed2016Jan}.
\end{enumerate}

Another challenge lies in the foundation of all analysis, the underlying data base. Researchers are facing two issues here:

\begin{enumerate}
  \item \textbf{Feature selection:} The decision about which features to use for training is crucial for the later detection performance of the model. In other domains where anomaly detection is applied, these features can be selected straightforward, for example, for credit card fraud detection \cite{Sommer2010Jan}. In the network domain, this is not as simple and is itself subject to research \cite{Iglesias2015Oct, Sarhan2020Nov}. A multitude of different metrics can be extracted from the network and enriched afterwards. For example, consider an IP address. Additional topology-related information such as the associated subnet or country can be added as a feature. There is also the question of how detailed the extracted features should be. For example, the TCP flags on a TCP connection can be valuable information, but all non-TCP connections do not have these features, reducing their value. So one has to find a trade-off between level of detail and generalizability.
  \item \textbf{The need for "perfect" training data:} Creating a good data base for training is extremely difficult. Depending on the type of machine learning you choose (see Section \ref{sec:foundation:anomaly_detection:approaches}), two problems arise: Firstly, in the case of supervised machine learning, you essentially have to include all possible anomalies, in sufficient quantity in the training data. Given the variety of possible anomalies, this is almost impossible to accomplish. Secondly however, in the case of unsupervised learning, the training data must include every possible "form" of normal behavior. If one does not do this, one later runs the risk of classifying unseen but normal behavior as anomalous. Witten et al. describe this latter problem as follows: "The idea of specifying only positive examples and adopting a standing assumption that the rest are negative is called the closed world assumption. [...] [The assumption] is not of much practical use in real-life problems because they rarely involve 'closed' worlds in which you can be certain that all cases are covered." \cite{Witten2011}. This challenge is rarely sufficiently addressed in research. The data's nature is often not fundamentally different in training and evaluation, resulting in no new form of normal behavior occurring. This could be one reason for the usually very low false positive rates in these studies \cite{Sommer2010Jan}.
\end{enumerate}

According to Sommer et al. one of the primary reasons for the low productive use of network anomaly detection systems to date is the high cost of errors. In this respect, the network domain is different from other domains \cite{Sommer2010Jan}. Consider a very popular application of machine learning, face recognition for verification purposes. False negatives can have severe security implications here, so it is advisable to set the decision threshold in a strict manner. The consequence of this can be that actually authorized users have to retry the verification attempt a second or even a third time. This may be annoying, but is usually not a problem. The situation is different for network anomaly detection. The consequences of a false negative can be substantial and cause high monetary damage \cite{Fernandes2019Mar}. Choosing the same approach here and setting the decision threshold accordingly high will again generate more false positives, only that these have significantly greater negative effects here. In a large network, millions of packets per minute can flow through the network. Even very low false positive rates generate an enormous number of false positives simply due to the sheer volume of data. These usually have to be dealt with by a network administrator, which is quite time-consuming. The benefit of such a system would therefore be rather counterproductive \cite{Sommer2010Jan}.

It is also difficult to interpret the results of network anomaly detection systems \cite{Sommer2010Jan}. Often, such systems are constrained to the mere identification of network anomalies, but lack classification. However, this information is of limited value to a network administrator, who is ultimately left wondering how best to respond to the anomaly \cite{Fernandes2019Mar}. This is especially important with respect to the variety of network anomalies described in Section \ref{sec:foundation:network_basics:anomalies}. Anomalies can occur for a variety of reasons, by far not all of which are of interest to a network administrator.



\subsection{Approaches}
\label{sec:foundation:anomaly_detection:approaches}

There are a variety of different machine learning approaches for anomaly detection that can be categorized according to many criteria. A basic distinction can be made based on the availability of labels. Labels are used to annotate to which class, i.e. anomaly or not, an instance belongs. We distinguish between three levels of supervision:

%Considering the long time that anomaly detection has been explored, it is not surprising that there are a number of different approaches available today.

%Aggrawal identifies several factors that come into play when choosing a anomaly detection approach: Level of supervision and the data representation \cite{Aggarwal2016Dec}. Beispiel wo ein Algorithmus gut ist, ein anderer aber nicht \cite{Aggarwal2016Dec}.

%According to Gogoi et al., key criteria for choosing an appropriate anomaly detection approach are the application domain, the type of data, and the availability of labeled data. "In such cases, data points may be incorrectly reported as outliers because of poor fit to the erroneous assumptions of the model." \cite{Gogoi2011Apr}.

\begin{description}
  \item [Supervised Anomaly Detection] requires labeled data including all possible classes during training. Supervised methods are usually very good at separating between classes. In addition, by manually labeling the instances, an expert can greatly influence on what he perceives as normal or anomalous. In application domains such as object recognition, supervised learning is the standard. However, in anomaly detection, the supervised learning approach introduces the so-called \textit{rare class problem}. Collecting normal instances is significantly easier than collecting anomalous ones, which inevitably leads to an unbalanced training set. For many algorithms, this poses a problem. Since it is unrealistic to include all possible anomalies in the training set, this approach is especially suitable for use cases where specific, well-defined incidents are sought \cite{Aggarwal2016Dec}.

  \item [Semi-Supervised Anomaly Detection] requires a training set consisting only of instances of one class. This approach can be used to address the mentioned rare class problem. Assuming one has labeled data but very few instances of anomalies, one could remove the anomalies from the data entirely and use only the normal instances for training. Theoretically, one could then detect every possible anomaly. However, this approach also requires labeled data, which is hard to obtain in the network domain \cite{Aggarwal2016Dec}.

  \item [Unsupervised Anomaly Detection] The advantage of this approach is that it does not require labels for training. Unsupervised machine learning algorithms, unlike semi-supervised algorithms, do not require the absence of anomalous instances in the training data. The underlying assumption is that new instances that are close to most other instances are potentially more normal than those far away from them. In general, this approach is well suited for detecting unknown anomalies, but it can also hold high error potential depending on the nature of the training data. Collective anomalies in particular can be mistakenly considered to be a group of normal instances due to their self-similarity, so that anomalies of the same type occurring later are mistaken for normal \cite{Bhattacharyya2013Apr}.
\end{description}


%Most anomaly detection methods make as underlying assumption that anomalousness is reflected in the distance to most other instances.

%Ahmed et al. provided a comprehensive survey of network anomaly detection techniques. They identified four main approaches for network anomaly detection (...).

%They also identified four main research challenges: A lack of universally applicable anomaly detection technique, noisy data which makes it harder to segregate anomalies, a lack of publicly available labeled datasets and the ever-changing nature of network traffic which may reduce the usefulness of older approaches. "Future research efforts are necessary for extensive quantitative evaluation with state-of-the-art network infrastructure." \cite{Ahmed2016Jan}

%In classical detection systems, a network expert provides information about certain known anomalies. If an anomaly occurs that has the learned characteristics, it can be detected. Since such a system can only detect threats that were known in advance, it is vulnerable to new attacks. In contrast, classification-based anomaly detection methods attempt to learn what normal behavior typically looks like so that deviations from this normal behavior can subsequently be detected. As a result, these systems are theoretically capable of detecting new attacks as long as they deviate enough from the baseline. However, if normal behavior occurs that was not seen during the training phase, it is incorrectly classified as an attack. Classification-based approaches include SVM, Bayesian Networks, and Neural Networks. Alle erklären? \cite{Ahmed2016Jan}

%\subsection{Output}
%\label{sec:foundation:anomaly_detection:output}


\subsubsection{Output}
\label{sec:foundation:anomaly_detection:output}
When a previously trained model is presented with new data, it reports its estimation of whether the data point is anomalous or not. This can be done in two ways, either by discrete labels or by continuous scores \cite{Aggarwal2016Dec}. There are arguments for both approaches. Often, the communication between an network administrator and an anomaly detection system works via so-called alerts. The administrator is thus notified when the system assumes to have found an anomaly. The system must therefore make a binary decision and assign data points to either the \textit{normal} or the \textit{anomaly} category. On the other hand, the transition between anomaly and normal instance is often fluid, so it is not necessarily possible to clearly determine the correct category. In such cases, scores that indicate the degree of anomalousness are useful \cite{Goldstein2016Apr}. Typically, hybrid methods are used, meaning that the models report the raw anomaly score and the data points are then labeled according to a predefined threshold. This preserves the higher information of the scores and at the same time provides a basis for decision-making \cite{Aggarwal2016Dec}. For example, when the scores are stored, an analyst has the possibility to examine only those instances with the highest scores within a period of time \cite{Bhattacharyya2013Apr}.



\section{Related Work}
\label{sec:foundation:related_work}

There has been substantial research in the area of anomaly detection in general and network anomaly detection in particular.

A good introduction to the topic of anomaly detection in network traffic is provided in the survey by Fernandes et al. They identify the strengths and weaknesses of statistical, clustering, classification, information theory, evolutionary computation, and hybrid methods based on numerous reviewed papers \cite{Fernandes2019Mar}. Our thesis provides a framework to evaluate promising algorithms in real networks. The work of Fernandes et al. can serve as an excellent source for the reader to find possible candidates to be integrated into our system.

A comprehensive survey on machine learning in networks has been provided by Boutaba et al. The authors extend their scope beyond anomaly detection and show how diverse the application areas for machine learning are in the network domain. For example, network operations can be supported by machine learning in resource management, congestion control, or traffic routing. They compare different approaches with respect to their suitability for these tasks and conclude that, despite promising results, it is questionable whether they remain useful in real networks \cite{Boutaba2018May}. This can serve as a source of inspiration for considerations in which areas our proposed system could be used with some modifications in the future.

There are numerous publications that address the problem of anomaly detection in network data. The results obtained in these works are almost always excellent. The detection performance is usually reported in terms of accuracy, i.e. the percentage of correctly classified data points. For example, in 2019 Ahmad et al. achieved an accuracy of over 92.00\% and in two cases even over 99\%, depending on the dataset. They used an k-NN classifier approach and did an extensive feature selection in advance \cite{Ahmad2019Jan}. In 2016, He et al. achieved an accuracy of 95.6\%. Their hypothesis was that depending on the network protocol, different features are relevant and a different machine learning algorithm is best suited \cite{He2016Mar}. In 2019, Jing et al. proposed a non-linear SVM approach achieving an accuracy of over 85\% \cite{Dishan2019Oct}. More approaches with their corresponding results are presented in \cite{Boutaba2018May}.

Despite the very promising results achieved over the last decades, anomaly detection based threat detection systems are still hardly used in production environments. Sommer et al. discuss possible reasons for this circumstance by comparing the network domain with other domains where machine learning is applied. Some of these reasons are discussed in Section \ref{sec:foundation:anomaly_detection:challenges}. Among other things, they found that research has focused almost exclusively on anomaly detection in synthetic or recorded traffic to date. While such experiments are essential to test and compare the theoretical anomaly detection performance of different approaches, the actual significance of these experiments is quite limited \cite{Sommer2010Jan}. That is, among other things, because the commonly used data sets such as the 1998 DARPA packet traces \cite{Lippmann1999Jan} or the 1999 KDD Cup data set \cite{KDD1999} are over 20 years old and thus cannot be considered up to date. The nature of the actual deployment sites of such systems, for example large corporate networks, has thus received surprisingly little attention in previous research. In 2019 Ring et al. presented a comparison of publicly available datasets used in research. 23 of 34 of the examined sets were recorded at small scale or university networks and only 4 of 34 at enterprise networks. Furthermore, in many cases only a few hours of traffic was recorded \cite{Ring2019Mar}. Sommer et al. have therefore emphasized that further experiments on artificial and non-representative data are of little interest to the research community \cite{Sommer2010Jan}.

In 2019, Zoppi et al. identified the need for dedicated support for experimental evaluation of machine learning approaches. They designed a system that can be extended by own implementations of machine learning algorithms \cite{Zoppi2019}. However, their system only allows evaluation using benchmark datasets. In this work, we take up the idea of experimental support and interchangeability of machine learning algorithms, but focus our attention on real network environments.

%For example, Zhao et al. achieved an average accuracy of 90.3\%. They worked with real network data captured in an university setting. However they assumed the captured traffic to be normal and did not inject any anomalies. Since their experiment did not require the algorithm to detect an anomaly their work is of limited value \cite{Zhao2015}.

%\begin{enumerate}
  %\item Altes Thema, viel beforscht; unglaublich viele Ansätze, Overview findet sich da und da.
  %\item Unglaubliche Ergebnisse erzielt (99\% Akkuratheit und mehr) \cite{Labayen2020Nov}
  %\item Trotzdem kaum echte System; woran liegt das? --> Sommer et al.
  %\item Dumme Datensets, künstlich und oder synthetisch; gibt zwar auch andere Möglichkeiten, aber Zoppi hat gezeigt, dass experimenteller Support benötigt wird. Ihr Support ist aber wieder nur für synthetische Daten
  %\item Deswegen sind wir ganz toll, weil wir sind echt.
%\end{enumerate}







%Welcher Fortschritt wurde hier insgesamt gemacht? Z.B. seit X Jahren auch produktiv in der Industrie eingesetzt?


%. Trotzdem kaum produktive System? Woran liegt das? --> Sommer et al.

%Zoppi et al. identified the need for dedicated support for experimental evaluation \cite{Zoppi2019}

%Other options to deal with the problem of missing meaningful public data sets (other than our approach of using live traffic): simulation and anonymization. Problem here: Realistic network traffic is difficult to simulate. \cite{Sommer2010Jan}.




%In 2005, Shon et al. presented a system that was able to outperform current security solutions at the time. Their approach was to combine the advantages of supervised and unsupervised learning by using two types of Support Vector Machines. The experiments were conducted using the 1999 DARPA set recorded at a military base, as well as on real data. Has shown that depending on what data is tested on, results can vary widely. Using packet features, not flows. Not capable of working with live traffic. "We assert  that our SVM framework is a   significant   contribution to anomaly  detection  and justifies deployment in real  world environments." \cite{Shon2005}

%In 2013, Thao et al. presented a system that is capable of working with live traffic using big data processing frameworks by Apache. They have identified the need to cope with large volumes of live traffic and store it for the future. "This proposed system design provides the ability to combine both  batch  and  real-time  processing". Real time data collection using Cisco NetFlow. Some basic processing steps (Drop bad flows). Only four features (Source/Destination IP and Port). Three algorithms: Naive Bayesian, SVM and Decision Tree. Results good but vary widely. "Future research  will  be  on  in-depth  analysis  of  anomaly  detection for  a  real-time  network  management  system,  enhancement of  machine  learning  and  optimization  algorithms  for  real-time  processing  and  high  accuracy,  and  implementation  of visualizing tools for comprehensive understanding of dynamic behaviors of complex networks."  \cite{Zhao2015}

%"Misuse IDS fails in detecting unknown and zero-day attacks where they are unavailable in the stored signatures.". "Anomaly-based    approaches    use    machine    learning    techniques to establish a normal profile usage. An anomalous request  will  be  considered  as  an  attack  if  it  violates  such  normal  profile.".  "The aim of this framework is to get the minimum number  of  features  that  achieve  the  highest  accuracy  under  best performance.". -> One way to target the curse of dimensionality ("Features selection reduces the dimensionality"). Uses UNSW-NB15, Cup '99 and NSL-KDD datasets. Four algorithms: Decision Tree, SVM, Naive Bayes and Logistic Regression. Unsupervised Learning is good: "A supervised  machine learning model that performs well with a particular dataset may not achieve a satisfactory performance with another." "The  proposed  framework  is  an  offline  process  to  decide  how to deal with online traffic for intrusion detection." Future Work: Extend Framework with more Machine Learning Algorithms. \cite{Anwer2018}

%In 2017, Katehareios et al. proposed a system working with live traffic and achieved promising results \cite{Kathareios2017}.

%In 2018, Zhu et al. presented a system that uses an Deep Learning approach. Evaluated with NSL-KDD dataset. Claim to achieve higher accuracy than traditional approaches as Random Forest, SVM and Naive Bayes. Use One-Hot encoding for categorical features. Use algorithmic scaling for duration, bytes/s  \cite{Mingyi2018}

%Proposes a new Benchmark formula to compare the efficiency and accuracy of real-time anomaly detection algorithms. Uses labeled, real-world time-seres data. Similar approach to ours: "Goal is to provide a standard, open source framework with which the research community can compare and evaluate different algorithms for detecting anomalies in streaming data. Not explicitly for network data.  \cite{Lavin2015Oct}
