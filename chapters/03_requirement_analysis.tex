\chapter{Requirement Analysis}
\label{ch:requirement_analysis}

As stated before, the goal of this thesis is to design a framework that can be used to test and compare machine learning algorithms in real network environments. To achieve this goal, we must first collect the requirements that our system must meet. When collecting requirements for complex tasks like this one, it can be helpful to structure the main problem into smaller sub-problems \cite{Catanio2006}. Therefore, we first define superordinate groups of requirements that are further refined later.

\section{Identification of requirement groups}
\label{sec:requirement_analysis:abstract_level}

In our search for the requirement groups, we orient ourselves to two generic anomaly detection frameworks. We focus on the frameworks proposed by Ahmed et al. \cite{Ahmed2016Jan} and Boutaba et al. \cite{Boutaba2018May}, respectively. They are illustrated in Figure \ref{fig:generic_frameworks}. There are a variety of other concepts of anomaly detection systems, but to the best of our knowledge no other approaches are as generic as those of Ahmed and Boutaba's. Like this thesis, they deal specifically with the network domain and are therefore well suited to provide initial insights for our requirement analysis.

Overall, the two frameworks are rather similar, although some differences can be noted. For example, in the case of Boutaba et al. it becomes apparent that the decision about the type of machine learning to be used (supervised or unsupervised) must already be made at a early stage as labels might need to be provided during data collection. Furthermore, there are differences in the control flow of both approaches. In the case of Ahmed et al., the process seems to run through linearly and to be completed with the evaluation, whereas in the case of Boutaba et al. it appears that after the evaluation - or validation as it is called here - the previous steps are fed back and the process starts anew. However, the common ground is that both approaches reveal that there are basically four tasks to consider when designing an anomaly detection framework:

\begin{enumerate}
  \item \textbf{Data Collection} takes care of network traffic capture, feature extraction, and persistent data storage.
  \item \textbf{Data Pre-processing} prepares the data for the machine learning process.
  \item \textbf{Model Learning} describes the training of a machine learning model based on a feature selection to represent the normal state of a network.
  \item \textbf{Evaluation} means reviewing the predictions of whether a data point is normal or anomalous made by the model after it has been presented with new data.
\end{enumerate}

Covering these tasks are thus the four fundamental functional requirements for our system and serve as a starting point for the more in-depth analysis in Section \ref{sec:requirement_analysis:concrete_level}. In addition, there are requirements that do not directly concern the functionality and apply to the system as a whole. They form the fifth requirement group, the \textbf{non-functional requirements}.

%\vspace{0.5cm}

%\begin{figure*}[htb!]
%  \centering
%	\includegraphics[width=\linewidth]{images/requirement_results}
%	\caption{Division of the goal of this thesis into smaller sub-problems.}
%	\label{fig:requirement_analysis_subdivision}
%\end{figure*}


%The term \textit{framework} might need some further explanation. Stanojevic et al. define a software framework as "a  skeleton  of  an  application,   that   includes   the   complete   code   for   the   basic   functions of a system, which can be conformed to the needs of one specific  application" \cite{Stanojevic2011}. This seems to be a appropriate approach for our system since it allows us to reuse much of our code. The biggest part of the application does not change, no matter which machine learning algorithm is used. Frameworks are useful in such situations since one has to implement the framework core that exists unchanged in every framework instance only once. Internal add-ins that vary from instance to instance can then be easily added \cite{Stanojevic2011}. In our case these add-ins are the machine learning algorithms.

\begin{figure}[bth]
  \myfloatalign
  \subfloat[Proposed by Ahmed et al. in \cite{Ahmed2016Jan}.]{
     \label{fig:generic_framework1}
     \includegraphics[width=.45\linewidth]{images/general_pipeline_ahmed.pdf}
   } \quad
   \subfloat[Proposed by Boutaba et al. in \cite{Boutaba2018May}.] {
     \label{fig:generic_framework2}
     \includegraphics[width=.45\linewidth]{images/general_pipeline_boutaba.jpg}
   } \\
   \caption{Two generic machine learning anomaly detection frameworks.}
   \label{fig:generic_frameworks}
\end{figure}

\section{In-depth requirements}
\label{sec:requirement_analysis:concrete_level}

Our system is intended to both adopt approaches of past work and fill gaps identified in those works. We will therefore investigate which characteristics comparable systems exhibit and where potential for improvement has been found. The in-depth requirements are presented in tables organized according to their higher-level requirement group.

\subsection*{Data Collection}
\label{sec:requirement_analysis:concrete_level:data_collection}

The underlying data base is one of the most important factors that determine success or failure of machine learning applications. Due to the diversity of possible deployment sites of our network anomaly detection system, we do not want to dictate the user specifically what the input data should look like, nor from which source this data should come. Instead, we want to be as agnostic as possible towards the input data.

%In past research there are mainly three sources of data: online ingestion \cite{Zhao2015, Shon2005}, offline ingestion using pre-recorded traffic \cite{Dromard2017} and the usage of existing datasets with the features already extracted \cite{Zoppi2019}.


%With regard to the storage solution, this means in particular that various interfaces must be provided for different data sources.
Nevertheless, wherever the data ultimately comes from, its source usually has to meet several requirements. First of all, this concerns the mode in which the ingestion runs. Two modes are conceivable here: On the one hand an online mode, where the data is extracted from the network in real time. On the other hand, an offline mode, where pre-recorded traffic is used. Since previous research has focused mainly on labeled benchmark data sets, there are comparatively few systems that can also handle live traffic. However, an online mode is indispensable, as the ultimate goal of all network anomaly detection attempts is to use them in real time monitoring tools \cite{Fernandes2019Mar}. Still, the use of recorded data can also be valuable since capturing live traffic in sufficient quantity for experiments is time consuming. In a situation where there is already a large amount of network recording available, it is convenient to be able to feed the recordings into the system. We therefore require our data collection solution to be able to collect live traffic in an online mode (\textbf{C1}) and to work with recorded traffic in an offline mode (\textbf{C2}).

When collecting data in online mode, the often very high bandwidths must be considered. Corporate networks with bandwidths of 100Gbps and more are no longer a rarity \cite{Fernandes2019Mar}. The data ingestion tool used must be able to cope with such bandwidths which also applies to the storage solution that must be able to handle the high data throughput produced by the ingestion tool (\textbf{C3}).

%The extraction of valuable features is another aspect to be considered in data collection. As explained in Section \ref{sec:foundation:anomaly_detection:challenges}, the choice of meaningful features is not trivial \cite{Bhattacharyya2013Apr}. There is no consensus in research on a standardized feature set. Again, depending on the deployment site, different features may be desirable. Therefore we want to ensure through the choice of our data source that the selection can be easily adapted and expanded by the user in the future (\textbf{R3}).

As mentioned in Section \ref{sec:foundation:network_basics:traffic}, one possible approach to deal with the dynamic nature of network traffic is by aggregating the data. According to Sommer et al., it is actually those systems that operate on highly aggregated data that are already in productive use \cite{Sommer2010Jan}. Therefore, our system should provide the ability to aggregate the input data (\textbf{C4}).\neuerabsatz
\noindent
%One concern that is raised when network data is collected excessively is privacy. Network data can be used to track down the behavior of specific users. Sommer et al. have actually identified privacy and security concerns as the main reason for why there are so few public data sets for network intrusion detection \cite{Sommer2010Jan}. In production, it is common practice that the collected data is anonymized either live during collection or afterwards, while retaining the information necessary for the intended purpose \cite{Kim2019Aug}. Since our goal is that our framework can be deployed not only in research facilities, but also in corporate networks in the long run, our system should also provide a way to anonymize the data, preferably with influence on exactly which features should be anonymized (\textbf{R6}).
A list of the in-depth requirements regarding the data collection, is given in Table \ref{tab:input_requirements}.

\begin{table}[!htb]
\centering
\begin{tabular}{cp{10cm}	}
\hline
\textbf{Name} & \multicolumn{1}{c}{\textbf{Requirement}}   \\ \hline
C1            & Possibility to record live traffic                     \\
C2            & Possibility to use pre-recorded traffic                    \\
%C3            & Allow user to customize feature set                \\
C3            & Ingestion and storage solution must handle high bandwidths                   \\
C4            & Possibility to aggregate data              \\  \hline
%C6            & Possibility to anonymize data                \\
%C8            & Storage solution must adapt to conditions of different networks. \\ \hline
\end{tabular}
\caption{Requirements for the data collection component.}
\label{tab:input_requirements}
\end{table}

\subsection*{Data Pre-processing}
\label{sec:requirement_analysis:concrete_level:data_processing}

Before the data can be used as input for machine learning algorithms, it usually has to undergo some form of preprocessing \cite{Ahmad2019Jan}. Remarkably, this is a step that is left out in many of the works we reviewed.

One issue that must be addressed is missing values \cite{Shopov2013Jan}. Due to the heterogeneity of network traffic, it can often happen that not all attributes are defined for each data point. For example, one might want to have a feature that provides information about the HTTP method used in a request. It is obvious that for requests that do not use the HTTP application protocol, this field will have no or an undefined value. However, many machine learning algorithms cannot handle null values in the data, so our system must provide the ability to handle missing values in the pre-processing stage (\textbf{P1}).

Another important aspect is coping with the sometimes very large value ranges of individual features. Many algorithms are prone to overfitting on features with significantly higher values \cite{Khah2018}. This is an issue that often occurs in the network domain. For example, the value of a feature that describes, say, the size of a packet in bits will probably be magnitudes larger than one that describes its time to live. Usually in such cases methods of scaling or normalization are applied, which therefore have to be integrated into our system (\textbf{P2}).

Most machine learning algorithms need numerical values as input. However, in the network domain there are a variety of categorical variables, such as IP or MAC addresses or network protocol names. These must first be converted to numeric values, which is called encoding \cite{Ahmed2016Jan}. Thus, our system must enable the encoding of the data (\textbf{P3}).

The encoding of categorical variables can increase the number of dimensions, i.e. features, dramatically which poses a new problem. High-dimensional data leads to higher computation times and potentially worse detection performance \cite{Shopov2013Jan}. This phenomenon is known as "the curse of dimensionality". Therefore our system must provide possibilities to reduce the number of features, i.e. perform dimensionality reduction (\textbf{P4}).


\begin{table}[!htb]
\centering
\begin{tabular}{cp{8cm}}
\hline
\textbf{Name} & \multicolumn{1}{c}{\textbf{Requirement}} \\ \hline
P1            & Handle missing values                    \\
P2            & Perform common types of scaling          \\
P3            & Perform common types of encoding         \\
P4            & Possibility to reduce dimensionality    \\ \hline
\end{tabular}
\caption{Requirements for the data processing component.}
\label{tab:processing_requirements}
\end{table}

\subsection*{Model learning}
\label{sec:requirement_analysis:concrete_level:model_learning}

Sommer et al. point out that it is a common pitfall to use a particular machine learning approach as a starting point for a network anomaly detection system \cite{Sommer2010Jan}. Therefore one core feature of our system is to provide the possibility to easily add new machine learning algorithms and to use them interchangeably (\textbf{M1}).

Our goal is to build a system that can be used in real network environments. From a machine learning perspective, this brings the challenge that we have to work without labeled data. Logically, we don't know which of the collected data points are anomalous and which are not. So our system has to cope with the unavailability of labeled data (\textbf{M2}).

Another point to consider when training the models is concept drifts in network traffic. Concept drift refers to the dynamics of network traffic explained in Section \ref{sec:foundation:anomaly_detection:challenges}: While the short-term dynamics probably need to be dealt with by the algorithm itself, our framework can help address the longer-term dynamics. For example, it can be assumed that in a corporate network the traffic behaves differently at the weekend than on a workday. Thus, our system should be able to help the user adapt to such concept drifts (\textbf{M3}).


\begin{table}[!htb]
\centering
\begin{tabular}{cp{8cm}}
\hline
\textbf{Name} & \multicolumn{1}{c}{\textbf{Requirement}}   \\ \hline
M1            & Possibility to easily add new algorithms  \\
M2            & Handle unavailability of labeled data     \\
M3            & Possibility to adapt to concept drifts                    \\\hline
\end{tabular}
\caption{Requirements for the model learning component.}
\label{tab:learning_requirements}
\end{table}

\subsection*{Evaluation}
\label{sec:requirement_analysis:concrete_level:evaluation}

As stated before, most anomaly detection systems work with benchmark data or in some cases data recorded at universities or similar. Accordingly, these systems are only able to evaluate historical data. However, with the same reasoning why we require the recording of live traffic, it is necessary that our system can also report the anomalousness of live data (\textbf{E1}).

During prediction the previously trained model is presented with new data. It is evident that this data needs to be pre-processed the same way as during training (\textbf{E2}).

After the model has scored the data, the user must have a way to evaluate the quality of the results. After all, one intended purpose of our system is to compare algorithms. Typically there will always be a analyst who manually evaluates the anomaly scores and decides whether the prediction was correct or not \cite{Laptev2015}. So our system must provide a suitable way for the user to analyze the results (\textbf{E3}).

%In the past, not enough effort has been made to provide the user of the system with sufficient insight into the nature of the data. The mere information where the system considers to have detected an anomaly is not enough \cite{Sommer2010Jan, Wang2019}.

%Providing intuitive insight into the data/features/anomaly scores is important \cite{Zhao2015}. In real life there will always be a analyst who evaluates the anomaly scores and decides if it's really an anomaly or not \cite{Laptev2015}.


\begin{table}[!htb]
\centering
\begin{tabular}{cp{8cm}}
\hline
\textbf{Name} & \multicolumn{1}{c}{\textbf{Requirement}}                       \\ \hline
E1           & Report predicted anomalousness of either live or historical data                              \\
E2            & Ensure that data is prepared identically as before training \\
E3            & Provide an intuitive way of assessing the quality of the prediction results                         \\ \hline
\end{tabular}
\caption{Requirements for the evaluation component.}
\label{tab:evaluation_requirements}
\end{table}

\subsection*{Non-funcational Requirements}
\label{sec:requirement_analysis:concrete_level:system_requirements}

The non-functional requirements can not be attributed to one specific component but refer to the system as a whole. Our system is not intended to be used for a one-time experiment, but should be able to serve for continuous use and for the constant comparison and improvement of new approaches. For this reason, it is important that the system is comfortable and easy to use (\textbf{N1}). Since the system is to be operated in the user's own networks, it must be possible to install it without much effort. It must therefore be as portable as possible (\textbf{N2}).

%Allow different instances of the system to run simultaneously with different feature configuration. Dromard et al. identified multiple feature subspaces that are capable to detect different attacks. For example (nbSrcs and avgPktSize) to detect DDoS attacks \cite{Dromard2017}. Also useful to include temporal context: One classifier trained on all past Mondays, one classifier trained on all past Decembers and one on all Mondays in past Decembers etc..

\begin{table}[!htb]
\centering
\begin{tabular}{cp{8cm}}
\hline
\textbf{Name} & \multicolumn{1}{c}{\textbf{Requirement}}                       \\ \hline
N1            & The system must be easy-to-use                         \\
N2            & The system must be easily portable \\ \hline
\end{tabular}
\caption{Non functional requirements.}
\label{tab:system_requirements}
\end{table}





%\chapter{Experiment}
%\label{ch:experiment}
%\begin{itemize}
  %\item To answer the question of a possible design for a system that detects anomalies in network traffic data, first the necessary components of the system are identified.
  %\item Then the requirements for the system are identified based on a literature review.
  %\item Based on these requirements, a concept for the system is then devised.
  %\item To validate the concept, a prototype of the system will be implemented and deployed in a test environment.
%\end{itemize}


%\begin{itemize}
  %\item "One way to reduce the diversity of Internet traffic is to employ aggregation." “volume per hour” or “connections per source” already exist. "many promising approaches turn out in practice to fall shortof one’s expectations" -> Possibility for data aggregation \cite{Sommer2010Jan}
  %\item Possibility to capture live data from network interfaces and to cope with high bandwidths. "Anomaly  detection  is  a  critical  component  at  the  heart of many real-time monitoring systems with applications in [...] network intrusion." \cite{Laptev2015}.
  %\item Deal with challenges that arise from the nature of the data. (-> Time Series Data) \cite{Laptev2015}
  %\item Possibility to evaluate historical data
  %\item Labeled data is not available in test environments, so the system has to work with unlabeled data. \cite{Goldstein2014Oct}
  %\item New machine learning algorithms can be included into the system in a simple way.
  %\item The system should take care of some fundamental processing steps and it should be possible for the user to add more processing steps that meet his needs. (-> Dimensionality reduction: Many algorithms are susceptible to the "curse of dimensionality". Scaling: Reduce the range of values. Encoding: encode categorical variables in a meaningful way. etc.).
  %\item Agnostic towards input data. Some prefer to use packet-level features [QUELLEN], some prefer flow-level features \cite{Dromard2015, Zhao2015}
  %\item Easy way to create and monitor configurations and training/prediction schedules. (too specific I guess.)
  %\item Easy and fast to deploy in test environments
%\end{itemize}
