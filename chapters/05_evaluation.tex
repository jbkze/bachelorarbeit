\chapter{Evaluation}
\label{ch:evaluation}

To evaluate the general framework concept and the chosen technologies, a prototype of the system is implemented and deployed in a test environment. Furthermore, we will conduct a proof-of-concept experiment using this setup to evaluate to what extent the framework meets our requirements.

\section{Implementation}
\label{sec:evaluation:implementation}

\subsection*{Ingestion and Storage}

We first look at the ingestion and storage part of our framework. Our ingestion tool argus sends its data via UDP to Logstash, the input pipeline of Elasticsearch. Logstash can also handle many other kinds of input such as TCP via plugins, however using UDP allows for the highest throughput. argus works on a client server principle. The argus server, called sensor, captures traffic from a specified interface or reads a pcap file and converts the data to a proprietary binary format. This format can then be interpreted by the argus clients, first and foremost the basic client ra. argus allows to specify via a list of arguments which features should be extracted from the 126 possible features. As described in Section \ref{sec:foundation:anomaly_detection:challenges}, the choice of features itself is subject of research. However, the focus of this work is not the attempt to find a list of optimal features, which is why we decided to extract those features from the feature list used for the creation of the popular benchmark dataset UNSW-NB15 that can be obtained with argus. A list of all features we extract including descriptions is given in Appendix \ref{app:features}.

We configured argus to output the timestamp fields stime (start time) and ltime (last time) according to the ISO standard 8601 since this is the expected time value format of Elasticsearch. It would be possible to run the argus server and the argus client on different machines and have them communicate over the network. But since we have both programs running on the same machine, we can simply send the output of argus via a pipe to ra, which in turn sends its output in a comma separated form via a second pipe to Netcat. Netcat is used for the UDP communication with Logstash. A simplified version of our argus/ra/netcat setup is given in Listing \ref{lst:ingestion_live}. Argus can also read in pcaps instead of listening to a live interface, as shown in Listing \ref{lst:ingestion_pcap}. %The rest of the pipeline is identical. Benchmark datasets are usually provided in comma separated files. Logstash offers a csv plugin, but it is also possible to reuse the presented pipeline. Instead of sending netcat the output of argus/ra, we simply pass the contents of the csv file as shown in Listing \ref{lst:ingestion_csv}.

\begin{lstlisting}[caption={Live ingestion using argus, ra and netcat.}, label={lst:ingestion_live},language=bash]
argus -i eth0 -w - | ra -s sport smac ... -c, | ncat -u $ip $port
\end{lstlisting}

\pagebreak

\begin{lstlisting}[caption={Read in pcaps with argus.}, label={lst:ingestion_pcap},language=bash]
argus -r record.pcap -w - | ra -s sport smac ... -c, | ncat -u $ip $port
\end{lstlisting}

%\begin{lstlisting}[caption={Send csv to Logstash.}, label={lst:ingestion_csv}]
%cat benchmark_set.csv | ncat -u $ip $port
%\end{lstlisting}

\subsection*{Graphical User Interface}

Our framework is controlled via a graphical user interface (GUI). For the implementation of the GUI, just like for the rest of our application, we decided to use the Python programming language. Besides personal experience with this language, this is due to the excellent pool of libraries. For the implementation of the GUI we use the open source web framework flask \cite{flask2021}. With flask, both front-end and back-end can be easily implemented using templates.

Essentially, our GUI serves two purposes: It can be used to create and manage various configuration files and to initialize the different tasks of our framework. There are four configuration files. For one thing a Elasticsearch configuration file that specifies IP, port, user, and password of Elasticsearch. Then the \textit{stat. feat config}, which defines the statistical features, i.e. the aggregations to be performed on the database. Secondly, the two filters for feature selection and time range selection. In the following we will refer to them as \textit{feature config} and \textit{time config}, respectively. For all four configs we use the YAML markup language, mainly because of its superior readability compared to e.g. XML or JSON.

The creation of the feature config is dynamic, which means that the user can load a list of all features from the database and can then make his selection per feature. This is illustrated in Figure \ref{lst:feature_config}. In the feature config for each feature it is specified if the feature should be used in training and whether and how it should be scaled, encoded, and reduced in its dimensionality. To address the problem of null values, the default value for the feature can also be specified here. That means we use a global constant as fallback value. Besides other possibilities like taking the mean from surrounding values or using an interpolated value, this is a valid approach \cite{Shopov2013Jan}. An example of a feature config is given in Listing \ref{lst:feature_config}.

\begin{figure*}[htb!]
  \centering
	\includegraphics[width=\linewidth]{images/feature_config.png}
	\caption{Creation of an feature config using the GUI.}
	\label{fig:feature_config}
\end{figure*}

The purpose of the time config is to specify as precisely as possible the time periods to be trained on. As described in Section \ref{sec:foundation:anomaly_detection:challenges}, network traffic can be very dynamic. It can be assumed that there is usually different traffic at different times, for example one expects less traffic at night than at noon or less at weekends than on weekdays. In our time config you can therefore filter by date, weekday, time and tags (holiday, working day, weekend). Instead of specifying a fixed date range, the user can also specify a variable period such as "last 30 days". During training, this is then interpreted and the corresponding date range is subsequently added. Only if a certain date matches at least one of the tags and the corresponding weekday is set to \texttt{use: true}, it will be included in the training. An example of a time config is given in Listing \ref{lst:time_config}. In this example, all working days of the entire year 2020 are selected. Note that although for sunday \texttt{use: true} is set, no Sunday will be included in the training, because a Sunday will never match the working day tag.

\noindent\begin{minipage}{.45\textwidth}
\begin{lstlisting}[caption=Feature Config,frame=tlrb, label={lst:feature_config}]{Name}
  destination.bytes:
    use: true
    default: 0
    scale: min_max
    enc: false
    dim_red: false

  destination.ip:
    use: true
    default: 0.0.0.0
    scale: false
    enc: hash
    n_components: 128
    dim_red: pca
\end{lstlisting}
\begin{lstlisting}[caption=Statistical Feature Config,frame=tlrb, label={lst:stat_feature_config}]{Name}
  interval: hour

  stat_features:
    parallel_conns:
      aggregation:
        field: event.start
        type: value_count
      derivative: 3

\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.45\textwidth}
\begin{lstlisting}[caption=Time Config,frame=tlrb, label={lst:time_config}]{Name}
  field:
    "@timestamp"

  date:
    start: 2020-01-01
    end:   2020-12-31

  time:
    start: "08:00"
    end:   "16:00"

  tags:
    working_days: true
    holidays:     false
    weekends:     false

  weekdays:
    monday:
      use: true
    ...
    sunday:
      use: true
\end{lstlisting}
\end{minipage}

To create the stat. feat config the user can add custom fields, select a reference attribute and choose the aggregation type. In addition, it is also possible to calculate the change of a value over a period of time, i.e. the derivation. Therefore, the user can select which degree of derivation should be calculated. If, for example, the third derivative is selected, the value of the first and second derivative will also be calculated and stored. The user interface for creating a stat. feat config is shown in Figure \ref{fig:stat_feature_config}. An example of a stat. feature config is given in Listing \ref{lst:stat_feature_config}.


\begin{figure*}[htb!]
  \centering
	\includegraphics[width=\linewidth]{images/stat_feat_config.png}
	\caption{Creation of an statistical features config using the GUI.}
	\label{fig:stat_feature_config}
\end{figure*}


There are three different tasks that can be launched from the GUI: aggregation, training, and prediction. The system is designed in a way that several instances of the same process can run simultaneously. Each task can be named by the user. The crontab notation can be used to specify a schedule for the task. The aggregation process takes the stat. feat config file as input as well as the name of the source index and the destination index in which the aggregated values are written. The training process takes a feature config, a time config, the Elasticsearch index name, and the name of the machine learning algorithm that should be used for training. Additionally he can give the resulting model a name. The prediction process takes the previously trained model as input as well as the Elasticsearch index name. The user has the choice to score either live or historical data. For the latter, he must specify a time configure In addition, he can specify the name of the Elasticsearch field in which the anomaly score is written.

All three tasks are implemented using the Prefect \cite{prefect2021} library. Prefect is an open source dataflow automation library that can be used to create, deploy, schedule, and monitor workflows written in Python. For each task we implemented a prefect workflow definition. For example, the training workflow is given in Listing \ref{lst:prefect_training}. When the user initializes a task via the GUI, the corresponding flow is scheduled using the given parameters. Prefect comes with an own monitoring interface that we embedded into our user interface. With the help of the Prefect GUI, the user can see which flows are scheduled for execution, whether all runs have completed successfully, and how long each step within a flow took. In the following, we will elaborate on the implementations of the said three processes.


\subsection*{Aggregation}

Elasticsearch brings several aggregation options out of the box that we use in our implementation. Assume that the user has set an aggregation interval of one hour. Then, each time our aggregation flow is executed, it will check if a full hour or more of new data has arrived in Elasticsearch since the last aggregation was performed. If this is the case, the aggregated data is collected via the Elasticsearch Aggregations API and written back to Elasticsearch.

\subsection*{Pre-Processing}

As described in Section \ref{sec:framework_design:general_concept}, features with very high value ranges can pose a problem for machine learning algorithms. We integrated three common scaling methods into our system: Min-Max Scaling, MaxAbsScaling and StandardScaler, using the scikit-learn \cite{scikit2021} implementation, a popular data science python library. The three scaling methods including their formula and description are listed in Table \ref{tab:scaling}.

\begin{table}[h]
\begin{tabular}{p{.2\textwidth}m{.2\textwidth}p{.5\textwidth}}
\toprule
\textbf{Name} & \textbf{Formula} & \textbf{Description}\\
\midrule
Min Max & $\frac{x - x_{min}}{x_{max} - x_{min}}$ & Scales features to the range [0, 1]\\ [0.5ex]
Max Abs & $\frac{x}{|x_{max}|}$ & Scales each feature by its maximum absolute value\\
Standard & $\frac{x - mean(x)}{std(x)}$ & Scales features such that its  distribution will have a mean value 0 and standard deviation of 1\\
\bottomrule
\end{tabular}
\caption{Integrated scaling methods.}
\label{tab:scaling}
\end{table}

In network traffic we have many categorical variables such as the network protocol. Values like UDP or TCP cannot easily be used as input for the machine learning process, so we need to find a numerical representation of the values. One possibility would of course be to assign a value from 0 to n to all possible values. However, this would suggest an ordinality that does not exist. For example, if UDP were assigned a 0, TCP a 1, and ICMP a 2, a spurious order ICMP > TCP > UDP could be assumed. One way to prevent this is to create a new column per unique value of a feature and always set only the value of one column to 1 while the other columns are 0. This procedure is called One-Hot-Encoding (OHE). For features where the value range of a feature is known during training, this works well, but when new values occur during prediction no corresponding column exists. Also, for features where the value range is extremely large (IP/MAC addresses), you would need billions of columns. One way to address this problem is to use hash encoding. Here the value is hashed with a fixed bit number and the binary representation of the hash is taken as the new value for the feature. The number of bits of the hash corresponds to the new number of columns. We have integrated both methods into our system, again using the scikit-learn implementation.


Whether OHE or hash encoding is used, the number of columns increases greatly. This makes the data sparse, which, as mentioned earlier, can be a problem because the statistical significance decreases. We have integrated two methods of dimensionality reduction: Principal Component Analysis (PCA) and Variance Threshold (VT), both using the scikit-learn implementation. PCA uses linear algebra techniques to project data into a lower dimensional space. It tries to find linearly independent vectors - the principal components - that represent the original data. These components are ranked according to the variance along them. One can then choose a number of components that now represent the data (see \texttt{n\_components} in our feature config in Listing \ref{lst:feature_config}). PCA tries to keep as much of the original variance as possible. That means if, say, the first 16 components are chosen, those 16 vectors with the highest variance along them are used \cite{Bro2014}. As the name suggests the Variance Threshold method discards all features whose variance is below a certain threshold. This method is quite simple since it does not require any linear transformations. The threshold can also be specified in our feature config if VT is selected as the dimensionality reduction method.

%As can be seen in the code for the training flow (Listing \ref{lst:training_prediction}) the performed pre-processing steps are not set in stone. In case the user wants to add other pre-processing steps, he can easily do so and integrate them into the flow. Also the feature config and its parser are extensible, so adding new parameters is simple.


\subsection*{Training}

The code for the training workflow is given Listing \ref{lst:training_prediction}. First the application context is initialized with the passed parameters. Then, depending on the content of the feature config and time config, the training data is loaded from Elasticsearch. All null values are overwritten with the corresponding default values specified in the feature config and flows with measurement errors (missing IP address) are dropped. After that, again depending on the content of the feature config, the training data is scaled, encoded, and reduced in its dimensionality.

\begin{minipage}{\linewidth}
\begin{lstlisting}[caption={Prefect flow for training.},label={lst:prefect_training},language=Python]
from prefect import Flow

with Flow("Training Template") as flow:
    # Load parameters into application context
    app_context.initialize(params)

    # Make sure that the extraction starts after the initialization of the AppContext has finished by setting the initialization task as upstream dependency
    raw_data = database.query_historical.set_upstream(app_context.initialize)

    # Data cleansing
    processed_data = processing.handle_null_values(processed_data)
    processed_data = processing.drop_bad_flows(processed_data)

    # Scaling
    scaled_data = scaling.scale(processed_data)

    # Encoding & Dimensionality Reduction
    encoded_data = encoding.encode(scaled_data)

    # Training
    model = training.train(encoded_data)

    # Save model
    storage.save_model(model)
\end{lstlisting}
\end{minipage}

After the pre-processing is complete, the actual training begins. We have implemented the repository of training algorithms mentioned in the general concept as a collection of python scripts. This means that there is one .py file per algorithm. Adding new algorithms is straightforward thanks to the code frame shown in Listing \ref{lst:training_prediction}. The algorithm implementation must provide a training method and a prediction method. The training method takes a pandas DataFrame as input, which contains the pre-processed data. Everything else is up to the user. We have integrated over 20 algorithms into our system, mainly from the pyOD (python outlier detection) library \cite{zhao2019pyod}. pyOD offers a unified API for various outlier detection algorithms. In the example, the One Class SVM from the pyOD libarary is integrated for demonstration purposes, but inserting your own code with novel approaches is of course possible and encouraged. In the end, the method returns the trained model. Thanks to python's dynamic typing, the model can be of any data type.

Then the model is stored together with several parameters, which must be the same for prediction as for training. These include the feature config, since of course the same features must be loaded from the database for prediction and they must be subjected to the same pre-processing steps. In addition, the dynamic parameters that were generated during pre-processing are stored. For the scaling methods, for example, these are the min-max values, for the OHE the list of unique features, and for the PCA the linear components.


\begin{minipage}{\linewidth}
\begin{lstlisting}[caption={Interface for machine learning algorithms.},label={lst:training_prediction},language=Python]
import pandas as pd
from pyod.models.ocsvm import OCSVM

def train(data: pd.DataFrame):
    """Your training code goes here.
    Args:
        data: The pre-processed data, ready for training.
    Returns:
        The trained model
    """
    # In this minimal example we use a standard One Class SVM
    classifier = OneClassSVM()
    classifier.fit(data)

    return classifier

def predict(classifier, data: pd.DataFrame) -> pd.DataFrame:
    """Your prediction code goes here.
    Args:
        classifer: The previously trained model.
        data: The pre-processed data, ready for prediction.
    Returns:
        The anomaly scores as predicted by the model. For consistency in a pandas DataFrame.
    """
    scores = classifier.score_samples(data)

    return pd.DataFrame(scores)
\end{lstlisting}
\end{minipage}

\subsection*{Prediction}

The prediction runs very similarly to the training, which is why the corresponding flow definition is not shown again here. The application context is initialized with the values loaded from the model, especially the feature config and the pre-processing parameters. If the user has decided to evaluate historical data, the corresponding data is loaded from the database based on the time config. If the user has chosen live prediction, the data that has been added since the last prediction is loaded from the database. Then, as with training, the data is pre-processed. The actual prediction is done by calling the prediction method of the algorithm used for training. The method is passed the data to be evaluated and the trained model as can be seen in Listing \ref{lst:training_prediction}. The concrete code of the prediction method is of course again left to the user. The expected return value is a pandas DataFrame containing the anomaly scores for each data point. Afterwards, the anomaly scores are written back to Elasticsearch.

\subsection*{Evaluation}

%With traditional systems that work with benchmark data sets, the evaluation process is relatively straight forward. Since the expected result is known for each data point, it is easy to see how many predictions were correct or how close they were to the truth. From this, various accuracy metrics can be calculated so that algorithms can be easily compared. For a system like ours that is intended for experimental use in live networks, one has to get a bit more creative with the evaluation. [TODO]

With the visualization tool Kibana, which is included in Elasticsearch, one has many possibilities to visualize and analyze the raw data and to observe the predictions of the different models live. With Kibana, the user is able to gain live insight into the data and see possible correlations between anomaly scores and the features. Kibana works with dashboards for visualization, which can be created intuitively via an web interface. Examples of such dashboards are presented in Section \ref{sec:evaluation:experiments}.

Of course, the user is not limited to Kibana. For custom analysis, Elasticsearch's excellent interface to data science programming languages like python can be used. With the help of Elasticsearch client libraries like eland \cite{eland2021} one can easily load the data from an index into a pandas DataFrame. Thus, on the one hand, visualizations can be created that go beyond the capabilities of Kibana, or, when labeled data is available, the accuracy metrics mentioned above can be calculated. However, this is beyond the scope of this thesis.


\section{Deployment}
\label{sec:evaluation:deployment}

To test our implementation, we deployed it in a laboratory network at Darmstadt University of Applied Sciences. We had access to three machines: one server on which we installed our system and two other machines that were used to generate load. We used a very basic load generator called noisy \cite{noisy2021} that generates random DNS/HTTP(S) traffic. All three machines are connected via 1Gbit line to a FreeBSD router, which has access to the Internet. The network administrator of the university has installed argus on the router, listening on the interfaces the three machines are connected to. Via a separate interface the router sends the data generated by the argus client via UDP to Logstash. This way the UDP stream does not interfere with the line argus is listening on. The technical specifications of the server on which our system is installed is listed in Table \ref{tab:specs}. A schematic overview of the network setup is given in Figure \ref{fig:uni_lab}. Thanks to the use of docker-compose the system was easy to install. Essentially, all that needs to be done is to copy the code along with the docker-compose files to the server and launch the services via \texttt{docker-compose up}. All dependencies are handled within the Docker images without requiring the user to install them manually. The only requirement is that the Docker daemon is installed on the server.


\begin{figure*}[htb]
  \centering
	\includegraphics[width=0.8\linewidth]{images/deployment_setup.pdf}
	\caption{Uni lab network setup.}
	\label{fig:uni_lab}
\end{figure*}

\begin{table}[]
\centering
\begin{tabular}{lp{7cm}}
\hline
\multicolumn{1}{c}{\textbf{Component}} & \multicolumn{1}{c}{\textbf{Specification}}                             \\ \hline
Processor                              & (2x) Intel(R) Xeon(R) Gold 5218 CPU @\,2.30GHz (32 cores - 64 threads) \\
RAM                                    & 192 GB                                                                \\
Storage                                & 5 TB                                                                  \\ \hline
\end{tabular}
\caption{Hardware specifications for the server our system is installed on.}
\label{tab:specs}
\end{table}


\section{Experiment}
\label{sec:evaluation:experiments}

A key point in machine learning is to analyze the problem that should be solved thoroughly beforehand. Training a model with all available data and hoping that it will be able to detect all possible anomalies will most certainly not be effective. It is therefore important to select useful features to train with in advance, which is non-trivial as explained in Section \ref{sec:foundation:anomaly_detection:challenges}. Our system can help with this step.

Suppose our goal is to train a model that detects server failures, not an uncommon concern for a network administrator. Then the first question is on which features such a model should be trained, i.e. in which features a server failure is reflected. Figure \ref{fig:metrics_overview} shows a portion of a Kibana dashboard that visualizes all features that argus extracts, broken down by source and destination. The time period of the last 4 weeks is visualized. In the example, we can see that after about a third of the time, the values of most features drop. This is supposed to simulate a server failure and was provoked by us by stopping the load generators on two machines.

\begin{figure*}[htb]
  \centering
	\includegraphics[width=\linewidth]{images/metrics_overview_ausschnitt.png}
	\caption{Kibana dashboard showing an overview over features.}
	\label{fig:metrics_overview}
\end{figure*}

The decline is most noticeable in the number of unique source and destination ports. Since the port number is randomly selected when a connection is established, the number of unique ports very accurately reflects how many connections were made. The drop is also clearly visible in the bytes per second features, especially the destination bytes. However, here the problem occurs that we called "short-term dynamics" in Section \ref{sec:foundation:anomaly_detection:challenges}. In Figure \ref{fig:destination_bytes}, we can see the destination bytes feature zoomed in for a time period in which the load generator was running. We see that very high amplitudes with values from a few thousand to 400,000 bytes/s occurred. This means that the state "load generator is running" is not reflected uniformly in this feature, so that at most an aggregated variant of this feature would be suitable for training.

It is interesting to note that the source bits per second, in contrast to the destination bits per second, are virtually unaffected by the omission of the load generators. This can perhaps be explained by the fact that the requests sent by the load generator are generally quite small and the omission of these requests is therefore not that significant. Either way, the source bits per second feature is not suitable for our training.

\begin{figure*}[htb]
  \centering
	\includegraphics[width=\linewidth]{images/destination_bytes.png}
	\caption{Kibana visualization of the destination bytes feature.}
	\label{fig:destination_bytes}
\end{figure*}

These investigations can now be conducted for the remaining features as well. We decided to train our model on the unique number of source and destination ports and aggregated forms of the destination bytes and destination bits per second. We trained two models using two different machine learning algorithms: K-Nearest-Neighbor (KNN) and Isolation Forest. We let all three models score the same time period shown in Figure \ref{fig:metrics_overview}. The anomaly scores can be seen in Figure \ref{fig:metrics_overview_as}. The results obtained with the KNN algorithm are plotted in red, those from the Isolation Forest in blue. We see that both algorithms, reliably detected the omission of the load generator. However, one can clearly see that the anomaly scores of the KNN algorithm have significantly more downward spikes, which can potentially lead to false negatives. One reason for this may be that the KNN is a distance-based algorithm which are prone to errors if the training data contains too many irregularities. We ran the experiment again for the KNN, but this time using a standard scaler that can help address this issue. As can be seen in Figure \ref{fig:metrics_overview_as}, this approach was successful. The new results are plotted in yellow. We see that the KNN outperforms not only the original KNN, but also the isolation forest when the data has been previously scaled.


\begin{figure*}[htb]
  \centering
	\includegraphics[width=\linewidth]{images/metrics_overview_as_ausschnitt.png}
	\caption{Kibana overview dashboard with anomaly scores. Isolation Forest in red, K-Nearest-Neighbor in blue.}
	\label{fig:metrics_overview_as}
\end{figure*}


\begin{figure*}[htb]
  \centering
	\includegraphics[width=\linewidth]{images/load_as_scaled.png}
	\caption{KNN achieves better results when data is scaled.}
	\label{fig:metrics_overview_as}
\end{figure*}




% \begin{lstlisting}[language=Python]
% import seaborn as sn
% import eland
%
% from elasticsearch import Elasticsearch
%
% # Instantiate Elasticsearch client
% es_client = Elasticsearch("localhost:9200")
%
% # Proxy DataFrame
% eland_data = eland.DataFrame(es_client=es_client,
%                              es_index_pattern="network-lab-data-*")
%
% # Pandas DataFrame
% data = eland_data.to_pandas()
%
% # Calculate correlation matrix
% corrMatrix = data.corr()
%
% # Visualize correlation matrix
% sn.heatmap(corrMatrix)
% plt.show()
%
% \end{lstlisting}

%\cite{Dromard2017} hat rausgefunden, dass bestimmte Attacken sich in bestimmten Features Spaces wiederfinden. Ein paar dieser Attacken simulieren wir hier.

\section{Discussion}
\label{sec:evaluation:discussion}

With our implementation and our experiment we have shown that we reached the stated goal of this thesis of designing a framework that can be used to test and compare machine learning algorithms for their ability to detect anomalies in real network environments.

Our system can work with either live or historical data and is able to perform the required pre-processing steps on the data. We have proven that through our plug and play mechanism, the machine learning algorithms can be easily interchanged. Portability was demonstrated by deploying the system to a new network and usability is ensured by an easy-to-use graphical interface. In addition, we showed how visualizations can be used to compare the quality of the predictions made by the different machine learning models. At this point, some limitations arise due to the nature of the type of machine learning used. It is common in research to compare algorithms based on certain metrics that provide information about the accuracy of the predictions. This way, algorithms can be directly compared to each other. The reason that our system does not report such metrics is that we were specifically aiming to work in real network environments. In this scenario, there is no labeled data, so one does not know in advance exactly what results to expect. Due to the lack of these labels, no performance metrics can be calculated. In any case, this difficulty was addressed by the presented possibility to gain insights through visualizations of data and anomaly scores. In addition, the researcher is of course free to manually label the collected data afterwards and calculate these metrics on his own. Due to the excellent integration of our proposed database solution Elasticsearch in many popular programming languages, this is technically feasible with no problem. However, this process was beyond the scope of this work. Furthermore, there are already works that offer a solution to test different algorithms in offline scenarios with labeled data \cite{Zoppi2019}. Thus, a possible workflow could be to use such offline tools in a preliminary analysis to compare algorithms with respect to common detection performance metrics and use the results as a starting point for selecting algorithms that will then be incorporated into our system to test them in real network environments.

If we recall back to Section \ref{sec:foundation:anomaly_detection:challenges}, which discusses the difficulties inherent to anomaly detection in network traffic, one problem cited was that results achieved on one network do not necessarily translate to other networks. Thus, one could argue that our conducted experiment is of limited significance. However, one should bear in mind that the goal of this work was never to discover a particularly effective or transferable method of anomaly detection. Instead, we aimed to design a system that could be used to test and compare machine learning algorithms in real network environments, and this goal has been achieved. Our system is portable, allowing future researchers to conduct their own experiments on their networks without much effort.

However, our solution does have some limitations, although these relate more to our implementation than to the concept of the framework. For example, there is no validation of the configuration files. In particular, our system does not detect configurations that require features to be pre-processed in a nonsensical way such configurations that require a feature to be encoded \textit{and} scaled.

In addition, our implementation requires that all algorithms be written in the same programming language, which in our case is python. A remedy here could be the integration of remote procedure calls (RPC), which can be implemented across programming languages with frameworks such as Apache Thrift. The actual algorithm could then be written in Java, for example, and called in our interface via an RPC.

Another limitation is that our system does not inherently provide a parameterization capability for the algorithms. Tuning hyperparameters of machine learning algorithms can increase the detection performance. However, since the user has full control over the implementation of the algorithms, it would be conceivable to let the algorithm read in parameters via text files and perform the hyperparameter tuning via these text files. However, integrating this option into the graphical user interface would certainly make sense.

Our generic design brings the advantage that the user can incorporate both supervised and unsupervised algorithms. With the former, however, obtaining the labels has to be taken care of during training. One has to know at the time of implementation what the column is called in which the labels are to be found, so that they can be considered during training. That might be inconvenient and error-prone. However, since the goal of the thesis is explicitly the use in real networks and labeled data is uncommon here, the trade-off in favor of generality over facilitation of supervised learning makes sense.


%The value this work has created is apparent when looking at Table \ref{tab:discussion}. Although a variety of network anomaly detection systems have been presented in the past, it can be seen that none of this work meets all of the requirements that we have identified.


%\begin{table}[]
%\centering
%\begin{tabular}{c|cllc}
%Publication          & \cite{Zoppi2019} & \multicolumn{1}{c}{.} & \multicolumn{1}{c}{.} & \textbf{Ours} \\
%\textbf{Requirement} & 2019             & \multicolumn{1}{c}{.} & \multicolumn{1}{c}{.} & \textbf{2021} \\ \hline
%C1                   & \xmark           & \multicolumn{1}{c}{}  & \multicolumn{1}{c}{}  & \cmark        \\
%C2                   & \cmark           & \multicolumn{1}{c}{}  & \multicolumn{1}{c}{}  & \cmark        \\
%C3                   & \cmark           &                       &                       & \cmark        \\
%C4                   & \xmark           &                       &                       & \cmark        \\
%C5                   & \xmark           &                       &                       & \cmark        \\
%C6                   & \xmark           &                       &                       & \cmark        \\
%C7                   & \xmark           &                       &                       & \cmark        \\
%C8                   & \xmark           & \multicolumn{1}{c}{}  & \multicolumn{1}{c}{}  & \cmark        \\ \hline
%P1                   & \xmark           & \multicolumn{1}{c}{}  & \multicolumn{1}{c}{}  & \cmark        \\
%P2                   & \xmark           &                       &                       & \cmark        \\
%P3                   & \xmark           & \multicolumn{1}{c}{}  & \multicolumn{1}{c}{}  & \cmark        \\
%P4                   & \xmark           & \multicolumn{1}{c}{}  & \multicolumn{1}{c}{}  & \cmark        \\ \hline
%M1                   & \cmark           & \multicolumn{1}{c}{}  & \multicolumn{1}{c}{}  & \cmark        \\
%M2                   & \cmark           &                       &                       & \cmark        \\
%M3                   & \xmark           &                       &                       & \cmark        \\
%M4                   & \xmark           & \multicolumn{1}{c}{}  & \multicolumn{1}{c}{}  & \cmark        \\ \hline
%E1                   & \xmark           & \multicolumn{1}{c}{}  & \multicolumn{1}{c}{}  & \cmark        \\
%E2                   & \cmark           &                       &                       & \cmark        \\
%E3                   & \xmark           &                       &                       & \cmark        \\
%E4                   & \xmark           & \multicolumn{1}{c}{}  & \multicolumn{1}{c}{}  & \cmark        \\ \hline
%N1                   & \cmark           &                       &                       & \cmark        \\
%N2                   & \cmark           &                       &                       & \cmark        \\ \hline
%\end{tabular}
%\caption{We are the best.}
%\label{tab:discussion}
%\end{table}
