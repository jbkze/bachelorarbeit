\chapter{Framework Design}
\label{ch:framework_design}

Now that we have identified the requirements for our system, we can proceed to the design. The result of the design phase will consist of two parts. On the one hand, the result should be a framework design that is as generally valid as possible and not yet tied to specific technologies. On the other hand, a comparative technology recommendation will be made on the basis of this general concept reflecting the current state-of-the-art.


\section{General Concept}
\label{sec:framework_design:general_concept}

For the design of our proposed solution, we want to adhere in particular to the design principle of separation of concerns. This principle states that "a given problem involves different kinds of concerns, which should be identified and separated [...]" and thus helps us to divide our system into easier-to-manage, loosely coupled components \cite{Aksit2001Jan}. In the following, we will therefore design individual components and then combine them into a complete system.

\subsection{Data Capturing}
\label{sec:framework_design:general_concept:capturing}

The entry point into our framework is the data ingestion component. All data from the various sources is gathered here and stored in the central storage solution. Here, we will address the problems that can arise during ingestion and our approaches to solving them. With our requirements C1, C2 and C3, we cover the three basic situations that can be encountered during ingestion. C1 requires the ability to extract information from current traffic in real time, C2 requires the information to be extracted from already recorded traffic, and C3 requires that research-standard IDS datasets can be used as input.

The most complex task is the satisfaction of C1. One difficulty in capturing live traffic is the distributed nature of networks. The way computer networks are constructed is called network topology. These topologies can be highly branched and split into different sub-networks. This complicates the task of capturing the traffic of a network holistically. Often, there is not one entry or exit point of a network where a capturing solution could be installed. One solution to this problem is to operate a mesh of sensors that can be distributed throughout the network. The sensors collect the traffic of their monitored node and send it to a collection point. This collection point must then reassemble the data in the correct order.

In some cases, the capturing part has already been taken care of elsewhere. For example, some companies save backups of their traffic, and some research projects provide their raw data in the form of packet captures. As required by C2, we also want to be able to work with this kind of input. The problem here is that traffic can be saved in many formats, depending on the tool used to capture it. If the stored traffic comes in the same format as that produced by our live capture solution, the traffic can simply be passed on to the next component, the feature extraction. If not, the traffic must first be converted to the correct format.

Requirement C3, is the least demanding to fulfill. IDS datasets are usually created by either generating completely artificial data streams or recording traffic in controlled environments and then extracting specific features from this data \cite{Ring2019Mar}. Therefore, the part of capturing and feature extraction can be omitted in our system for this input type. In addition, these sets are usually available in a structured form, for example as text files, so they can be easily fed into the database.

As mentioned in Sec. \ref{sec:requirement_analysis:abstract_level}, Boutaba's framework design (see Fig. \ref{fig:generic_framework2}) illustrates that in the case of (semi-)supervised learning, labels must already be provided during data collection. At the same time, however, as required by M3, we want to be able to work with entirely unlabeled data. Therefore, our design does not impose any obligation on the user to provide labels at this point, nor does it specify any way in which he might do so. The latter is due to the fact that labels can come in different forms and we want to remain as generic as possible. For instance, the UNSW-NB15 dataset \cite{Moustafa2012Oct} uses not only the binary classifications \textit{normal} and \textit{attack}, but uses also labels for the different attack types. In case the user does provide labels, they are treated like any other attribute and are stored with the rest of the data in the database.

\subsection{Feature Extraction}
\label{sec:framework_design:general_concept:feature_extraction}

Whether the traffic came in pre-recorded or was recorded live by our system, it is now still in a raw format that cannot yet be readily worked with. The process of obtaining information from the raw data is called feature extraction. There is a wide range of possible list of features that can be extracted from network traffic. At this abstract level, we will present only the two ways in which traffic can be analyzed, without already choosing among them or even selecting a particular set of features. Network traffic can be analyzed on two levels: at the packet level or at the flow level \cite{Ring2019Mar}. As the name suggests, feature extraction at packet level examines each individual network packet. Information at this level is usually extracted from the packet headers. In addition, deep packet inspections can be performed, which also take the payload into account. As described in Sec. \ref{sec:foundation:network_basics:traffic}, one characteristic of network traffic is that packets can differ greatly depending on which protocols are used. Accordingly, the information that can be extracted at the packet level also varies from packet to packet. In machine learning applications, however, the goal is usually to have a reasonably uniform feature set, since many algorithms do not handle sparse input matrices well \cite{Adomavicius2012Apr}. This leads to the fact that the very high information content, which is available at the packet level, can usually not be fully exploited and the features must be boiled down to a list of common attributes. When analyzing network traffic at the flow level, packets that belong together are aggregated to network flows. The packets belonging to a flow are identified by shared attributes such as IP addresses and port numbers, and by their temporal proximity. This abstraction makes it possible to compute metadata of two participants' communication that is not visible at the packet level. At the same time, detailed information is lost due to aggregation. Also, the payload is usually ignored. Both approaches have their advantages and disadvantages and at this point we do not want to decide in favor of either variant; the framework design per se is the same in both cases. In fact, our design even allows the parallel use of both approaches. After collecting the raw data, theoretically two feature extraction processes could run in parallel, one at package level, one at flow level.

\subsection{Storage}
\label{sec:framework_design:general_concept:storage}

At this point, we either have our data readily available in text form from existing datasets or we have extracted our own features from live or pre-recorded traffic. That data now needs to be stored persistently. The high speed at which data flows through the network and the sheer volume of data that accumulates after a very short time poses a challenge to our database. Our database is the central data hub and not only has to cope with the high speed of incoming data, but also the output needs to be fast. A congestion at output turns the database into a bottleneck and the entire application is slowed down. Requirement C4 demands accordingly that the database can cope with these conditions. There are numerous approaches from different vendors to address the performance issue, however, at this abstract level we want to look at what basic property our database must have. When coping with growing volumes of data, Lake et al. identify scalability as the most important quality of a database. They define scalability as "the ability of a database system to continue to function well when more users or data are added." \cite{Lake2013}. In our case, scalability is solely related to data, as it is not expected that an exceedingly high amount of users will attempt to access our system. Specifically, this means that the database still allows efficient access to the data despite the increased amount of data coming in \cite{Lake2013}. Databases can scale either horizontally or vertically. Vertical scaling involves enhancing existing infrastructure in terms of its resources, i.e. equipping it with more RAM or CPU computing power. Due to the ever decreasing prices for hardware, this is a relatively easy measure to take \cite{Lake2013}. The drawback is that eventually hardware limitations occur, to a point where the system cannot be improved any further. When there is a large amount of stored data, accessing the entries then takes too long. When scaling horizontally, multiple instances are interconnected to work as a composite. These instances then share the memory and processing load \cite{Lake2013}. This method is quite flexible, as instances can be added or removed as needed. Certainly, there are numerous other methods to tune database performance. The most common means is called indexing. The goal here is to make obtaining certain entries as efficient as possible without requiring to scan the entire database. For this purpose, data structures are created, which contain field values and pointers to the records these values relates to. A suitable analogy is a telephone book. If you are looking for the number of a certain person, you don't read through all the entries, but first filter by city, then last name, then first name, so that you end up reading only a small number of entries. In Sec. \ref{sec:framework_design:technology_choices}, we will take a closer look at further measures taken by other technologies and which of them are suitable for our project. At this point, we suffice to stress the need for a database that is optimized in terms of input and output speed and that can scale horizontally and/or vertically with changing demands.

Once the data is stored, we are able to perform aggregations on it as required by C5. Aggregation in this case does not mean aggregating multiple packets into one flow, but rather aggregations over a longer period of time. Once enough data has been collected, statistics over several hours, days, weeks or even longer periods can be calculated. Information of interest could be, for example, how many users access the network on average, how many bytes or packets flow in which direction in a given time, how long communications take on average, etc. The user has the option to create his own types of aggregation. The aggregated data are stored separately in the database and are available for the machine learning process just like the non-aggregated data.

\begin{figure*}[htb!]
  \centering
	\includegraphics[width=\linewidth]{images/Input.png}
	\caption{Data Collection Concept. [TODO. Nochmal in IPE machen]}
	\label{fig:data_collection}
\end{figure*}


\subsection{Pre-Processing}
\label{sec:framework_design:general_concept:pre_processing}

Requirement P1-P4 necessitate a number of pre-processing steps. The question is at which point this pre-processing takes place. Clearly, it would be convenient if the data was already processed during ingestion, so that only high-quality data - ready for the machine learning process - would be stored in the database. Especially since requirement E1 that demands the same pre-processing to occur before prediction as before training would automatically be fulfilled then. However, this is not possible for two reasons. For one thing, there are numerous possibilities how data processing can happen. While there are certain steps that are commonly performed such as scaling, encoding or dimensionality reduction, there are many different technical implementations for each of these steps. The choice of the right method itself is subject of research. Thus, it would be a mistake to commit to a particular variant of processing in the ingestion part. Especially since the proper choice can be different for each machine learning algorithm. Therefore, the user has the option to configure per feature which pre-processing steps should be performed. How exactly this works will be discussed later in this section. The graver reason, however, is that it would not be technically feasible to do so. Techniques such as scaling are dynamic. Scaling methods are usually based on min and max values, which of course differ depending on the subset of the data at hand. So the processing step has to happen downstream, after the data has been loaded from the database, and before it gets into the machine learning part.

We focus here on the three essential processing steps scaling, encoding and dimensionality reduction, but many more steps are possible, which should be easy to add by the user. For example, there are algorithms (or their implementations) that can only work with float data, not with integer data (e.g. the Robust Random Cut Forest implementation presented in \cite{bartos_2019_rrcf}). Here, an appropriate type cast would have to be done in advance. The order of the processing steps, however, cannot be arbitrary. If, for example, scaling is performed first and missing values are inserted after, these inserted values will not have been affected by the scaling. The treatment of missing values should therefore take place first. The order of scaling and encoding, on the other hand, does not matter because the set of features concerned do not intersect. Only numeric values, such as the packet size, reasonable to be scaled. Encoding, on the other hand, is performed on categorical variables, such as the name of the transport protocol. Scaling the values created during encoding would not make sense. Dimensionality reduction occurs last. The goal here is to reduce the number of dimensions, i.e. features. This results in lower computation times and potentially better detection performance. This is only possible when all features are already scaled and encoded as desired. If the user inserts additional processing steps, he must find a suitable place for them.

Coming back to requirement E1, which requires the same processing before prediction as before training. The difficulty is that this goal is not met by simply running the same implementations of each processing step before prediction. As described earlier, some of the processing steps are dynamic. For example, with respect to scaling again, the min-max values observed during training must be stored and reused during prediction. The parameters generated during the training stage are therefore stored together with the later trained machine learning model and loaded along with the model during the prediction stage. This is illustrated in Fig. \ref{fig:processing}.

\begin{figure*}[htb!]
  \centering
	\includegraphics[width=\linewidth]{images/training_processing_save.png}
	\caption{Make sure that data is processed in the same way during ingestion. [TODO. Nochmal in IPE machen]}
	\label{fig:processing}
\end{figure*}

\subsection{Machine Learning}
\label{sec:framework_design:general_concept:machine_learning}

The machine learning part splits into two parts: training and prediction. Training involves using a machine learning algorithm to train a model based on the preprocessed data, which is then presented with new data during prediction. Training must naturally take place before prediction. However, the prediction does not have to follow immediately after the training. Our system is designed in a way that the resulting model is stored permanently and can be used as desired for a prediction run later on.

M2 requires the ability to easily extend the set of available machine learning algorithms. For this purpose, a repository is built into our system where the implementations of each algorithm are stored. The user can add new algorithms by extending the repository with the implementation of the algorithm, which must provide methods for training and prediction. The input for the training method is the pre-processed data, the output is the trained model. The input for the prediction method is the previously trained model and the data to be evaluated, the output is a list of anomaly scores. Some algorithms, do not need any prior training, but only start during prediction on the basis of the data to be evaluated, to grasp the nature of the data and to detect any deviations. Such algorithms can also be integrated into our system. In this case, the training method simply returns an empty data structure and ignores the dummy model during prediction. Furthermore, by not requiring labels as input to the training method, we also satisfy requirement M3. Although our framework is intended for  unsupervised machine learning, the user still has the option to use (semi-)supervised algorithms if desired. For this, the user must know the name of the column with the labels and can then use its values for the supervised algorithm.

Once the training is finished, the model is saved with all the parameters needed for prediction. In addition to the already mentioned parameters that result from the pre-processing, we also store the user's selection of which features should be used and whether and how they should be pre-processed. The prediction can only work if the model has the same kind of input as it had during training. One could trust the user to reliably make the same selection before prediction as before training. However, this is error-prone and contradicts requirement N1, which emphasizes usability.

During prediction, the model will be loaded together with these parameters, so that the same features are fetched from the database and the pre-processing steps are performed in the same way as during training, fulfilling requirement E1. Then the model evaluates the new data. As described in Sec. 1, the output can be either in the form of discrete labels or continuous scores. We do not want our design to dictate which variant should be used. For the sake of simplicity, however, we will refer to the output as \textit{anomaly score} in the following. After the prediction is completed, these scores are finally written back to the database. As a result, for each entry, the associated anomaly score lies in the database.

\subsection{Usage}
\label{sec:framework_design:general_concept:usage}

So far, the individual components of the system have been described, rather than how the system is supposed to be used. It is important that the system can be coordinated by a user as easily as possible, this results from requirement N1. We propose a central interface for the user. The interaction of the user with the system is shown in Fig. \ref{fig:proposed_system} through the colored arrows. Basically our application has two operational branches: The training branch and the prediction branch. Some parameter settings are set only for training (marked in red) or only for prediction (marked in green), some are set independently for both (red and green), and some are set during training and are then copied for prediction (orange).

If enough data has been collected and the user now wants to train on (part of) this data, he has to specify several things. On the one hand he specifies via a filter, that we call \textit{feature config} in the following, which features should be loaded from the database and if and how they should be pre-processed. In addition, he specifies via the \textit{time config} the time period of interest from which data should be loaded. This temporal selection is necessary to be able to adapt to concept shifts as required by requirement M4. Finally, the user must select one of the available machine learning algorithms. Of course, the user can train multiple models, for example, multiple models for different combinations of features, for different types of pre-processing or for different time periods. All these models are stored in the model repository. During prediction, the user can then select from one of the trained models. The selection of which features are loaded from the database and their pre-processing are loaded from the stored model, so that the user does not have to select them again. All he has to specify is whether he wants to evaluate the current live traffic or historical data.


In Fig. \ref{fig:proposed_system} you can see our proposed system.

\begin{figure*}[htb!]
  \centering
	\includegraphics[width=\linewidth]{images/framework.png}
	\caption{Our proposed system.}
	\label{fig:proposed_system}
\end{figure*}


\section{Technology Recommendations}
\label{sec:framework_design:technology_choices}

In the following, we will compare different technologies in terms of their suitability for our project to flesh out the concept from Sec. \ref{sec:framework_design:general_concept}. The selection presented here must be treated with the reservation that it can only reflect the current state of the art and may be in part outdated in the future. In addition, there are many considerations that might influence this selection, and depending on the user and his existing environment, other technologies may be more suitable than the ones chosen here. We assume an environment in which no infrastructure is yet in place. We work, so to speak, on the drawing board.


\subsection{Choice of Ingestion Tool}
\label{sec:framework_design:technology_choices:ingestion}

There are a variety of tools that can be used to capture network traffic and extract information from it. We discuss several candidates for packet level and flow level feature extraction and then choose a suitable solution.

\subsection*{Tools for packet-level feature extraction }

Some of these tools include the tasks of data collection and feature extraction, which are separate from each other in our general concept.

The de-facto standard for packet-level traffic capture is the pcap (packet capture) API \cite{Velea2017}. It is available on UNIX based operation systems in form of the C/C++ libpcap library and the command-line tool tcpdump which is using libpcap\footnote{https://www.tcpdump.org/}. WinPcap\footnote{https://www.winpcap.org/} offers an implementation for Windows machines. Tcpdump can capture traffic from the Transport, Internet and Application Layer and provides the possibility to capture specifically the traffic of certain hosts, subnets or applications using filters. The output is in the pcap format which is supported by most network analyzing tools \cite{Velea2017}. Depending on the filter selection, pcap files can be used to create one-to-one copies of the traffic. They can be used to extract all forms of packet level or flow level features. The bandwidth limit of tcpdump depends largely on the installed hardware, especially the CPU, and the size of the packets \cite{Emmerich2016Dec}. Tcpdump works on a best effort principle, which means that packets are dropped if tcpdump cannot keep up with the speed. However, considering the low hardware prices nowadays, it is possible to monitor high bandwidth networks with tcpdump. One way of supporting networks with even higher bandwidths is to use load balancing techniques to split the traffic across several interfaces, which are easier to monitor on their own \cite{Schneider2007Apr}. One challenge is that it is not easily possible to run tcpdump in multiple locations on the network and merge the output of each instance at a central place. This can be solved by an alternation of our general concept: Instead of extracting the features from the merged traffic, multiple feature extraction instances can be run in parallel and the extracted features can then be merged based on the timestamp.

There are many tools that can be used to analyze pcap files to extract packet level features. These tools usually have the ability to exploit the high information content available at that level. Accordingly, the main criterion is not the data quality or quantity of features the tools can extract but the performance with which the tools can extract the features. Four tools are presented in the following and compared in terms of their extraction performance.

\begin{description}
  \item[pyshark]\footnote{https://github.com/KimiNewt/pyshark} is an open source python wrapper for tshark\footnote{https://www.wireshark.org/docs/man-pages/tshark.html}, the command-line version of Wireshark\footnote{https://www.wireshark.org/}. Wireshark is the most prominent tool to analyze network traffic on packet level, able to display all fields of a network packet and providing a feature rich graphical user interface \cite{Ndatinya2015Jan}. Tshark provides the same functionality as Wireshark only without the GUI. It can be used to analyze existing pcaps or live traffic using the libpcap library. Pyshark simplifies the usage of tshark by providing python syntax for the tshark commands. It was used by its authors in \cite{Berger2011} for real-time monitoring and e.g. in \cite{Li2017} for analyzing pcaps.

  \item[dpkt]\footnote{https://github.com/kbandla/dpkt} is an open source packet analyzing library for python. Unlike pyshark, dpkt does not natively provide a way to access network interfaces. However, with the help of the libpcap python wrapper pypcap\footnote{https://github.com/pynetwork/pypcap} dpkt can also analyze live traffic, as seen in \cite{Schut2015}.

  \item[scapy]\footnote{https://scapy.net/} is a python library for forging and dissecting network packets. Scapy uses the libpcap library under the hood so it is able to read pcap files as well as traffic directly from an network interface. Scapy was used in a variety of security related publications such as \cite{Dalky2014}, \cite{DAngelo2021Jan}, \cite{Hadhrami2020Jul} and \cite{Dheeraj2021}.

  \item[Pcapplusplus]\footnote{https://pcapplusplus.github.io/} In contrast to the other libraries mentioned, PcapPlusPlus (pcpp) is not written in python but C++. C++ is known for its high speed, especially compared to inherently slow scripting languages like python, so it's interesting to see if that translates into feature extraction performance. Pcpp uses libpcap and is therefore able to capture traffic in live mode as well as using pcap files as input. Pcpp was used in many publications such as \cite{Joshi2018Aug}, \cite{Lucia2019} and \cite{Dong2020Oct}.
\end{description}

\begin{lstlisting}[caption={Sample code for extracting packet level features with dpkt.},label={lst:label},language=Python]
import dpkt
import pcap  # pypcap

traffic = dpkt.pcap.Reader(open("files/test.pcap"))  # Read from pcap
traffic = pcap.pcap("eth0")  # Read from interface eth0

for timestamp, buffer in traffic:
  # Access Ethernet Layer
  eth = dpkt.ethernet.Ethernet(buffer)

  # Access Internet Layer
  ip = eth["ip"]

  # Extract Features
  src_mac = eth.src  # Source MAC address
  dst_mac = eth.dst  # Destination MAC address

  src_ip = ip.src  # Source IP address
  dst_ip = ip.dst  # Destination IP address

  src_port = ip.data.sport  # Source Port
  dst_port = ip.data.dport  # Destination Port

  # etc.
\end{lstlisting}


%As mentioned before, the ingestion component is the entry point to our system and there are quite a few matters to consider here. First of all, it is necessary to think about the way in which the traffic should be captured.

%As described in Sec. \ref{sec:foundation:anomaly_detection:challenges}, feature extraction is not a trivial task. The choice of the features significantly influences the success of a machine learning application.

We use a global constant for missing values \cite{Shopov2013Jan}.

%To simplify the integration, the user only needs to provide two methods: \texttt{train} and \texttt{predict}. The input for the \texttt{train} method is the pre-processed data, the output is the trained model. The input for the \texttt{predict} method is the previously trained model and the data to be evaluated, the output is a list of anomaly scores. Some algorithms, do not need any prior training, but only start during prediction on the basis of the data to be evaluated, to grasp the nature of the data and to detect any deviations. Such algorithms can also be integrated into our system. In this case, the \texttt{train} method simply returns an empty data structure and ignores the dummy model during prediction. Furthermore, by not requiring labels as input to the \texttt{train} method, we also satisfy requirement M3. Although our framework is intended for  unsupervised machine learning, the user still has the option to use (semi-)supervised algorithms if desired. For this, the user must know the name of the column with the labels and can then use its values for the supervised algorithm.

%\subsection{Data Collection}
%\label{sec:experiment:concept:data_collection}
%In the following, we will compare different technologies and select a suitable candidate that meets our requirements for the data collection component (see Table \ref{tab:input_requirements}). %The topic of the storage of the data is thematically directly related to that of the input. In this chapter, we will therefore also select a candidate that meets requirement \textit{R3}, which demands the scalability and performance of the storage solution.

%There are many approaches for data collection and in general we want to be agnostic towards the input data, meaning that the user should be able to easily change the way he ingests his data.

% In both architectures, the option of supervised and unsupervised learning is pointed out. However, Boutaba et al. highlight the fact that the choice of one of the models must be made directly at the beginning of the design process, since with a supervised or semi-supervised approach the labels must already be supplied during data collection.

%There are several factors to consider in data collection. First, the data quality must be sufficiently high. This refers on the one hand to an appropriate selection of features, which is not trivial as described in Sec. \ref{sec:foundation:anomaly_detection:challenges}. On the other hand, it is beneficial if the data is not too sparse, i.e. not too many features have null values. Furthermore, the solution must be able to accommodate the distributed nature of networks and support high bandwidths.

\neuerabsatz
\noindent
Es gibt diese und jenen Datenbanken...\neuerabsatz
\noindent
elasticsearch stack\footnote{https://www.elastic.co/de/elastic-stack}, explicitly designed for scalability and proved to be able to index over 20000 entries per second \cite{Akca2016Dec}.\neuerabsatz
\noindent
Packet level features are das und das. Gut/schlecht weil... Üblicherweise mittels eines TCP Dumps gesammelt und dann z.B. mit einem der folgenden Tools analysiert.

\begin{description}
  \item[Wireshark] ...
  \item[Scapy] ...
  \item[dpkt] ...
\end{description}

\neuerabsatz
\noindent
Flow level features are das und das. Gut/schlecht weil... Anderer Ansatz, mehrere Pakete mittels IP/Port gematcht.

\begin{description}
  \item[NetFlow] ...
  \item[packetbeat] ...
  \item[openargus] \begin{itemize}
    \item openargus\footnote{https://openargus.org/}
    \item Very scalable, can be used with both prerecorded traffic (pcaps) and live traffic (interfaces)
    \item has demonstrated its ability in dozens of publications
    \item has already been used, among others, by the European Union Agency for Cyber Security (ENISA) and for the creation of the state-of-the-art evaluation data set UNSW-NB15
    \item supports complex client server deployments with multiple clients and a multiplexer so that the distributed nature of networks can be accommodated
  \end{itemize}
\end{description}

\neuerabsatz
\noindent
We use openargus, weil... We aggregate the data periodically. In Fig. \ref{fig:data_collection} sieht man die Architektur.



%\subsection{Data Processing}
%\label{sec:experiment:concept:data_processing}
%Dimensionality reduction either explicitly or by the use of a model. e.g. a neural network, that performs dimensionality reduction by itself \cite{Kathareios2017}.

Scaling, Encoding, dimensionality reduction etc. Processing steps can be added by the user.

%\subsection{Model Training}
%\label{sec:experiment:concept:model_training}
We exploit the intrinsic plug and play nature of unsupervised machine learning algorithms \cite{Dromard2017}.

Either periodically performed training with schedules or incremental training with algorithm implementations provided e.g. by the library called PySAD by Yilmaz \cite{Yilmaz2020Sep}.

%\subsection{Inference}
%\label{sec:experiment:concept:inference}

Text Text Text.

%\subsection{Evaluation}
%\label{sec:experiment:concept:evaluation}

Kibana Plugin (elasticsearch) can be used for visual presentation of data/anomaly score. This way, possible correlations between the course of the features and the anomaly score can be visualized. Mention alternatives, etc..

%\subsection{System}
%\label{sec:experiment:concept:inference}

Portability $\rightarrow$ Docker (compose). "One Click deployment". Mention alternatives, etc..

%\section{Results}
%\label{sec:framework_design:results}

Hier steht das fertige mit Technologien versehene Framework.
