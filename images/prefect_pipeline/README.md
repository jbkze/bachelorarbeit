[![coverage report](https://git.da.dicos.de/anemo/gui/badges/master/coverage.svg)](https://git.da.dicos.de/anemo/gui/commits/master)

# ANEMO grafical user interface

Idea is to have one central place to configure the machine learning algorithms.

# Capabilities

* Run scheduled Docker containers
* Create or upload config files and config templates
* Login secured web front-end

# Operations

Before you can start, be sure to copy the anemo/pipeline to the proper location
```
.
├── build_context
│   ├── celery
│   │   ├── Dockerfile
│   │   └── src
│   ├── flask
│   │   ├── Dockerfile
│   │   └── src
│   └── pipeline
│       ├── backend.toml
│       ├── Dockerfile
│       └── src
│           ├── config
│           └── src
│               ├── flows
│               ├── helpers
│               ├── ml
│               ├── rpc
│               └── tasks
│  
├── create_prefect_defaults.sh
├── docker-compose-prefect-client.yml
├── docker-compose-prefect-server.yml
├── docker-compose.yml
└── README.md
```

## PRE-BUILD

Change settings in .env according to your needs
Change the following parameter to reflect your hostname / ip.
Must be reachable from outside.
```
APOLLO_URL=
```


## BUILD
```bash
docker-compose build
docker-compose -f docker-compose-prefect-client.yml build
```

## First start
```bash
# Start the flask app and Redis scheduler
docker-compose up -d
# Start the prefect server
docker-compose -f docker-compose-prefect-server.yml up -d
# Generate defaults for prefect server
./create_prefect_defaults.sh
# Start the prefect clients (rpyc and prefect worker)
docker-compose -f docker-compose-prefect-client.yml up -d
```

## GUIs

Prefect

    http://<hostname>:8080

Flask

    http://<hostname>:5500/login

## Test

You can start a test task from the docker host

```bash
docker container exec -it rpyc bash
python3
import rpyc
c = rpyc.connect("rpyc", 18861)
c.root.exposed_create_training_schedule()
```

# Login / Signup
* Use the `create_admin_user.py` to create a user which you can then use for login.

Account Settings:
* (Optional) Change your password
* (Optional) Create additional users
