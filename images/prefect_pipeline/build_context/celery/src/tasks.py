#!/usr/bin/env python
# encoding: utf-8
from datetime import timedelta
from celery import Celery
import docker

app = Celery('tasks', backend='redis://redis:6379',
             broker='redis://redis:6379')

# TODO: Check if we always need one task and if so, we need to make sure it is not deleted.
app.conf.update(
    CELERY_REDIS_SCHEDULER_URL='redis://redis:6379',
    CELERYBEAT_SCHEDULE={
        'heartbeat': {
            'task': 'tasks.add',
            'schedule': timedelta(seconds=3),
            'args': (1, 1)
        }
    }
)


# Dummy task for demostration purpose
@app.task
def add(x, y):
    return x + y


@app.task
def sub(x, y):
    return x - y


client = docker.from_env()

mount_conf = {'/opt/data/files': {'bind': '/opt/data/files', 'mode': 'rw'}}

@app.task
def training(args):
    container = client.containers.run('training', args, detach=True, volumes=mount_conf)
    return 'OK'  # TODO


@app.task
def prediction(args):
    container = client.containers.run('prediction', args, detach=True, volumes=mount_conf)
    return 'OK'  # TODO


@app.task
def stat_feat(args):
    container = client.containers.run('stat_feat', args, detach=True, volumes=mount_conf)
    return 'OK'  # TODO


@app.task
def argus(interface, ip, port):
    args = f"{interface} {ip} {port}"
    container = client.containers.run('argus', args, detach=True, volumes=mount_conf, network_mode='host')
    return 'OK'  # TODO


# Docker container handling:
@app.task
def run_container(container_name='hello-world'):
    container = client.containers.run(container_name, detach=True)
    return 'OK'  # TODO
