from flask import Flask
from flask_cors import CORS
from flask_bootstrap import Bootstrap

# User details will be stored in Database
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager

import config

from application.navigation_bar import navigation


db = SQLAlchemy()
login_manager = LoginManager()


def create_app(test=False):
    """Initialize the core application.
    Loads config from config.py.

    Args:
      test:  (Default value = False) Indicates if the app should load the test configuration.

    Returns:
        The Flask application. Can be used to run the app or for testing purposes.
    """
    app = Flask(__name__, instance_relative_config=False)

    app.config.from_object(config.TestingConfig if test else config.ProductionConfig)

    Bootstrap(app)
    CORS(app)

    # Initialize Plugins
    db.init_app(app)
    navigation.init_app(app)
    login_manager.init_app(app)

    with app.app_context():
        # Include our Routes
        from . import routes_configs
        from . import routes_schedules
        from . import routes_auth
        from . import routes_prefect

        db.create_all()

        # Register Blueprints
        app.register_blueprint(routes_configs.routes_configs, url_prefix='/configs')
        app.register_blueprint(routes_schedules.routes_schedules, url_prefix='/schedules')
        app.register_blueprint(routes_auth.routes_auth, url_prefix='/auth')
        app.register_blueprint(routes_prefect.routes_prefect, url_prefix='/')


        return app
