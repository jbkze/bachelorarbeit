from flask_wtf import FlaskForm
from wtforms import SubmitField, StringField, SelectField, FieldList, Form, FormField, validators, IntegerField, \
    FloatField

from ..helpers import BooleanSubField

available_encoding_options = [
    (None, "None"),
    ('ohe', "One-Hot"),
    ('hash', "Hash")
]

available_scaling_options = [
    (None, "None"),
    ('min_max', "Min-Max"),
    ('max_abs', "Max-Abs"),
    ('std', "Standard")
]

available_dimension_reduction_options = [
    (None, "None"),
    ('pca', "PCA"),
    ('vt', "Variance Threshold"),
]

available_bit_options = [
    (8, "8"),
    (16, "16"),
    (32, "32"),
    (64, "64"),
    (128, "128"),
]


# Responsible for Elasticsearch params
class FeatureConfigCreatorLoad(FlaskForm):
    ip_field = StringField('IP Address', default='141.100.70.175', validators=[validators.DataRequired()])
    port_field = StringField('Port', default='9200', validators=[validators.DataRequired()])
    user_field = StringField('User', default='admin', validators=[validators.DataRequired()])
    pass_field = StringField('Password', default='admin', validators=[validators.DataRequired()])
    index_field = StringField('Elaticsearch Index', default='logstash-data-openargus-2020*', validators=[validators.DataRequired()])

    exclude_field = StringField('Exclude Features', default=r'codisp|@.*')

    load_btn = SubmitField('Load')


# One single line in the config interface
class SingleFeatureConfigForm(Form):
    feat_name = StringField('Debug')

    use_field = BooleanSubField('Use')
    scale_field = SelectField('Scale', choices=available_scaling_options)
    enc_field = SelectField('Encode', choices=available_encoding_options)
    enc_config = SelectField('Bits', choices=available_bit_options)
    dim_red_field = SelectField('Dimension Reduction', choices=available_dimension_reduction_options)
    vt_field = FloatField('Variance Threshold')
    default_field = StringField('Default', default=0)



# Responsible for the configuration itself
class FeatureConfigCreatorConfigurator(FlaskForm):
    features = FieldList(FormField(SingleFeatureConfigForm))
    filename_field = StringField('Filename', default='feature_config.yml', validators=[validators.DataRequired()])

    generate_btn = SubmitField('Generate')
