from flask_wtf import FlaskForm
from flask_wtf.file import FileField
from wtforms import SubmitField, StringField, IntegerField


class FileUploadForm(FlaskForm):
    es_config_btn = FileField('Elasticsearch Config')
    feat_config_btn = FileField('Feature Config')
    stat_feat_config_btn = FileField('Statistical Feature Config')
    time_config_btn = FileField('Time Config')

    submit = SubmitField('Submit')


class ArgusForm(FlaskForm):
    interface = StringField("Network Interface", default="eth0")
    ip = StringField("IP Address", default="192.168.99.242")
    port = IntegerField("Port", default=561)

    running_status = StringField("Argus Status", default="Argus service is ...")

    submit = SubmitField('Submit')

