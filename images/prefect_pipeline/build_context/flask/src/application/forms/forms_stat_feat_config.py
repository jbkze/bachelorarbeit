from flask_wtf import FlaskForm
from wtforms import SubmitField, StringField, SelectField, FieldList, Form, FormField, validators
from wtforms.fields import html5 as h5fields

available_agg_types = [
    ("value_count", "Count"),
    ("cardinality", "Unique Count")
]


available_derivatives = [
    (None, "None"),
    (1, "First"),
    (2, "Second"),
    (3, "Third")
]


# Responsible for Elasticsearch params
class StatFeatureConfigCreatorLoad(FlaskForm):
    ip_field = StringField('IP Address', default='141.100.70.175', validators=[validators.DataRequired()])
    port_field = StringField('Port', default='9200', validators=[validators.DataRequired()])
    user_field = StringField('User', default='admin', validators=[validators.DataRequired()])
    pass_field = StringField('Password', default='admin', validators=[validators.DataRequired()])
    index_field = StringField('Elaticsearch Index', default='logstash-data-openargus-2020*',
                              validators=[validators.DataRequired()])

    exclude_field = StringField('Exclude Features', default=r'codisp|@.*')

    load_btn = SubmitField('Load')


# One single line in the config interface
class SingleStatFeatureConfigForm(Form):
    feat_name = StringField('New Feature Name')

    agg_type = SelectField("Aggregation Type", choices=available_agg_types)
    agg_field = SelectField('Aggregation Field')

    derivative_field = SelectField('Derivative', choices=available_derivatives)


# Responsible for the configuration itself
class StatFeatureConfigCreatorConfigurator(FlaskForm):
    interval = h5fields.IntegerField('Aggregation Interval')

    features = FieldList(FormField(SingleStatFeatureConfigForm))
    filename_field = StringField('Filename', default='stat_feature_config.yml', validators=[validators.DataRequired()])

    generate_btn = SubmitField('Generate')
