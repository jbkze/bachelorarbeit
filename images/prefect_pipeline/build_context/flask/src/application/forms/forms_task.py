from flask_wtf import FlaskForm
from wtforms import SubmitField, IntegerField, StringField, SelectField, BooleanField, validators
from wtforms.fields import html5 as h5fields


available_algorithms = [
    ('robust_random_cut_forest', 'Robust Random Cut Forest'),
    ('isolation_forest', 'Isolation Forest'),
    ('k_nearest_neighbor', 'K Nearest Neighbor'),
    ('geo_fence', 'Geo Fence'),
    ('novelty_detection', 'Novelty Detection')

]

available_prediction_modes = [
    ('live', 'Live'),
    ('historical', 'Historical')
]


class BaseAddTaskForm(FlaskForm):
    task_name = StringField('Task Name', [validators.DataRequired()])

    # TODO Add cron validator
    cron_job = StringField("Cron Job", default="*/5 * * * *", validators=[validators.DataRequired()])

    index = StringField('Elaticsearch Index', validators=[validators.DataRequired()])

    es_config = SelectField('Elasticsearch Config File', validators=[validators.DataRequired()])

    time_config = SelectField('Time Config File', validators=[validators.DataRequired()])


class TrainingTaskForm(BaseAddTaskForm):
    model_path = StringField('Model')  # StringField instead of SelectField because the model does not exist yet

    algorithm = SelectField('Machine Learning Algorithm', choices=available_algorithms, validators=[validators.DataRequired()])

    feat_config = SelectField('Feature Config File', validators=[validators.DataRequired()])

    submit = SubmitField('Submit')


class PredictionTaskForm(BaseAddTaskForm):
    dest_field = StringField('Anomaly Score field', validators=[validators.DataRequired()])

    model_path = SelectField('Model', validators=[validators.DataRequired()])

    inference_mode = SelectField('Prediction Mode', choices=available_prediction_modes, validators=[validators.DataRequired()])

    submit = SubmitField('Submit')


class StatisticalFeaturesForm(FlaskForm):
    task_name = StringField('Task Name', [validators.DataRequired()])

    cron_job = StringField("Cron Job", default="*/5 * * * *", validators=[validators.DataRequired()])

    flow_index = StringField('Flow Index', validators=[validators.DataRequired()])
    stat_index = StringField('Statistical Index', validators=[validators.DataRequired()])

    es_config = SelectField('Elasticsearch Config File', validators=[validators.DataRequired()])
    stat_feat_config = SelectField('Statistical Feature Config File', validators=[validators.DataRequired()])

    submit = SubmitField('Submit')
