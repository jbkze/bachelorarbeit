from flask_wtf import FlaskForm
from wtforms import SubmitField, StringField, SelectField, FieldList, Form, FormField, validators, BooleanField, \
    RadioField
from wtforms.fields.html5 import DateField, TimeField, IntegerField

from ..helpers import BooleanSubField

available_weekdays = [
    ("monday", "Monday"),
    ("tueday", "Tueday"),
    ("wednesday", "Wednesday"),
    ("thursday", "Thursday"),
    ("friday", "Friday"),
    ("saturday", "Saturday"),
    ("sunday", "Sunday")
]


# One single line in the config interface
class SingleTimeConfigForm(Form):
    weekday_name = StringField('Debug')

    use_field = BooleanSubField('Use', default=True)

    time_start = TimeField("Start", validators=[validators.DataRequired()])
    time_end = TimeField("End", validators=[validators.DataRequired()])


class TimeConfigCreator(FlaskForm):
    # Global config
    jinja = BooleanField("Jinja Template")

    date_start = DateField("Date Start", validators=[validators.DataRequired()])
    date_end = DateField("Date End", validators=[validators.DataRequired()])

    date_start_jinja = IntegerField("Date Start", default=30, validators=[validators.DataRequired()])
    date_end_jinja = StringField("Date End", default="Today", validators=[validators.DataRequired()])

    jinja_switch = BooleanField("Jinja Template")

    time_start_global = TimeField("Time Start", validators=[validators.DataRequired()])
    time_end_global = TimeField("Time End", validators=[validators.DataRequired()])

    working_days = BooleanField("Weekdays", default=True)
    holidays = BooleanField("Holidays", default=True)
    weekends = BooleanField("Weekends", default=True)

    # Weekday config
    weekdays = FieldList(FormField(SingleTimeConfigForm))

    filename_field = StringField('Filename', default='time_config.yml', validators=[validators.DataRequired()])

    submit_btn = SubmitField('Generate')
