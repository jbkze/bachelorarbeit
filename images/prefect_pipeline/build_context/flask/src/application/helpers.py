import os
from pathlib import Path
from os.path import relpath

import docker
import eland

from elasticsearch import Elasticsearch
from datetime import datetime, timedelta

from jinja2 import FileSystemLoader, Environment
from wtforms import BooleanField

from config import Config


class Timewarp:
    """Class to handle date stuff within a jinja2 template.
    Useful for the dynamic generation of time configs.
    """

    def __init__(self):
        self.now = datetime.today()

    def days_ago(self, days):
        """Calculates the date that lies the specified number of days in the past.

        Args:
          days: Number of days in the past.

        Returns:
          The date that lies the specified number of days in the past.
        """
        return (self.now - timedelta(days)).strftime("%Y-%m-%d")

    def today(self):
        """Returns the current date.

        Returns:
          Today's date.
        """
        return self.now.strftime("%Y-%m-%d")


class BooleanSubField(BooleanField):
    """BooleanField that should be used in FormFields forms.
    Needed because of an bug in BooleanField which makes BooleanField behave strangely in FormFields.
    See https://github.com/wtforms/wtforms/issues/308
    """

    def process_data(self, value):
        """Overrides process_data of BooleanField.
        process_data gets called twice in FormFields: one time the value of the BooleanField gets
        passed as a boolean built in (expected) and one time the BooleanField itself gets passed (unexpected).

        Args:
          value: Can be either the actual value of the BooleanField or the BooleanField itself.
        """
        if isinstance(value, BooleanField):
            # In case of a BooleanField
            self.data = value.data
        else:
            # In case of a boolean built in
            self.data = bool(value)


def get_file_extension(filename):
    """Returns the file extension of a given file.

    Args:
      filename: Path to a file.

    Returns:
        The file extension of the file path.
    """
    try:
        return Path(filename).suffix
    except:
        return ""


def join_path_filename(path, filename):
    """Joins a path with a filename.
    Useful because one does not have to deal with trailing slashes etc..

    Args:
      path: Path to a directory.
      filename: Name of the file.

    Returns:
        Joined path and filename.
    """
    return str(os.path.join(path, filename))


def abs_path_to_relative(dir, file):
    """Returns the path to a file, relative to a given directory.

    Args:
      dir: Absolute path to a directory.
      file: Absolute path to a file.

    Returns:
        The path to the file, relative to the directory.
    """
    return relpath(file, dir)


def list_files_in_dir(dir, as_tuple=False, relative_to_upload_folder=False):
    """

    Args:
      dir:
      as_tuple: (Default value = False)
      relative_to_upload_folder: (Default value = False)

    Returns:

    """
    all_filenames = [x.name for x in Path(dir).iterdir() if x.is_file()]
    all_abs_file_paths = [x for x in Path(dir).iterdir() if x.is_file()]

    if as_tuple:
        if relative_to_upload_folder:
            relative_paths = [abs_path_to_relative(Config.UPLOAD_FOLDER, file_path)
                              for file_path in all_abs_file_paths]
            return list(zip(relative_paths, all_filenames))
        else:
            return list(zip(all_abs_file_paths, all_filenames))
    else:
        return all_filenames


def get_feature_list(host, port, user, pwd, index):
    """Reads all available features/fields from elasticsearch.

    Args:
      host: The elasticsearch host.
      port: The elasticsearch port.
      user: The elasticsearch user.
      pwd: The elasticsearch password.
      index: A specific elasticsearch index or an index pattern.

    Returns:
        A sorted list of all feature names in the given index (pattern).
    """

    try:
        es_client = Elasticsearch(
            ['https://{}:{}@{}:{}'.format(user, pwd, host, port)],
            use_ssl=True,
            verify_certs=False,
        )

        proxy_df = eland.DataFrame(es_client=es_client,
                                   es_index_pattern=index)

        return sorted(proxy_df.columns.tolist())

    except Exception as e:  # TODO Exception handling
        print(e)
        return []


def get_feat_config_dir_path():
    """Creates the feature config directory and returns the respective path.

    Returns:
        The path to the feature config directory.
    """
    path = str(os.path.join(Config.UPLOAD_FOLDER, "feature_configs"))
    os.makedirs(path, exist_ok=True)
    return path


def get_stat_feat_config_dir_path():
    """Creates the statistical feature config directory and returns the respective path.

    Returns:
        The path to the statistical feature config directory.
    """
    path = str(os.path.join(Config.UPLOAD_FOLDER, "stat_feature_configs"))
    os.makedirs(path, exist_ok=True)
    return path


def get_time_config_dir_path():
    """Creates the time config directory and returns the respective path.

    Returns:
        The path to the time config directory.
    """
    path = str(os.path.join(Config.UPLOAD_FOLDER, "time_configs"))
    os.makedirs(path, exist_ok=True)
    return path


def get_es_config_dir_path():
    """Creates the elasticsearch directory and returns the respective path.

    Returns:
        The path to the elasticsearch config directory.
    """
    path = str(os.path.join(Config.UPLOAD_FOLDER, "es_configs"))
    os.makedirs(path, exist_ok=True)
    return path


def get_models_dir_path():
    """Creates the models directory and returns the respective path.

    Returns:
        The path to the models directory.
    """
    path = str(os.path.join(Config.UPLOAD_FOLDER, "models"))
    os.makedirs(path, exist_ok=True)
    return path


def get_global_config_dir_path():
    """Creates the global configs directory and returns the respective path.

    Returns:
        The path to the models directory.
    """
    path = str(os.path.join(Config.UPLOAD_FOLDER, "global_configs"))
    os.makedirs(path, exist_ok=True)
    return path


def delete_file(path):
    """Deletes a file.

    Args:
      path: Path to the file that gets deleted
    """
    os.remove(path)


def time_config_template_to_yml(jinja_path):
    """Transforms a jinja2 template to a time config in yml format.
    Saves the generated time config to the time configs folder.

    Args:
      jinja_path: Path to jinja2 template.

    Returns:
        Path to the newly created time config.
    """
    timewarp = Timewarp()

    templateLoader = FileSystemLoader(searchpath=f"{Config.UPLOAD_FOLDER}")
    env = Environment(loader=templateLoader)
    template = env.get_template(jinja_path, parent=None)

    new_fname = f'{jinja_path}.{timewarp.now.isoformat()}.yml'
    with open(f'{Config.UPLOAD_FOLDER}/{new_fname}', 'w') as fname:
        fname.write(template.render(time=timewarp))

    return new_fname


def container_status(container_name):
    """Checks if the container with the given name is running or not.

    Args:
      container_name: Name of the docker container.

    Returns:
        Running if the container is running or Stopped if the container is stopped or unknown.
    """
    try:
        client = docker.from_env()

        all_containers = client.containers.list()

        for container in all_containers:
            if container_name in str(client.images.get(container.attrs['Image'])):
                return "Running"

        return "Stopped"

    except Exception as e:
        return str(e)


if __name__ == '__main__':
    pass
