from dominate.tags import *
from flask_nav.elements import Navbar, View, Subgroup, Link
from flask_nav.renderers import Renderer
from flask_login import current_user
from flask_nav import Nav

navigation = Nav()


@navigation.navigation()
def mynavbar():
    """Creates the contents of the navigation bar.

    The returned Navbar is automatically registered as navigation bar, thanks to the @navigation.navigation decorator.

    Returns:
        An instance of Navbar containing all specified nodes.
    """
    return Navbar(
        'ANEMO Lobby',
        View('Configs', 'routes_configs.upload_file'),
        Subgroup(
            "Create Schedule",
            View('Training Schedules', 'routes_schedules.add_training'),
            View('Prediction Schedules', 'routes_schedules.add_prediction'),
            View('Statistical Features Schedules', 'routes_schedules.add_stat_feat')
        ),
        Subgroup(
            "Create Config",
            View('Feature Config', 'routes_configs.feature_config_creator'),
            View('Statistical Feature Config', 'routes_configs.stat_feature_config_creator'),
            View('Time Config', 'routes_configs.time_config_creator')
        ),
        View('Argus', 'routes_schedules.argus'),
        View('Prefect', 'routes_prefect.prefect'),
        Link("Kibana", "https://192.168.99.242/app/kibana#/dashboard/45d49680-1798-11eb-8dca-cd49a47acb27"),
        Subgroup(
            "Account",
            View('Change password', 'routes_auth.change_password') if current_user.is_authenticated else None,
            View('Add User', 'routes_auth.signup') if current_user.is_authenticated else None,
            View('Logout', 'routes_auth.logout') if current_user.is_authenticated
            else View('Login', 'routes_auth.login')
        ),
    )


@navigation.renderer()
class DicosNavBar(Renderer):
    """Custom Navbar Renderer.
    Generates a HTML structure similar to that of the navigation bar on the DICOS website.
    Useful for easier subsequent styling.
    This renderer is automatically registered as renderer, thanks to the @navigation.renderer decorator.

    To use the Renderer instead of the default renderer, you have to manually specify it in the Jinja2 template, e.g.:
        {{ nav.mynavbar.render(renderer='dicos_nav_bar') }}
    """

    def visit_Navbar(self, node):
        """Main entry point for the rendering of the navbar.
        Generates the HTML code for the navbar and all child nodes.
        Iterates over children and calls their generic visit methods which is internally dispatched to the
        respective visit methods (visit_Link, visit_View etc.)

        Args:
          node: The node in the navbar that gets rendered; in this case the navbar itself.

        Returns:
            The HTML code for the whole navbar
        """
        head = header(_class="banner")

        with head:
            with div(_class="container"):
                with a(_class="brand"):
                    img(src="https://www.dicos.de/wp-content/uploads/dicos_logo.svg", _class="logo")
                with nav(_class="main-menu"):
                    for item in node.items:
                        self.visit(item)

        return head

    def visit_Link(self, node):
        """Generates the HTML code for Links in the navbar.
        Almost the same as visit_View but only used for external links.

        Args:
          node: The link in the navbar that gets rendered.

        Returns:
            The text (link) of the node, capsuled in an "a" HTML tag.

        """
        return a(node.text, href=node.get_url())

    def visit_View(self, node):
        """Generates the HTML code for Views in the navbar.
        Views are the main navigation elements in the navbar.

        Args:
          node: The view in the navbar that gets rendered.

        Returns:
            The text (link) of the node, capsuled in an "a" HTML tag.

        """
        kwargs = {}
        if node.active:
            kwargs['_class'] = 'active'
        return a(node.text,
                 href=node.get_url(),
                 title=node.text,
                 **kwargs)

    def visit_Subgroup(self, node):
        """Generates the HTML code for Subgroups in the navbar.
        Subgroups have a title and a dropdown list.

        Args:
          node: The subgroup in the navbar that gets rendered.

        Returns:
            The text (link) of the node, capsuled in an "a" HTML tag.

        """
        group = div(_class="dropdown")

        with group:
            with button(node.title, _class="dropbtn"):
                span(_class="dropdown-toggle")

            with div(_class="dropdown-content"):
                for item in node.items:
                    self.visit(item)

        return group
