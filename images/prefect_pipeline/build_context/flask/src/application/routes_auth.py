"""Routes for user authentication."""
from flask import Blueprint, redirect, render_template, flash, request, session, url_for
from flask_login import login_required, logout_user, current_user, login_user
from .forms.forms_auth import LoginForm, SignupForm, ChangePassword
from .models import User, db
from . import login_manager

# Blueprint Configuration
routes_auth = Blueprint(
    'routes_auth', __name__,
    template_folder='templates',
    static_folder='static'
)


@routes_auth.route('/login', methods=['GET', 'POST'])
def login():
    """Log-in page for registered users.

    GET requests serve Log-in page.
    POST requests validate and redirect user to dashboard.
    """
    # Bypass if user is logged in
    if current_user.is_authenticated:
        return redirect(url_for('routes_configs.upload_file'))

    form = LoginForm()
    # Validate login attempt
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and user.check_password(password=form.password.data):
            login_user(user)
            next_page = request.args.get('next')
            return redirect(next_page or url_for('routes_configs.upload_file'))
        flash('Invalid username/password combination')
        return redirect(url_for('routes_auth.login'))
    return render_template(
        'form_auth_login.html',
        form=form,
        title='Login'
    )


@routes_auth.route('/signup', methods=['GET', 'POST'])
@login_required
def signup():
    """User sign-up page.

    GET requests serve sign-up page.
    POST requests validate form & user creation.
    """
    form = SignupForm()
    if form.validate_on_submit():
        existing_user = User.query.filter_by(email=form.email.data).first()
        if existing_user is None:
            user = User(
                name=form.name.data,
                email=form.email.data,
            )
            user.set_password(form.password.data)
            db.session.add(user)
            db.session.commit()  # Create new user
            login_user(user)  # Log in as newly created user
            return redirect(url_for('routes_configs.upload_file'))
        flash('A user already exists with that email address.')
    return render_template(
        'form_auth_signup.html',
        title='Create an <strong>Account</strong>',
        form=form
    )


@routes_auth.route('/change_password', methods=['GET', 'POST'])
@login_required
def change_password():
    """Change password of current user.

    GET requests serve change-password page.
    POST requests validate change-password form.
    """
    form = ChangePassword()
    if form.validate_on_submit():
        user = User.query.filter_by(id=current_user.id).first()
        if user and user.check_password(password=form.old_password.data):
            user.set_password(form.new_password.data)
            db.session.add(user)
            db.session.commit()  # Update user
            login_user(user)  # Log updated user
            return redirect(url_for('routes_configs.upload_file'))
        flash('Invalid password')
        return redirect(url_for('routes_auth.change_password'))
    return render_template(
        'form_auth_change_password.html',
        title='Update <strong>Password</strong>',
        form=form
    )


@routes_auth.route("/logout")
@login_required
def logout():
    """User log-out logic."""
    logout_user()
    return redirect(url_for('routes_auth.login'))


@login_manager.user_loader
def load_user(user_id):
    """Check if user is logged-in on every page load."""
    if user_id is not None:
        return User.query.get(user_id)
    return None


@login_manager.unauthorized_handler
def unauthorized():
    """Redirect unauthorized users to Login page."""
    flash('You must be logged in to view that page.')
    return redirect(url_for('routes_auth.login'))
