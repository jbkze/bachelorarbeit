import re

import yaml
from flask import Blueprint, jsonify, request, render_template, current_app, redirect, url_for, send_from_directory, \
    send_file
from flask_login import current_user, login_required

# current_app will only work during a request and is here used to access its logger.
from .forms.forms_feat_config import *
from .forms.forms_misc import *
from .forms.forms_stat_feat_config import *
from .forms.forms_time_config import *
from .helpers import *
from .exceptions import *

routes_configs = Blueprint('routes_configs', __name__, template_folder='templates', static_folder='static')


@routes_configs.route('/uploadFile', methods=['GET', 'POST'])
@login_required
def upload_file():
    """Page where user can upload and manage various config files.

    GET requests serve upload-config page.
    POST requests upload files which are then saved in their respective directories.
    """
    form = FileUploadForm()
    if request.method == 'POST':
        es_config = request.files.get('es_config_btn', None)
        feat_config = request.files.get('feat_config_btn', None)
        time_config = request.files.get('time_config_btn', None)
        stat_feat_config = request.files.get('stat_feat_config_btn', None)

        if es_config is not None:
            es_config.save(join_path_filename(get_es_config_dir_path(), es_config.filename))
        if feat_config is not None:
            feat_config.save(join_path_filename(get_feat_config_dir_path(), feat_config.filename))
        if time_config is not None:
            time_config.save(join_path_filename(get_time_config_dir_path(), time_config.filename))
        if stat_feat_config is not None:
            stat_feat_config.save(join_path_filename(get_stat_feat_config_dir_path(), stat_feat_config.filename))

        return redirect(url_for('routes_configs.upload_file'))

    elif request.method == 'GET':
        es_config_files = list_files_in_dir(get_es_config_dir_path(), as_tuple=True, relative_to_upload_folder=True)
        feat_config_files = list_files_in_dir(get_feat_config_dir_path(), as_tuple=True, relative_to_upload_folder=True)
        time_config_files = list_files_in_dir(get_time_config_dir_path(), as_tuple=True, relative_to_upload_folder=True)
        stat_feat_config_files = list_files_in_dir(get_stat_feat_config_dir_path(), as_tuple=True, relative_to_upload_folder=True)

        return render_template('form_misc_file_upload.html',
                               title="Upload <strong>Config Files</strong>",
                               es_config_files=es_config_files,
                               feat_config_files=feat_config_files,
                               stat_feat_config_files=stat_feat_config_files,
                               time_config_files=time_config_files,
                               form=form)


@routes_configs.route('/feature_config', methods=['GET', 'POST'])
@login_required
def feature_config_creator():
    """Page where user can create feature config files.
    Loads all feature names from elasticsearch.

    GET requests serve create-feature-config page.
    POST requests create a feature config yml file based on the user input.
    """
    load_form = FeatureConfigCreatorLoad()
    config_form = FeatureConfigCreatorConfigurator()

    if request.method == 'POST':
        if request.form.get('load_btn') == "Load":
            host = request.form.get('ip_field')
            port = request.form.get('port_field')
            user = request.form.get('user_field')
            pwd = request.form.get('pass_field')
            index = request.form.get('index_field')

            exclude_expression = request.form.get('exclude_field')
            exclude_expression = exclude_expression if exclude_expression != "" else '^$'
            exclude_expression = re.compile(rf'{exclude_expression}')  # TODO I guess that's not safe at all

            feat_list_unfiltered = get_feature_list(host, port, user, pwd, index)
            feat_list = [i for i in feat_list_unfiltered if not exclude_expression.search(i)]

            for name in feat_list:
                feature_config_form = SingleFeatureConfigForm()
                feature_config_form.feat_name = name

                config_form.features.append_entry(feature_config_form)

            allow_generate = True if len(feat_list) > 0 else False

            return render_template('form_config_feature.html',
                                   form=load_form,
                                   config_form=config_form,
                                   allow_generate=allow_generate,
                                   title="Create <strong>Feature Config</strong>")

        elif request.form.get('generate_btn') == "Generate":
            yaml_dict = {}

            filename = request.form.get('filename_field')

            for entry in config_form.features.entries:
                yaml_dict[entry.data['feat_name']] = {}
                yaml_dict[entry.data['feat_name']]['use'] = entry.data['use_field']
                yaml_dict[entry.data['feat_name']]['default'] = entry.data['default_field']
                yaml_dict[entry.data['feat_name']]['scale'] = entry.data['scale_field'] if entry.data['scale_field'] != "None" else False
                yaml_dict[entry.data['feat_name']]['enc'] = entry.data['enc_field'] if entry.data['enc_field'] != "None" else False

                if entry.data['enc_field'] != "None":
                    yaml_dict[entry.data['feat_name']]['n_components'] = entry.data['enc_config']

                yaml_dict[entry.data['feat_name']]['dim_red'] = entry.data['dim_red_field'] if entry.data['dim_red_field'] != "None" else False

                if entry.data['dim_red_field'] != "None":
                    if entry.data['dim_red_field'] == "vt":
                        yaml_dict[entry.data['feat_name']]['vt_config'] = float(entry.data['vt_field'])

            full_path = join_path_filename(get_feat_config_dir_path(), filename)

            with open(full_path, 'w') as file:
                yaml.dump(yaml_dict, file, default_flow_style=False)

            return send_file(full_path, as_attachment=True)

    elif request.method == 'GET':
        return render_template('form_config_feature.html',
                               form=load_form,
                               config_form=config_form,
                               allow_generate=False,
                               title="Create <strong>Feature Config</strong>")


@routes_configs.route('/stat_feature_config', methods=['GET', 'POST'])
@login_required
def stat_feature_config_creator():
    """Page where user can create statistical feature config files.
    Loads all feature names from elasticsearch.

    GET requests serve create-stat-feature-config page.
    POST requests create a statistical feature config yml file based on the user input.
    """
    load_form = StatFeatureConfigCreatorLoad()
    config_form = StatFeatureConfigCreatorConfigurator()

    feat_list = []

    if request.method == 'POST':
        if request.form.get('load_btn') == "Load":
            host = request.form.get('ip_field')
            port = request.form.get('port_field')
            user = request.form.get('user_field')
            pwd = request.form.get('pass_field')
            index = request.form.get('index_field')

            exclude_expression = request.form.get('exclude_field')
            exclude_expression = exclude_expression if exclude_expression != "" else '^$'
            exclude_expression = re.compile(rf'{exclude_expression}')  # TODO I guess that's not safe at all

            feat_list_unfiltered = get_feature_list(host, port, user, pwd, index)
            feat_list = [i for i in feat_list_unfiltered if not exclude_expression.search(i)]

            feature_config_form = SingleStatFeatureConfigForm()
            feature_config_form.feat_name = ""

            config_form.features.append_entry(feature_config_form)

            for form in config_form.features:
                form.agg_field.choices = [(feat, feat) for feat in feat_list]

            allow_generate = True if len(feat_list) > 0 else False

            return render_template('form_configs_stat_feat.html',
                                   form=load_form,
                                   config_form=config_form,
                                   allow_generate=allow_generate,
                                   title="Create <strong>Statistical Feature Config</strong>")

        elif request.form.get('generate_btn') == "Generate":
            yaml_dict = {}
            yaml_dict['stat_features'] = {}

            yaml_dict['interval'] = 'minute'

            filename = request.form.get('filename_field')

            for entry in config_form.features.entries:
                feat_name = entry.data['feat_name']
                yaml_dict['stat_features'][feat_name] = {}
                yaml_dict['stat_features'][feat_name]['aggregation'] = {}
                yaml_dict['stat_features'][feat_name]['aggregation']['type'] = entry.data['agg_type']
                yaml_dict['stat_features'][feat_name]['aggregation']['field'] = entry.data['agg_field']

                if entry.data['derivative_field'] != "None":
                    yaml_dict['stat_features'][feat_name]['derivative'] = int(entry.data['derivative_field'])

            full_path = join_path_filename(get_stat_feat_config_dir_path(), filename)

            with open(full_path, 'w') as file:
                yaml.dump(yaml_dict, file, default_flow_style=False)

            return send_file(full_path, as_attachment=True)

    elif request.method == 'GET':
        return render_template('form_configs_stat_feat.html',
                               form=load_form,
                               config_form=config_form,
                               allow_generate=False,
                               title="Create <strong>Statistical Feature Config</strong>")


@routes_configs.route('/time_config', methods=['GET', 'POST'])
@login_required
def time_config_creator():
    """Page where user can create time config files.

    GET requests serve create-time-config page.
    POST requests create a time config yml file based on the user input.
    """
    form = TimeConfigCreator()

    weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]

    for name in weekdays:
        time_config_form = SingleTimeConfigForm()
        time_config_form.weekday_name = name

        form.weekdays.append_entry(time_config_form)

    if request.method == 'POST':
        yaml_dict = {}

        filename = request.form.get('filename_field')

        yaml_dict['date'] = {}
        if 'date_start' in request.form:
            yaml_dict['date']['start'] = request.form.get('date_start')
        if 'date_end' in request.form:
            yaml_dict['date']['end'] = request.form.get('date_end')
        if 'date_start_jinja' in request.form:
            yaml_dict['date']['start'] = "{{ time.days_ago(%s) }}" % (request.form.get('date_start_jinja'))
        if 'date_end_jinja' in request.form:
            yaml_dict['date']['end'] = "{{ time.today() }}"

        yaml_dict['time'] = {}
        yaml_dict['time']['start'] = str(request.form.get('time_start_global'))
        yaml_dict['time']['end'] = str(request.form.get('time_end_global'))

        yaml_dict['tags'] = {}
        yaml_dict['tags']['working_days'] = True if request.form.get('working_days') == "y" else False
        yaml_dict['tags']['holidays'] = True if request.form.get('holidays') == "y" else False
        yaml_dict['tags']['weekends'] = True if request.form.get('weekends') == "y" else False

        yaml_dict['weekdays'] = {}

        for entry in form.weekdays.entries:
            weekday_name = entry.data['weekday_name'].lower()

            yaml_dict['weekdays'][weekday_name] = {}
            yaml_dict['weekdays'][weekday_name]['use'] = entry.data['use_field']

            #yaml_dict['weekdays'][weekday_name]['time'] = {}
            #yaml_dict['weekdays'][weekday_name]['time']['start'] = str(entry.data['time_start']._value)  # Does not work
            #yaml_dict['weekdays'][weekday_name]['time']['start'] = str(entry.data['time_start'].strftime('%H:%M'))  # Does not work
            #yaml_dict['weekdays'][weekday_name]['time']['end'] = str(dir(entry.data['time_end']))

        full_path = join_path_filename(get_time_config_dir_path(), filename)

        with open(full_path, 'w') as file:
            yaml.dump(yaml_dict, file, default_flow_style=False)

        return send_file(full_path, as_attachment=True)

    elif request.method == 'GET':
        return render_template('form_configs_time.html',
                               form=form,
                               title="Create <strong>Time Config</strong>")


@routes_configs.route('/apiVersion', methods=['GET'])
def get_api_version():
    """Return API version.

    GET requests return the API version.
    """
    return jsonify(current_app.config['API_VERSION'])


# TODO Currently not working bc of new folder structure
@routes_configs.route('/download')
@login_required
def download():
    filename = request.args.get("filename")
    return send_from_directory(current_app.config['UPLOAD_FOLDER'], filename, as_attachment=True)


# TODO Currently not working bc of new folder structure
@routes_configs.route('/delete')
@login_required
def delete():
    filename = request.args.get("filename")
    os.remove(os.path.join(current_app.config['UPLOAD_FOLDER'], filename))
    return redirect(url_for('routes_configs.upload_file'))


# This is how error handler for the API are definded.
@routes_configs.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


@routes_configs.errorhandler(KeyError)
def handle_key_error(error):
    message = "{}: {}".format(error.__class__.__name__, error)
    current_app.logger.warn(message)
    response = jsonify({'message': message})
    response.status_code = 400
    return response


@routes_configs.errorhandler(Exception)
def handle_error(error):
    """Handle all unhandled exceptions during execution.
    Logs to default application logger.

    Args:
      error: error that is raised from within the application

    Returns:
      error 500
    """
    try:
        # We try to log any log message to get most info for debugging
        current_app.logger.error("{}: {}".format(error.__class__.__name__, error.message))
    except Exception:
        # We always log the name of the error that occurred
        current_app.logger.error("{}: {}".format(error.__class__.__name__, error))
    finally:
        response = jsonify({'message': 'Unhandled Exception occurred: {}'.format(error.__class__.__name__)})
        response.status_code = 500
        return response
