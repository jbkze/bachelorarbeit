import re
import yaml
import requests

from urllib.parse import urlparse, urlunparse
from flask import Blueprint, jsonify, request, render_template, current_app, redirect, url_for, send_from_directory, \
    send_file, abort, Response, redirect
from flask_login import current_user, login_required

# current_app will only work during a request and is here used to access its logger.
from .forms.forms_feat_config import *
from .forms.forms_misc import *
from .forms.forms_stat_feat_config import *
from .forms.forms_time_config import *
from .helpers import *
from .exceptions import *

routes_prefect = Blueprint('routes_prefect', __name__, template_folder='templates', static_folder='static')


APPROVED_HOSTS = set(["google.com"])
CHUNK_SIZE = 1024


@routes_prefect.route('/<path:url>', methods=["GET", "POST"])
@routes_prefect.route('/prefect', methods=["GET", "POST"])
@login_required
def prefect(*args, **kwargs):
    url = request.url.replace(request.host_url, 'http://ui:8080/')
    url = url.replace('/prefect/', '/')

    resp = requests.request(
        method=request.method,
        url=url,
        headers={key: value for (key, value) in request.headers if key != 'Host'},
        data=request.get_data(),
        cookies=request.cookies,
        allow_redirects=False)

    excluded_headers = ['content-encoding', 'content-length', 'transfer-encoding', 'connection']
    headers = [(name, value) for (name, value) in resp.raw.headers.items()
               if name.lower() not in excluded_headers]

    response = Response(resp.content, resp.status_code, headers)
    return response




@routes_prefect.route('/apiVersion', methods=['GET'])
def get_api_version():
    """Return API version.

    GET requests return the API version.
    """
    return jsonify(current_app.config['API_VERSION'])


@routes_prefect.errorhandler(Exception)
def handle_error(error):
    """Handle all unhandled exceptions during execution.
    Logs to default application logger.

    Args:
      error: error that is raised from within the application

    Returns:
      error 500
    """
    try:
        # We try to log any log message to get most info for debugging
        current_app.logger.error("{}: {}".format(error.__class__.__name__, error.message))
    except Exception:
        # We always log the name of the error that occurred
        current_app.logger.error("{}: {}".format(error.__class__.__name__, error))
    finally:
        response = jsonify({'message': 'Unhandled Exception occurred: {}'.format(error.__class__.__name__)})
        response.status_code = 500
        return response

