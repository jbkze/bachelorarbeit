import yaml
import glob
import re
import rpyc

from munch import Munch

from flask import Blueprint, request, render_template, current_app, redirect, url_for, send_file
from flask_login import login_required
# current_app will only work during a request and is here used to access its logger.
from jinja2 import Environment, FileSystemLoader

from datetime import timedelta
from redisbeat.scheduler import RedisScheduler
from celery import Celery
from celery.schedules import crontab

from .forms.forms_feat_config import SingleFeatureConfigForm, FeatureConfigCreatorConfigurator, FeatureConfigCreatorLoad
from .forms.forms_misc import ArgusForm
from .forms.forms_stat_feat_config import StatFeatureConfigCreatorLoad, StatFeatureConfigCreatorConfigurator, \
    SingleStatFeatureConfigForm
from .forms.forms_task import PredictionTaskForm, TrainingTaskForm, StatisticalFeaturesForm
from .forms.forms_time_config import TimeConfigCreator, SingleTimeConfigForm
from .helpers import *

from config import RedisConfig


# TODO Test config for redis
try:
    app = Celery('tasks', backend=f'redis://{RedisConfig.HOST}:{RedisConfig.PORT}',
                 broker=f'redis://{RedisConfig.HOST}:{RedisConfig.PORT}')
    app.conf.update(
        CELERY_REDIS_SCHEDULER_URL=f'redis://{RedisConfig.HOST}:{RedisConfig.PORT}'
    )
    schduler = RedisScheduler(app=app)

except:
    RedisConfig.HOST = "127.0.0.1"
    app = Celery('tasks', backend=f'redis://{RedisConfig.HOST}:{RedisConfig.PORT}',
                 broker=f'redis://{RedisConfig.HOST}:{RedisConfig.PORT}')
    app.conf.update(
        CELERY_REDIS_SCHEDULER_URL=f'redis://{RedisConfig.HOST}:{RedisConfig.PORT}'
    )
    schduler = RedisScheduler(app=app)


routes_schedules = Blueprint('routes_schedules', __name__, template_folder='templates')


def parse_request(request, relevant_keys):
    task_name = request.form.get('task_name')
    #task_interval = int(request.form.get('task_interval'))

    cron_job = request.form.get('cron_job')

    if cron_job == "":
        cron_job = None

    data = dict(request.form)

    if 'time_config' in data:
        time_config_filename = data['time_config']

        if get_file_extension(time_config_filename) == ".j2":
            time_config_filename = time_config_template_to_yml(time_config_filename)
            data['time_config'] = time_config_filename  # Override j2 path with new yml path

    if 'model_path' in data:
        model_path = data['model_path']

        data['model_path'] = join_path_filename(get_models_dir_path(), model_path)

    new_data = {k: data[k] for k in relevant_keys if k in data}

    return task_name, cron_job, new_data


def add_periodic_task(name: str, task: str, cronjob: crontab, args: list):
    schduler.add(**{
        'name': name,
        'task': task,
        #'schedule': timedelta(seconds=interval),
        'schedule': cronjob,
        'args': args
    })


def add_onetime_task(task: str, args: dict):
    app.send_task(task, kwargs=args)


def list_schedules(task_name):
    all_schedules = schduler.list()
    json = [{'name': x.name, 'task': x.task, 'args': x.args, 'cronjob': x.schedule} for x in
            all_schedules if x.task == task_name]
    return json


def save_args_to_yaml(args: dict):
    filename = f"global_config.{datetime.now().isoformat()}.yml"
    full_path = join_path_filename(get_global_config_dir_path(), filename)
    with open(full_path, 'w') as file:
        yaml.dump(args, file, default_flow_style=False)

    return full_path


@routes_schedules.route('/add_training', methods=['GET', 'POST'])
@login_required
def add_training():
    form = TrainingTaskForm()
    # Add dynamic choices
    available_feat_config_files = list_files_in_dir(get_feat_config_dir_path(), as_tuple=True)
    available_time_config_files = list_files_in_dir(get_time_config_dir_path(), as_tuple=True)
    available_es_config_files = list_files_in_dir(get_es_config_dir_path(), as_tuple=True)

    form.feat_config.choices = available_feat_config_files
    form.time_config.choices = available_time_config_files
    form.es_config.choices = available_es_config_files

    if request.method == 'POST':
        relevant_keys = ['index', 'algorithm', 'es_config', 'feat_config', 'time_config', 'model_path']
        task_name, cron_job, args = parse_request(request, relevant_keys)

        # Connect to rpyc
        c = rpyc.connect("rpyc", 18861)

        c.root.exposed_create_training_schedule(task_name=task_name,
                                                es_config_path=args["es_config"],
                                                index=args["index"],
                                                ml_algorithm=args["algorithm"],
                                                feature_config_path=args["feat_config"],
                                                time_config_path=args["time_config"],
                                                model_path=args["model_path"],
                                                cron=cron_job
                                                )

        return redirect(url_for('routes_schedules.add_training'))

    elif request.method == 'GET':
        all_schedules = list_schedules('tasks.training')
        return render_template('form_schedules_training.html', schedules=all_schedules, form=form,
                               title="Add <strong>Training Schedule</strong>")


@routes_schedules.route('/add_prediction', methods=['GET', 'POST'])
@login_required
def add_prediction():
    form = PredictionTaskForm()
    # Add dynamic choices
    available_time_config_files = list_files_in_dir(get_time_config_dir_path(), as_tuple=True)
    available_es_config_files = list_files_in_dir(get_es_config_dir_path(), as_tuple=True)
    available_model_files = list_files_in_dir(get_models_dir_path(), as_tuple=True)

    form.time_config.choices = available_time_config_files
    form.es_config.choices = available_es_config_files
    form.model_path.choices = available_model_files

    if request.method == 'POST':

        relevant_keys = ['index', 'algorithm', 'es_config', 'feat_config', 'time_config', 'model_path',
                         'inference_mode', 'dest_field']
        task_name, cron_job, args = parse_request(request, relevant_keys)

        # Connect to rpyc
        c = rpyc.connect("rpyc", 18861)

        if args['inference_mode'] == "live":
            c.root.exposed_create_live_prediction_schedule(task_name=task_name,
                                                           es_config_path=args["es_config"],
                                                           index=args["index"],
                                                           model_path=args["model_path"],
                                                           anomaly_score_field=args["dest_field"],
                                                           cron=cron_job
                                                           )
        else:
            c.root.exposed_create_historical_prediction_schedule(task_name=task_name,
                                                                 es_config_path=args["es_config"],
                                                                 index=args["index"],
                                                                 time_config_path=args["time_config"],
                                                                 model_path=args["model_path"],
                                                                 anomaly_score_field=args["dest_field"],
                                                                 cron=cron_job
                                                                 )

        return redirect(url_for('routes_schedules.add_prediction'))

    elif request.method == 'GET':
        all_schedules = list_schedules('tasks.prediction')

        return render_template('form_schedules_prediction.html', schedules=all_schedules, form=form,
                               title="Add <strong>Prediction Schedule</strong>")


@routes_schedules.route('/add_stat_feat', methods=['GET', 'POST'])
@login_required
def add_stat_feat():
    form = StatisticalFeaturesForm()
    # Add dynamic choices
    available_stat_feat_config_files = list_files_in_dir(get_stat_feat_config_dir_path(), as_tuple=True)
    available_es_config_files = list_files_in_dir(get_es_config_dir_path(), as_tuple=True)

    form.stat_feat_config.choices = available_stat_feat_config_files
    form.es_config.choices = available_es_config_files

    if request.method == 'POST':

        relevant_keys = ['flow_index', 'stat_index', 'es_config', 'stat_feat_config']
        task_name, cron_job, args = parse_request(request, relevant_keys)


        # Connect to rpyc
        c = rpyc.connect("rpyc", 18861)

        c.root.exposed_create_statistical_features_schedule(task_name=task_name,
                                                            es_config_path=args["es_config"],
                                                            flow_index=args["flow_index"],
                                                            stat_index=args["stat_index"],
                                                            stat_feat_config_path=args["stat_feat_config"],
                                                            cron=cron_job
                                                            )
        
        return redirect(url_for('routes_schedules.add_stat_feat'))

    elif request.method == 'GET':
        all_schedules = list_schedules('tasks.stat_feat')

        return render_template('form_schedules_stat_feat.html', schedules=all_schedules, form=form,
                               title="Add <strong>Statistical Features Schedule</strong>")


@routes_schedules.route('/argus', methods=['GET', 'POST'])
@login_required
def argus():
    form = ArgusForm()
    form.running_status.data = container_status("argus")

    if request.method == 'POST':
        interface = request.form.get('interface')
        ip = request.form.get('ip')
        port = request.form.get('port')

        args = dict(interface=interface, ip=ip, port=port)

        add_onetime_task("tasks.argus", args)

        return redirect(url_for('routes_schedules.argus'))

    elif request.method == 'GET':
        return render_template('form_misc_argus.html',
                               title="Start <strong>Argus</strong>",
                               form=form)


@routes_schedules.route('/delete/', defaults={'schedule': 'default'})
@routes_schedules.route('/delete/<schedule>')
@login_required
def delete(schedule):
    result = schduler.remove(schedule)
    print("Deleted Schedule: " + schedule)
    # TODO: Error handling
    # TODO: Return to add_training OR add_prediction
    return redirect(url_for('routes_schedules.add_training'))
