function enable(select_field, input_field, value_when_enable) {
    var e = document.getElementById(select_field);
    var strChoice = e.options[e.selectedIndex].value;

    document.getElementById(input_field).disabled = strChoice !== value_when_enable;
}

function time_config_jinja() {
    var use_jinja = document.getElementById("jinja_switch").checked;

    if (use_jinja) {
        document.getElementById("date_start_jinja").disabled = false
        document.getElementById("date_end_jinja").disabled = false
        document.getElementById("date_start").disabled = true
        document.getElementById("date_end").disabled = true
    } else {
        document.getElementById("date_start_jinja").disabled = true
        document.getElementById("date_end_jinja").disabled = true
        document.getElementById("date_start").disabled = false
        document.getElementById("date_end").disabled = false
    }
}

function showfilename(id) {
    let input = document.getElementById(id);
    let label = input.nextElementSibling,
        labelVal = label.innerHTML;

    let fileName = '';
    fileName = input.value.replace(/^.*[\\\/]/, '')

    if (fileName)
        label.querySelector('span').innerHTML = fileName;
    else
        label.innerHTML = labelVal;
}

function add_row(table_id) {
    var tbodyRef = document.getElementById(table_id).getElementsByTagName('tbody')[0];

    var rows = tbodyRef.getElementsByTagName("tr");
    var new_row = rows[rows.length - 1].cloneNode(true)

    var ths = new_row.getElementsByTagName("th");

    for (var i = 0; i < ths.length; i++) {
        var child = ths[i].childNodes[0]
        oldID = child.id;
        child.id = oldID.replace(/(\d+)+/g, function (match, number) {
            return parseInt(number) + 1;
        });

        oldName = child.name;
        child.name = oldName.replace(/(\d+)+/g, function (match, number) {
            return parseInt(number) + 1;
        });

        var input = new_row.getElementsByTagName("input")[0].value = "";
    }

    tbodyRef.append(new_row)
}

function set_time_range_defaults(input_id, table_id) {
    let input = document.getElementById(input_id);
    let value = input.value

    let table = document.getElementById(table_id).getElementsByTagName('tbody')[0];
    let rows = table.getElementsByTagName("tr");

    for (var i = 0; i < rows.length; i++) {
        let inputs = rows[i].getElementsByTagName('input')

        for (var j = 0; j < inputs.length; j++) {
            if (inputs[j].id.includes(input_id) && !inputs[j].className.includes("edited")) {

                inputs[j].value = value
            }
        }
    }
}