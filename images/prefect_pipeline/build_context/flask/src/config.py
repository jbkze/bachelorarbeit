class Config(object):
    # FLASK #
    PORT = 5000
    HOST = '0.0.0.0'

    DEBUG = False
    TESTING = False

    SECRET_KEY = 'asdfjreatio983cvasdf495w5ZUEaQ14'

    # DATABASE #
    SQLALCHEMY_DATABASE_URI = 'sqlite:///:memory:'
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # MISC #
    API_VERSION = '1'
    UPLOAD_FOLDER = '/opt/data/files'  # Mounted to docker volume


class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'sqlite:////src/anemogui.db'


class TestingConfig(Config):
    TESTING = True
    DEBUG = True

    WTF_CSRF_ENABLED = False

class RedisConfig:
    HOST = 'redis'
    PORT = 6379
