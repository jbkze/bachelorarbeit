#!/usr/bin/env python
"""Create a new admin user."""
from getpass import getpass
import sys


from application import create_app
from application.models import User, db

app = create_app()

def main():
    """Main entry point for script."""
    with app.app_context():

        db.metadata.create_all(db.engine)
        if User.query.all():
            print('A user already exists! Create another? (y/n):')
            create = input()
            if create == 'n':
                return

        print('Name: ')
        name = input()
        print('Email: ')
        email = input()
        password = getpass()
        assert password == getpass('Password (again):')

        user = User(
            name=name,
            email=email, 
        )
        user.set_password(password)
        db.session.add(user)
        db.session.commit()
        print('User added.')


if __name__ == '__main__':
    sys.exit(main())

