import io
import json
import os

from sqlalchemy import func
from flask import request
import pytest

from flask_login import current_user

import application
from application import helpers

from application.models import User, db

###############################
########## CONSTANTS ##########
###############################

prefix_configs = '/configs'
prefix_schedules = '/schedules'
prefix_auth = '/auth'

name = "test_user"
email = "user@test.de"
password = "test_pass123"


##############################
###### HELPER FUNCTIONS ######
##############################
def get_dummy_user():
    user = User(
        name=name,
        email=email,
    )
    user.set_password(password)

    return user


def add_dummy_user_to_db():
    db.metadata.create_all(db.engine)

    user = get_dummy_user()

    db.session.add(user)
    db.session.commit()

    return user


##############################
########## FIXTURES ##########
##############################
@pytest.fixture
def app():
    app = application.create_app(test=True)
    with app.app_context():
        yield app


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture()
def authenticated_request(mocker):
    mocker.patch('flask_login.utils._get_user', return_value=get_dummy_user())


#############################
######## MISC. TESTS ########
#############################
def test_api_version(client):
    """
    Test if api version endpoint is reachable and validate response.
    """
    uri = prefix_configs + '/apiVersion'

    response = client.get(uri, follow_redirects=True)

    assert response.status_code == 200

    response = json.loads(response.data.decode().strip())[0]
    assert response.isdigit()


def test_endpoint_reachability_unauthorized(client):
    """
    Test reachability of all endpoints as anonymous user and check if response codes are as expected.
    """
    with client:
        # Should be reachable for anonymous users
        uri = prefix_configs + '/apiVersion'
        response = client.get(uri)
        assert response.status_code == 200

        uri = prefix_auth + '/login'
        response = client.get(uri)
        assert response.status_code == 200

        # Should NOT be reachable for anonymous users
        uri = prefix_auth + '/signup'
        response = client.get(uri)
        assert response.status_code == 302

        uri = prefix_auth + '/change_password'
        response = client.get(uri)
        assert response.status_code == 302

        uri = prefix_configs + '/uploadFile'
        response = client.get(uri)
        assert response.status_code == 302

        uri = prefix_schedules + '/add_training'
        response = client.get(uri)
        assert response.status_code == 302

        uri = prefix_schedules + '/add_prediction'
        response = client.get(uri)
        assert response.status_code == 302

        uri = prefix_schedules + '/add_stat_feat'
        response = client.get(uri)
        assert response.status_code == 302

        uri = prefix_configs + '/feature_config'
        response = client.get(uri)
        assert response.status_code == 302

        uri = prefix_configs + '/time_config'
        response = client.get(uri)
        assert response.status_code == 302

        uri = prefix_configs + '/stat_feature_config'
        response = client.get(uri)
        assert response.status_code == 302

        uri = prefix_schedules + '/argus'
        response = client.get(uri)
        assert response.status_code == 302


#############################
######## AUTH. TESTS ########
#############################
@pytest.mark.usefixtures('authenticated_request')
def test_endpoint_reachability_authorized(client):
    """
    Test reachability of all endpoints as authenticated user and check if response codes are as expected.
    """
    with client:
        # Should NOT be reachable for authenticated users
        uri = prefix_auth + '/login'
        response = client.get(uri)
        assert response.status_code == 302

        # Should be reachable for authenticated users
        uri = prefix_configs + '/apiVersion'
        response = client.get(uri)
        assert response.status_code == 200

        uri = prefix_auth + '/signup'
        response = client.get(uri)
        assert response.status_code == 200

        uri = prefix_auth + '/change_password'
        response = client.get(uri)
        assert response.status_code == 200

        uri = prefix_configs + '/uploadFile'
        response = client.get(uri)
        assert response.status_code == 200

        uri = prefix_schedules + '/add_training'
        response = client.get(uri)
        assert response.status_code == 200

        uri = prefix_schedules + '/add_prediction'
        response = client.get(uri)
        assert response.status_code == 200

        uri = prefix_schedules + '/add_stat_feat'
        response = client.get(uri)
        assert response.status_code == 200

        uri = prefix_configs + '/feature_config'
        response = client.get(uri)
        assert response.status_code == 200

        uri = prefix_configs + '/time_config'
        response = client.get(uri)
        assert response.status_code == 200

        uri = prefix_configs + '/stat_feature_config'
        response = client.get(uri)
        assert response.status_code == 200

        uri = prefix_schedules + '/argus'
        response = client.get(uri)
        assert response.status_code == 200


def test_login_logout(client):
    """
    Test if login works.
    Manually adds a user to the user db and then checks if login is successful.
    """
    # Check that there is no user in the db yet
    assert db.session.query(func.count(User.id)).scalar() == 0
    # Add user to db
    add_dummy_user_to_db()
    # Check that there is one user in the db now
    assert db.session.query(func.count(User.id)).scalar() == 1

    with client:
        # Try to login with user credentials
        uri = prefix_auth + '/login'
        response = client.post(uri, data=dict(
            email=email,
            password=password
        ), follow_redirects=True)

        # Check if login was successful
        assert response.status_code == 200
        assert current_user.name == name
        assert current_user.is_authenticated is True

        uri = prefix_auth + '/logout'
        response = client.get(uri,
                              follow_redirects=True)

        # Check if logout was successful
        assert response.status_code == 200
        assert current_user.is_anonymous is True


@pytest.mark.usefixtures('authenticated_request')
def test_sign_up_authorized(client):
    """
    Test if signup works.
    Try to create a user via POST request and then check if user is in db.
    """
    # Number of users before signup
    before_signup = db.session.query(func.count(User.id)).scalar()

    with client:
        uri = prefix_auth + '/signup'
        # Try to sign up a new user
        response = client.post(uri, data=dict(
            name="signup_user",
            email="signup@test.de",
            password="test_pass123",
            confirm="test_pass123",
            submit="Register"
        ), follow_redirects=True)

        assert response.status_code == 200

        # Number of users after signup
        after_signup = db.session.query(func.count(User.id)).scalar()
        # Now there should be one more user in the db
        assert after_signup == before_signup + 1


def test_sign_up_unauthorized(client):
    """
    Test that anonymous user can not sign up a new user.
    """
    with client:
        uri = prefix_auth + '/signup'
        # Try to sign up a new user
        response = client.post(uri, data=dict(
            name="signup_user",
            email="signup@test.de",
            password="test_pass123",
            confirm="test_pass123",
            submit="Register"
        ), follow_redirects=True)

        # Redirect to Login page
        assert request.path == prefix_auth + '/login'


##############################
######## CONFIG TESTS ########
##############################
@pytest.mark.usefixtures('authenticated_request')
def test_post_upload_single_config_file(client):
    """
    Test if upload of a single config file works.
    Create a dummy config file and try to upload it.
    """
    # Create dummy config
    filename = "dummy_config.yml"

    data = {'es_config_btn': (io.BytesIO(b'Dummy config file'), filename)}

    with client:
        uri = prefix_configs + '/uploadFile'
        # Try to sign up a new user
        response = client.post(uri,
                               data=data,
                               content_type='multipart/form-data',
                               follow_redirects=True)

        assert response.status_code == 200

        # Now the should be in the expected directory
        assert filename in os.listdir(helpers.get_es_config_dir_path())


@pytest.mark.usefixtures('authenticated_request')
def test_post_upload_multiple_config_files(client):
    """
    Test if upload of a single config file works.
    Create a dummy config file and try to upload it.
    """
    # Create dummy config
    filename_es = "dummy_es_config.yml"
    filename_feat = "dummy_feat_config.yml"
    filename_stat_feat = "dummy_stat_feat_config.yml"
    filename_time = "dummy_time_config.yml"

    data = {
        'es_config_btn': (io.BytesIO(b'Dummy es config file'), filename_es),
        'feat_config_btn': (io.BytesIO(b'Dummy feat config file'), filename_feat),
        'stat_feat_config_btn': (io.BytesIO(b'Dummy stat feat config file'), filename_stat_feat),
        'time_config_btn': (io.BytesIO(b'Dummy time config file'), filename_time)
    }

    with client:
        uri = prefix_configs + '/uploadFile'
        # Try to sign up a new user
        response = client.post(uri,
                               data=data,
                               content_type='multipart/form-data',
                               follow_redirects=True)

        assert response.status_code == 200

        # Now the should be in the expected directory
        assert filename_es in os.listdir(helpers.get_es_config_dir_path())
        assert filename_feat in os.listdir(helpers.get_feat_config_dir_path())
        assert filename_stat_feat in os.listdir(helpers.get_stat_feat_config_dir_path())
        assert filename_time in os.listdir(helpers.get_time_config_dir_path())


##############################
######## CONFIG TESTS ########
##############################
@pytest.mark.usefixtures('authenticated_request')
def test_post_upload_single_config_file(client):
    """
    Test if upload of a single config file works.
    Create a dummy config file and try to upload it.
    """
    # Create dummy config
    filename = "dummy_config.yml"

    data = {'es_config_btn': (io.BytesIO(b'Dummy config file'), filename)}

    with client:
        uri = prefix_configs + '/uploadFile'
        # Try to sign up a new user
        response = client.post(uri,
                               data=data,
                               content_type='multipart/form-data',
                               follow_redirects=True)

        assert response.status_code == 200

        # Now the should be in the expected directory
        assert filename in os.listdir(helpers.get_es_config_dir_path())
