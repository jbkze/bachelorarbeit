# Prefect Setup

#### Start the server
``$ prefect server start``

#### Start the agent
``$ prefect agent local start``

#### To create a project
``$ prefect create project "ANEMO"``

#### Prefect UI
Browser: ``localhost:8080``