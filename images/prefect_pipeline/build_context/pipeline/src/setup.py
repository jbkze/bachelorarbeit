from pkg_resources import parse_requirements
from setuptools import setup, find_packages

install_reqs = parse_requirements('requirements.txt')

setup(name="anemo", version="0.1", packages=find_packages())
