import argparse

from time import sleep

from prefect import Flow, Parameter

from src.helpers.app_context import AppContext
from src.tasks import database, processing, scaling, encoding, training, prepare, prediction, storage
from src.helpers import app_context

# Parameters
es_config_path = Parameter("es_config_path", required=True)

index = Parameter("index", required=True)

time_config_path = Parameter("time_config_path", required=True)

model_path = Parameter("model_path", required=True)

anomaly_score_field = Parameter("anomaly_score_field", required=True)

with Flow("Prediction Historical Template") as flow:
    # Override those values of the AppContext which might be different from those during training
    # -> it's important that for example the same Feature config is used during prediction but the
    # index or the elastic configuration might have changed
    flow.set_dependencies(
        app_context.initialize,
        keyword_tasks={
            "_es_config_path": es_config_path,
            "_index": index,
            "_time_config_path": time_config_path,
            "_model_path": model_path,
            "_anomaly_score_field": anomaly_score_field,
        },
    )

    # Load model and restore AppContext
    model = storage.load_model.set_upstream(app_context.initialize)

    # Prepare
    # prepare.elastic_health_check()

    # Extract
    # Make sure that the extraction starts AFTER the initialization of the AppContext has finished
    # by setting the initialization task as upstream dependency
    raw_data = database.query_historical.set_upstream(storage.load_model)

    # Processing
    processed_data = processing.init_missing_columns(raw_data)
    processed_data = processing.handle_null_values(processed_data)
    processed_data = processing.drop_bad_flows(processed_data)
    processed_data = processing.ip_mac_transformation(processed_data)

    # Scaling
    scaled_data = scaling.scale(processed_data)

    # Encoding & Dimensionality Reduction
    encoded_data = encoding.encode(scaled_data)

    # Prediction
    float_data = processing.check_float_type(encoded_data)
    predictions = prediction.predict(float_data, model)

    # Write back to elasticsearch
    database.send_codisp_to_elastic(predictions)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Historical Prediction.')

    parser.add_argument('es_config_path', default="path/to/config/elastic_config.yml")
    parser.add_argument('index', default="logstash-data-openargus-2020*")
    parser.add_argument('time_config_path', nargs='?')
    parser.add_argument('model_path', default="myModel.model")
    parser.add_argument('anomaly_score_field', default="codisp")

    args = parser.parse_args()
    args = vars(args)

    flow.run(parameters=args)
