import argparse

from time import sleep

from prefect import Flow, Parameter

from src.helpers.app_context import AppContext
from src.tasks import database, processing, scaling, encoding, training, prepare, prediction, storage
from src.helpers import app_context

# Parameters
es_config_path = Parameter("es_config_path", required=True)

stat_feat_config_path = Parameter("stat_feat_config_path", required=True)

# Source index where the "regular" flows are store
flow_index = Parameter("flow_index", required=True)

# Destination index where the aggregated flows are stored
stat_index = Parameter("stat_index", required=True)


with Flow("Statistical Features Template") as flow:
    # Make sure that parameters are loaded before initializing the AppContext
    flow.set_dependencies(
        app_context.initialize,
        keyword_tasks={
            "_es_config_path": es_config_path,
            "_flow_index": flow_index,
            "_stat_index": stat_index,
            "_stat_feature_config_path": stat_feat_config_path,
        },
    )

    # Prepare
    # prepare.elastic_health_check()

    # Extract
    # Make sure that the extraction starts AFTER the initialization of the AppContext has finished
    # by setting the initialization task as upstream dependency
    aggregated_data = database.statistical_features.set_upstream(app_context.initialize)

    # Write back to elasticsearch
    database.send_stat_feats_to_elastic(aggregated_data)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Statistical Features')

    parser.add_argument('es_config_path', default="path/to/config/elastic_config.yml")
    parser.add_argument('stat_feat_config_path', default="path/to/config/stat_feat_config.yml")
    parser.add_argument('flow_index', default="flow-index")
    parser.add_argument('stat_index', default="stat-index")

    args = parser.parse_args()
    args = vars(args)

    flow.run(parameters=args)
