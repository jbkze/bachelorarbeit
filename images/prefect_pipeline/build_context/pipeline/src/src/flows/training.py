import argparse
from time import sleep

from prefect import Flow, Parameter

from src.helpers.app_context import AppContext
from src.tasks import (
    database,
    processing,
    scaling,
    encoding,
    training,
    prepare,
    storage,
)
from src.helpers import app_context

# Parameters
es_config_path = Parameter("es_config_path", required=True)

index = Parameter("index", required=True)

ml_algorithm = Parameter("ml_algorithm", required=True)

feature_config_path = Parameter("feature_config_path", required=True)

time_config_path = Parameter("time_config_path", required=True)

model_path = Parameter("model_path", required=True)

# Flow Definition
with Flow("Training Template") as flow:
    # Make sure that parameters are loaded before initializing the AppContext
    flow.set_dependencies(
        app_context.initialize,
        keyword_tasks={
            "_es_config_path": es_config_path,
            "_index": index,
            "_ml_algorithm": ml_algorithm,
            "_feature_config_path": feature_config_path,
            "_time_config_path": time_config_path,
            "_model_path": model_path,
        },
    )

    # Prepare
    # prepare.elastic_health_check()

    # Extract
    # Make sure that the extraction starts AFTER the initialization of the AppContext has finished
    # by setting the initialization task as upstream dependency
    raw_data = database.query_historical.set_upstream(app_context.initialize)

    # Processing
    processed_data = processing.init_missing_columns(raw_data)
    processed_data = processing.handle_null_values(processed_data)
    processed_data = processing.drop_bad_flows(processed_data)
    processed_data = processing.ip_mac_transformation(processed_data)

    # Scaling
    scaled_data = scaling.scale(processed_data)

    # Encoding & Dimensionality Reduction
    encoded_data = encoding.encode(scaled_data)

    # Training
    #float_data = processing.check_float_type(encoded_data)
    model = training.train(encoded_data)

    # Save model
    storage.save_model(model)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Training.')

    parser.add_argument('es_config_path', default="path/to/config/elastic_config.yml")
    parser.add_argument('index', default="logstash-data-openargus-2020*")
    parser.add_argument('ml_algorithm', default="robust_random_cut_forest")
    parser.add_argument('feature_config_path', default="path/to/config/feature_config.yml")
    parser.add_argument('time_config_path', nargs='?')
    parser.add_argument('model_path', default="myModel.model")

    args = parser.parse_args()
    args = vars(args)

    flow.run(parameters=args)
