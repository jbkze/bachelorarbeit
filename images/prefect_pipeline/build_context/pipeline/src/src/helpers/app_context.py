import prefect
from prefect import task

from src.helpers import helpers
from src.helpers.feature_config import FeatureConfig
from src.helpers.time_config import TimeConfig
from src.helpers.stat_feature_config import StatFeatureConfig


class AppContext:
    """Make attributes accessible to all modules."""
    es_client = None
    index = None
    feature_config = None
    time_config = None
    ml_algorithm = None
    model_path = None
    anomaly_score_field = None

    # Only for statistical feature generation
    stat_feat_config = None
    flow_index = None
    stat_index = None

    scalers = {}
    encoders = {}
    dms = {}


@task
def initialize(
        _es_config_path: str = None,
        _index: str = None,
        _feature_config_path: str = None,
        _time_config_path: str = None,
        _ml_algorithm: str = None,
        _model_path: str = None,
        _anomaly_score_field: str = None,
        _stat_feature_config_path: str = None,
        _flow_index: str = None,
        _stat_index: str = None,

):
    logger = prefect.context.get("logger")

    if _es_config_path:
        logger.info(f"Load elasticsearch config from {_es_config_path}")
        es_client = helpers.get_es_client(_es_config_path)
        AppContext.es_client = es_client
    if _index:
        logger.info(f"Set index to {_index}")
        AppContext.index = _index
    if _feature_config_path:
        logger.info(f"Load feature config from {_feature_config_path}")
        feature_config = FeatureConfig(_feature_config_path)
        AppContext.feature_config = feature_config
    if _time_config_path:
        logger.info(f"Load time config from {_time_config_path}")
        time_config = TimeConfig(_time_config_path)
        AppContext.time_config = time_config
    if _ml_algorithm:
        logger.info(f"Set ml algorithm to {_ml_algorithm}")
        AppContext.ml_algorithm = _ml_algorithm
    if _model_path:
        logger.info(f"Set model path to {_model_path}")
        AppContext.model_path = _model_path
    if _anomaly_score_field:
        logger.info(f"Set anomaly score field to {_anomaly_score_field}")
        AppContext.anomaly_score_field = _anomaly_score_field

    # Only for statistical features generation
    if _stat_feature_config_path:
        logger.info(f"Load statistical feature config from {_stat_feature_config_path}")
        stat_feat_config = StatFeatureConfig(_stat_feature_config_path)
        logger.info(f"Statistical feature config: {stat_feat_config}")
        AppContext.stat_feat_config = stat_feat_config
    if _flow_index:
        logger.info(f"Set Flow Index to {_flow_index}")
        AppContext.flow_index = _flow_index
    if _stat_index:
        logger.info(f"Set Stat Index to {_stat_index}")
        AppContext.stat_index = _stat_index
