import yaml
import pandas as pd


class FeatureConfig:
    def __init__(self, config_path):
        with open(config_path, "r") as yaml_file:
            feat_list = yaml.load(yaml_file, Loader=yaml.FullLoader)

        config = pd.DataFrame.from_dict(feat_list).transpose()

        # Only keep used features
        config = config[config["use"]]

        self.used_features = list(config.index)

        self.scaled_features = config[config["scale"] != False]["scale"].T.to_dict()

        self.encoded_features = config[config["enc"] != False]["enc"].T.to_dict()

        # TODO Only a workaround for the Arbeitsmeeting, muss gefixt werden!!
        #self.dim_reduced_features = config[config["dim_red"] != False][
            #["dim_red", "vt_config", "n_components"]
        #].T.to_dict()
        self.dim_reduced_features = {}

        self.default_values = (
            config[config["default"] != None]["default"].fillna(0).T.to_dict()
        )

        self.default_values = {
            key: float(value) if str(value).isdigit() else value
            for key, value in self.default_values.items()
        }

        # TODO check that only encoded features can be dim reducted
        # TODO check that encoded features are not scaled
