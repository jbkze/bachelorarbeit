import os
import warnings
import urllib3
import yaml

from munch import Munch
from elasticsearch import Elasticsearch

warnings.simplefilter("ignore", UserWarning)
warnings.simplefilter("ignore", FutureWarning)
warnings.simplefilter("ignore", urllib3.exceptions.InsecureRequestWarning)


def get_es_client(elastic_config_path: str) -> Elasticsearch:
    with open(elastic_config_path, "r") as yml_file:
        config = yaml.load(yml_file, Loader=yaml.FullLoader)
    config = Munch.fromDict(config)

    user = config.elasticsearch.user
    pwd = config.elasticsearch.pwd
    host = config.elasticsearch.host
    port = config.elasticsearch.port

    return Elasticsearch(
        ["https://{}:{}@{}:{}".format(user, pwd, host, port)],
        use_ssl=True,
        verify_certs=False,
    )


def check_path_exists(file_path):
    if file_path is not None:
        return os.path.exists(file_path)
    else:
        return False
