import yaml
import pandas as pd


class StatFeatureConfig:
    def __init__(self, config_path):
        with open(config_path, "r") as yaml_file:
            config = yaml.load(yaml_file, Loader=yaml.FullLoader)

        # TODO validate config

        self.interval = config['interval']

        self.aggregations = []

        for bucket_name, attrs in config['stat_features'].items():
            agg_type = attrs['aggregation']['type']
            agg_field = attrs['aggregation']['field']

            self.aggregations.append(
                {
                    'bucket_name': bucket_name,
                    'interval': self.interval,
                    'agg_type': agg_type,
                    'agg_field': agg_field
                }
            )

        self.puffer = 1
        self.derived_features = {}  # feat_name -> # of derivatives
        for feature, attr in config['stat_features'].items():
            count = attr.get('derivative', 0)
            self.derived_features[feature] = int(count)
            self.puffer = count if count > self.puffer else self.puffer

        self.puffer += 2  # Just to be sure
