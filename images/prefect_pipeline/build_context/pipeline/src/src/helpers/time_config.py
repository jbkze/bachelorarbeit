import datetime
from functools import reduce

import requests
import yaml
import pandas as pd
from elasticsearch_dsl import Q

from munch import Munch


class TimeConfig:
    def __init__(self, config_path=None):
        with open(config_path, "r") as yaml_file:
            self.config = yaml.load(yaml_file, Loader=yaml.FullLoader)

        self.config = Munch.fromDict(self.config)

        self.dates = self.parse_config()

    def get_holidays(self, jahre, bundesland="HE"):
        """Returns a list of public holidays for a specific state and the special,
        company related holidays specified in the config file.
        The year(s) of interest can be specified as scalar in case of a single year,
        or as list in case of several years."""
        def get_holidays_one_year(jahr, bundesland):
            resp = requests.get(
                f"https://feiertage-api.de/api/?jahr={jahr}&nur_land={bundesland}"
            )
            if resp.status_code != 200 or resp.json() is None:
                raise Exception("No response from API")
            return [
                datetime.datetime.strptime(value["datum"], "%Y-%m-%d").date()
                for key, value in resp.json().items()
            ]

        holidays = []

        # Special Holidays
        for day in self.config.special_holidays:
            holidays.append(day)

        if isinstance(jahre, list):
            for jahr in jahre:
                holidays += get_holidays_one_year(jahr, bundesland)[:]
        else:
            holidays += get_holidays_one_year(jahre, bundesland)

        return holidays

    def parse_config(self):
        if "time_field" in self.config:
            self.timestamp_field = self.config.time_field
        else:
            self.timestamp_field = "@timestamp"

        column_names = [
            "date",
            "weekday",
            "weekend",
            "time_start",
            "time_end",
            "holiday",
            "working_day",
            "use",
        ]
        dates = pd.DataFrame(columns=column_names)

        # Parse Date
        dates["date"] = pd.date_range(
            start=self.config.date.start,
            end=self.config.date.end,
        )

        dates["weekday"] = dates["date"].dt.day_name().str.lower()
        dates["use"] = False

        # Holidays & Working Days & Weekend
        dates["holiday"] = False
        dates["working_day"] = True

        years_between_start_and_end = list(
            range(dates["date"].min().year, dates["date"].max().year + 1)
        )
        try:
            holiday_days = self.get_holidays(years_between_start_and_end)
        except:  # feiertage-api.de down
            holiday_days = []

        for day in holiday_days:
            dates.loc[dates["date"] == pd.Timestamp(day), "holiday"] = True
            dates.loc[dates["date"] == pd.Timestamp(day), "working_day"] = False

        dates.loc[dates["date"].dt.day_name() == "Saturday", "working_day"] = False
        dates.loc[dates["date"].dt.day_name() == "Sunday", "working_day"] = False

        dates["weekend"] = False
        dates.loc[dates["date"].dt.day_name() == "Saturday", "weekend"] = True
        dates.loc[dates["date"].dt.day_name() == "Sunday", "weekend"] = True

        # Global time
        start = [int(i) for i in self.config.time.start.split(":")]
        end = [int(i) for i in self.config.time.end.split(":")]
        dates["time_start"] = datetime.time(start[0], start[1], 0)
        dates["time_end"] = datetime.time(end[0], end[1], 0)

        # Weekday time (overwrites global time)
        for weekday, attr in self.config.weekdays.items():
            if attr.get("time", {}).get("start", {}):
                start = [int(i) for i in attr.time.start.split(":")]
                dates.loc[
                    dates["date"].dt.day_name() == weekday.capitalize(), "time_start"
                ] = datetime.time(start[0], start[1], 0)

            if attr.get("time", {}).get("end", {}):
                end = [int(i) for i in attr.time.end.split(":")]
                dates.loc[
                    dates["date"].dt.day_name() == weekday.capitalize(), "time_end"
                ] = datetime.time(end[0], end[1], 0)
        # Set use status
        used_weekdays = [day for day, attr in self.config.weekdays.items() if attr.use]

        dates.loc[
            (dates["weekday"].isin(used_weekdays))
            & (
                (self.config.tags.working_days and dates["working_day"])
                | (self.config.tags.holidays and dates["holiday"])
                | (self.config.tags.weekends and dates["weekend"])
            ),
            "use",
        ] = True

        # Delete all unused days
        df = dates.loc[dates["use"]]

        return df

    def to_elastic_filter(self):
        queries = []

        for _, row in self.dates.iterrows():
            from_datetime = (
                row["date"]
                .to_pydatetime()
                .replace(hour=row["time_start"].hour, minute=row["time_start"].minute)
            )
            to_datetime = (
                row["date"]
                .to_pydatetime()
                .replace(hour=row["time_end"].hour, minute=row["time_end"].minute)
            )

            queries.append(
                Q(
                    "range",
                    **{
                        f"{self.timestamp_field}": {
                            "from": f"{from_datetime.isoformat()}",
                            "to": f"{to_datetime.isoformat()}",
                            "time_zone": "Europe/Berlin",
                        }
                    },
                )
            )

        return reduce(lambda x, y: x | y, queries)
