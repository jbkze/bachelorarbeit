import pandas as pd

from pyod.models.copod import COPOD as COPOD_


def train(trainX):
    model = COPOD_()
    model.fit(trainX)

    return model


def predict(model, data):
    data = data.values
    # scores = ((model.decision_function(data) + 0.5) * -1 + 1) * 100 or:
    # scores = (-model.decision_function(data) + 0.5) * 100
    scores = (model.decision_function(data) + 0.5) * 100
    return pd.DataFrame(scores)
