import pandas as pd
import prefect

from ip2geotools.databases.noncommercial import DbIpCity


def train(trainX):
    blacklist = ["CN", "RU"]

    return blacklist


def predict(model, data):
    logger = prefect.context.get("logger")

    if not "destination.ip" in data:
        logger.warning("Destination IP not in data. Add destination.ip to your feature_list.")
        scores = [-1] * len(data)
        return pd.DataFrame(scores)

    scores = []

    for index, row in data.iterrows():
        ip = row.get("destination.ip", None)

        if ip is None:
            logger.warning("Destination IP not in row.")
            scores.append(-1)
            continue

        try:
            response = DbIpCity.get(ip, api_key='free')

            if response.country in model:
                logger.info(f"{response.country} is blacklistet!")
                scores.append(1)
            else:
                logger.info(f"{response.country} is not blacklistet!")
                scores.append(0)
        except Exception as e:
            # Internal
            logger.warning(f"{type(e)} - {ip}")
            scores.append(-2)

    return pd.DataFrame(scores)
