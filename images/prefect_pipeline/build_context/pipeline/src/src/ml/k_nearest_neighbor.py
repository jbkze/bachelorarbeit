import pandas as pd

from pyod.models.knn import KNN as KNN_


def train(trainX):
    model = KNN_()
    model.fit(trainX)

    return model


def predict(model, data):
    data = data.values
    # scores = ((model.decision_function(data) + 0.5) * -1 + 1) * 100 or:
    # scores = (-model.decision_function(data) + 0.5) * 100
    scores = model.predict_proba(data)
    scores = scores[:, 1]  # Only attack scores
    return pd.DataFrame(scores)
