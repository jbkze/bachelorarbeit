import pandas as pd
import numpy as np
import itertools


def train(trainX):
    model = trainX.values.tolist()
    model = np.concatenate(model).ravel().tolist()
    model = set(model)
    model = list(model)

    return model


def predict(model, data):
    data = data.values.tolist()
    data = np.concatenate(data).ravel().tolist()

    scores = []

    for item in data:
        print(item)
        if item in model:
            scores.append(0)
        else:
            scores.append(1)
    return pd.DataFrame(scores)
