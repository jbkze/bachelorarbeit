import numpy as np
import pandas as pd
import rrcf

from tqdm import tqdm

from pathos.helpers import mp as multiprocessing


def train(trainX):
    num_trees = 100  # Trees per training iteration
    forest = []

    X = trainX.values
    temp_forest = []
    tree_size = len(X) // 8

    print(tree_size)

    while len(temp_forest) < num_trees:
        n = len(trainX)
        ixs = np.random.choice(n, size=(n // tree_size, tree_size), replace=False)
        trees = []

        for ix in tqdm(ixs):
            # Prevent Error
            xmax = X[ix].max(axis=0)
            xmin = X[ix].min(axis=0)
            l = xmax - xmin
            if l.sum() == 0:
                continue
            try:
                trees.append(rrcf.RCTree(X[ix], index_labels=ix))
            except Exception as e:
                print(e)
                pass
        temp_forest.extend(trees)
    forest.extend(temp_forest)

    return forest


def predict(model, data, processes=0) -> pd.Series:
    codisp = mp_codisp(model, data.values, insert=False, processes=processes)
    codisp = codisp.divide(len(model[0].leaves) * 100)  # 1-100

    return codisp


def calc_codisp(tree, data, insert=False, cutoff=1):
    codisp = pd.Series(0.0, index=np.arange(len(data)))
    l = max(tree.leaves) + 1
    removed = False

    for i in range(len(data)):
        tree_codisp = pd.Series({leaf: tree.codisp(leaf) for leaf in tree.leaves})
        idx = l + i
        tree.insert_point(data[i], index=idx)
        codisp[i] = tree.codisp(idx)

        if not insert or tree.codisp(idx) < np.quantile(tree_codisp, cutoff):
            removed = True
            tree.forget_point(idx)

        if insert and not removed:
            min_idx = min(tree.leaves)
            tree.forget_point(min_idx)
    return codisp


def mp_worker(queue, forest, data, insert=False, cutoff=1):
    codisp = pd.Series(0.0, index=np.arange(len(data)))
    for tree in tqdm(forest):
        codisp += calc_codisp(tree, data, insert, cutoff)
    queue.put(codisp)


def mp_codisp(forest, data, processes=0, insert=False, cutoff=1):
    if processes == 0:
        processes = multiprocessing.cpu_count()

    queue = multiprocessing.Queue()
    chunk_size = int(np.ceil(len(forest) / processes))
    procs = []

    print(f"Processes  : {processes}")
    print(f"Forest size: {len(forest)}")
    print(f"Chunk size : {chunk_size}")

    avg_codisp = pd.Series(0.0, index=np.arange(len(data)))
    for i in range(processes):
        proc = multiprocessing.Process(
            target=mp_worker,
            args=(
                queue,
                forest[i * chunk_size : (i + 1) * chunk_size],
                data,
                insert,
                cutoff,
            ),
        )
        procs.append(proc)
        proc.start()

    for i in range(processes):
        q = queue.get()
        avg_codisp += q
    for p in procs:
        p.join()
    return avg_codisp.divide(len(forest))
