from datetime import datetime, timedelta
import socket
import prefect

import rpyc
from croniter import croniter
from prefect.schedules import Schedule
from prefect.schedules.clocks import CronClock
from prefect.environments import LocalEnvironment
from prefect.run_configs import UniversalRun

from src.flows.training import flow as training_template_flow
from src.flows.prediction_historical import flow as historical_prediction_template_flow
from src.flows.prediction_live import flow as live_prediction_template_flow
from src.flows.statistical_features import flow as statistical_features_template_flow


class PipelineService(rpyc.Service):
    def on_connect(self, conn):
        print(
            f"Connected: client with IP {socket.socket.getpeername(conn._channel.stream.sock)}"
        )

    def on_disconnect(self, conn):
        pass

    def exposed_create_training_schedule(
        self,
        task_name,
        es_config_path,
        index,
        ml_algorithm,
        feature_config_path,
        time_config_path,
        model_path,
        cron,
    ):
        """
        This is a exposed method because of the exposed_ prefix.
        This method validates the input parameters and registers the training
        flow with the given parameters and schedule.

        The client can call this method like this:

        import rpyc
        c = rpyc.connect("localhost", 18861)
        c.root.exposed_create_training_schedule(...)
        """
        print("create_training_schedule called.")

        # Validate params
        try:
            if not croniter.is_valid(cron):
                print("Invalid cron string")
                return 400, "Invalid cron string"
        except:
            print("Invalid cron string")
            return 400, "Invalid cron string"

        # TODO check if paths exist etc.
        # TODO user did not provide schedule? -> "one-time" run, without schedule

        params = {
            "es_config_path": es_config_path,
            "index": index,
            "ml_algorithm": ml_algorithm,
            "feature_config_path": feature_config_path,
            "time_config_path": time_config_path,
            "model_path": model_path,
        }

        schedule = Schedule(
            clocks=[
                CronClock(
                    cron=cron,
                    parameter_defaults=params,
                )
            ]
        )

        # Set flow name
        training_template_flow.name = task_name

        training_template_flow.schedule = schedule

        #training_template_flow.run_config = UniversalRun(labels=["label-1", "label-2"])

        training_template_flow.register(project_name="ANEMO")

        return 200, "Success"


    def exposed_create_historical_prediction_schedule(
        self,
        task_name,
        es_config_path,
        index,
        time_config_path,
        model_path,
        anomaly_score_field,
        cron
    ):
        """
        This is a exposed method because of the exposed_ prefix.
        This method validates the input parameters and registers the historical prediction
        flow with the given parameters and schedule.

        The client can call this method like this:

        import rpyc
        c = rpyc.connect("localhost", 18861)
        c.root.exposed_create_historical_prediction_schedule(...)
        """
        print("create_historical_prediction_schedule called.")

        # Validate params
        try:
            if not croniter.is_valid(cron):
                print("Invalid cron string")
                return 400, "Invalid cron string"
        except:
            print("Invalid cron string")
            return 400, "Invalid cron string"

        # TODO check if paths exist etc.
        # TODO user did not provide schedule? -> "one-time" run, without schedule

        params = {
            "es_config_path": es_config_path,
            "index": index,
            "time_config_path": time_config_path,
            "model_path": model_path,
            "anomaly_score_field": anomaly_score_field
        }

        schedule = Schedule(
            clocks=[
                CronClock(
                    cron=cron,
                    parameter_defaults=params,
                )
            ]
        )

        # Set flow name
        historical_prediction_template_flow.name = task_name

        historical_prediction_template_flow.schedule = schedule

        #historical_prediction_template_flow.environment = LocalEnvironment(labels=[])

        historical_prediction_template_flow.register(project_name="ANEMO")

        return 200, "Success"


    def exposed_create_live_prediction_schedule(
        self,
        task_name,
        es_config_path,
        index,
        model_path,
        anomaly_score_field,
        cron
    ):
        """
        This is a exposed method because of the exposed_ prefix.
        This method validates the input parameters and registers the live prediction
        flow with the given parameters and schedule.

        The client can call this method like this:

        import rpyc
        c = rpyc.connect("localhost", 18861)
        c.root.exposed_create_live_prediction_schedule(...)
        """
        print("create_live_prediction_schedule called.")

        # Validate params
        try:
            if not croniter.is_valid(cron):
                print("Invalid cron string")
                return 400, "Invalid cron string"
        except:
            print("Invalid cron string")
            return 400, "Invalid cron string"

        # TODO check if paths exist etc.
        # TODO user did not provide schedule? -> "one-time" run, without schedule

        params = {
            "es_config_path": es_config_path,
            "index": index,
            "model_path": model_path,
            "anomaly_score_field": anomaly_score_field
        }

        schedule = Schedule(
            clocks=[
                CronClock(
                    cron=cron,
                    parameter_defaults=params,
                )
            ]
        )

        # Set flow name
        live_prediction_template_flow.name = task_name

        live_prediction_template_flow.schedule = schedule

        #live_prediction_template_flow.environment = LocalEnvironment(labels=[])

        live_prediction_template_flow.register(project_name="ANEMO")

        return 200, "Success"


    def exposed_create_statistical_features_schedule(
        self,
        task_name,
        es_config_path,
        flow_index,
        stat_index,
        stat_feat_config_path,
        cron
    ):
        """
        This is a exposed method because of the exposed_ prefix.
        This method validates the input parameters and registers the statistical
        features flow with the given parameters and schedule.

        The client can call this method like this:

        import rpyc
        c = rpyc.connect("localhost", 18861)
        c.root.exposed_create_statistical_features_schedule(...)
        """
        print("create_statistical_features_schedule called.")

        # Validate params
        try:
            if not croniter.is_valid(cron):
                print("Invalid cron string")
                return 400, "Invalid cron string"
        except:
            print("Invalid cron string")
            return 400, "Invalid cron string"

        # TODO check if paths exist etc.
        # TODO user did not provide schedule? -> "one-time" run, without schedule

        params = {
            "es_config_path": es_config_path,
            "flow_index": flow_index,
            "stat_index": stat_index,
            "stat_feat_config_path": stat_feat_config_path
        }

        schedule = Schedule(
            clocks=[
                CronClock(
                    cron=cron,
                    parameter_defaults=params,
                )
            ]
        )

        # Set flow name
        statistical_features_template_flow.name = task_name

        statistical_features_template_flow.schedule = schedule

        statistical_features_template_flow.register(project_name="ANEMO")

        return 200, "Success"


if __name__ == "__main__":
    from rpyc.utils.server import ThreadedServer

    RPYC_PORT = 18861

    print(f"IP  : {socket.gethostbyname(socket.gethostname())}")
    print(f"Port: {RPYC_PORT}\n")
    print("Start Pipeline Service.")

    # Avoid default host machine label
    prefect.config.flows.defaults.storage.add_default_labels = False

    t = ThreadedServer(PipelineService, port=18861)
    t.start()
