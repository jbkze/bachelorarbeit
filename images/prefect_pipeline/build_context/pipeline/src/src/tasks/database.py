# Basic imports
from time import sleep

import eland as eland
import pandas as pd
import prefect
import re

from prefect import task
from elasticsearch_dsl import Search
from datetime import timedelta

# User imports
from src.helpers.app_context import AppContext


@task()
def query_historical() -> pd.DataFrame:
    """
    Fetches historical data from elasticsearch depending on
    the given time range config.

    Returns:
        Unprocessed dataframe with the raw results of the query.
    """
    logger = prefect.context.get("logger")

    if AppContext.es_client is None:
        logger.error(f"Elasticsearch client is None!")

    s = Search(using=AppContext.es_client, index=AppContext.index)

    if AppContext.feature_config is not None:
        s = s.source(AppContext.feature_config.used_features)
    else:
        logger.warning("Feature Config is None! As a result all features are loaded.")

    if AppContext.time_config is not None:
        s = s.query(AppContext.time_config.to_elastic_filter())
        logger.info(f"Number of records: {s.count()}")
    else:
        logger.warning(
            "Time Config is None! As a result the complete available time range is loaded which might lead "
            "to high memory consumption."
        )
        logger.info(f"Number of records: {s.count()}")

    if s.count() == 0:
        logger.error(f"Number of records is 0. Either no entries lie in the specified time range or you're lacking a @timestamp field in your data.")

    df = pd.DataFrame(
        [
            {**d.to_dict(), **{"id": d.meta["index"] + "#" + d.meta["id"]}}
            for d in s.scan()
        ]
    )

    if df is not None:
        df = df.set_index('id', drop=True)

    return df


@task()
def query_latest() -> pd.DataFrame:
    """
    Gets the latest entries from the database, i.e. those added after latest_timestamp

    Returns:
        Unprocessed dataframe with the raw results of the query.
    """
    logger = prefect.context.get("logger")

    if AppContext.es_client is not None:
        logger.info(f"Using elasticsearch client: {AppContext.es_client}")
    else:
        logger.error(f"Elasticsearch client is None!")

    s = Search(using=AppContext.es_client, index=AppContext.index)

    if AppContext.feature_config is not None:
        s = s.source(AppContext.feature_config.used_features)
    else:
        logger.warning("Feature Config is None! As a result all features are loaded.")

    # Find latest timestamp
    proxy_df = eland.DataFrame(es_client=AppContext.es_client,
                               es_index_pattern=AppContext.index)

    try:
        # All rows for which a score has been calculated
        all_predicted_rows = proxy_df.query(f'{AppContext.anomaly_score_field}.notnull()')
        # The timestamp of the latest of these records
        latest_timestamp = all_predicted_rows['@timestamp'].max()
        # Puffer 1min
        latest_timestamp = pd.Timestamp(latest_timestamp - timedelta(seconds=60))
    except:
        logger.info(f"There is no anomaly score of this name yet")
        # If no codisp exists yet
        latest_timestamp = proxy_df['@timestamp'].max()
        # Puffer 2min
        latest_timestamp = pd.Timestamp(latest_timestamp - timedelta(seconds=120))

    logger.info(f"Latest Timestamp: {latest_timestamp.tz_localize('Europe/Berlin')}")

    # Get records since latest_timstamp
    s = s.query('range', **{'@timestamp': {"from": f'{latest_timestamp.isoformat()}',
                                           "to": f'now',
                                           }})

    logger.info(f"Number of records: {s.count()}")

    df = pd.DataFrame(
        [
            {**d.to_dict(), **{"id": d.meta["index"] + "#" + d.meta["id"]}}
            for d in s.scan()
        ]
    )

    if df is not None:
        df = df.set_index('id', drop=True)

    return df


@task()
def statistical_features():
    # Find latest timestamp
    logger = prefect.context.get("logger")

    try:
        proxy_df = eland.DataFrame(es_client=AppContext.es_client,
                                   es_index_pattern=AppContext.index)
        # The timestamp of the latest record
        latest_timestamp = all_predicted_rows['@timestamp'].max()
        latest_timestamp = datetime.utcfromtimestamp(ts / 1e3)
    except:
        latest_timestamp = None

    interval_s_mapping = {
        'minute': 60,
        'hour': 60 * 60,
        'day': 24 * 60 * 60,
        'week': 7 * 24 * 60 * 60,
        'month': 30 * 24 * 60 * 60
    }

    s = Search(using=AppContext.es_client, index=AppContext.flow_index)

    puffer_in_sec = AppContext.stat_feat_config.puffer * interval_s_mapping[AppContext.stat_feat_config.interval]

    puffer_in_sec = puffer_in_sec * 30

    logger.info(f"Buffer: {puffer_in_sec}")

    if latest_timestamp is None:
        from_time = f"now-{puffer_in_sec}s/m"
    else:
        latest_timestamp -= timedelta(seconds=puffer_in_sec)
        from_time = latest_timestamp.isoformat()

    logger.info(f"From Time: {from_time}")

    s = s.query('range', **{'@timestamp': {"gte": f'{from_time}',
                                            "lt": f'now/m',
                                            }})

    for aggregation in AppContext.stat_feat_config.aggregations:
        s.aggs.bucket(name=aggregation['bucket_name'],
                      agg_type='date_histogram',
                      field='@timestamp',
                      calendar_interval=AppContext.stat_feat_config.interval) \
              .metric(name=aggregation['bucket_name'],
                      agg_type=aggregation['agg_type'],
                      field=aggregation['agg_field'])

    results = s.execute().aggs

    buckets = [aggregation['bucket_name'] for aggregation in AppContext.stat_feat_config.aggregations]

    # Convert aggregations to dataframe
    data = {}
    for bucket in buckets:
        item = results[bucket].buckets._l_
        values = {}
        for i in item:
            values[i['key_as_string']] = i[bucket]['value']
        data[bucket] = values

    dataframe = pd.DataFrame(data)

    # Derivates
    for feat_name, count in AppContext.stat_feat_config.derived_features.items():
        src_feat_name = feat_name
        for i in range(1, count+1):
            if "_derivative" in src_feat_name:
                dst_feat_name = re.sub("\d", str(i), src_feat_name)
            else:
                dst_feat_name = feat_name + "_" + str(i) + "_derivative"

            dataframe[dst_feat_name] = dataframe[src_feat_name].diff(periods=1)
            src_feat_name = dst_feat_name

    logger.info(f"Stat Features: {dataframe}")

    dataframe = dataframe.dropna()

    return dataframe


@task()
def send_codisp_to_elastic(df):
    # TODO there might be a more efficient way to do this
    for index_id, row in df.iterrows():
        index_name = index_id.split("#")[0]
        id = index_id.split("#")[1]
        AppContext.es_client.update(
            index=index_name,
            id=id,
            body={"doc": {AppContext.anomaly_score_field: row["anomaly_score"]}},
            refresh=True,
        )

@task()
def send_stat_feats_to_elastic(df):
    df = df.reset_index()
    df = df.rename(columns={"index": "@timestamp"})
    for id, row in df.iterrows():
        if not AppContext.es_client.exists(index=AppContext.stat_index, id=row['@timestamp']):
            body = row.to_dict()
            AppContext.es_client.index(index=AppContext.stat_index, id=row['@timestamp'], body=body)
