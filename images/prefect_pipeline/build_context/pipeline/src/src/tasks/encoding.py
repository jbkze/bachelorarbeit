import hashlib

import pandas as pd

from prefect import task
from sklearn.decomposition import PCA
from sklearn.feature_selection import VarianceThreshold
from sklearn.preprocessing import OneHotEncoder

from src.helpers.app_context import AppContext


@task()
def encode(df: pd.DataFrame) -> pd.DataFrame:
    """Wrapper function for the encoding step."""
    for feature, encoder in AppContext.feature_config.encoded_features.items():
        if feature in df:
            dimension_reduction = AppContext.feature_config.dim_reduced_features.get(
                feature, {}
            ).get("dim_red", None)

            n_components = AppContext.feature_config.dim_reduced_features.get(
                feature, {}
            ).get("n_components", 8)

            variance_threshold = AppContext.feature_config.dim_reduced_features.get(
                feature, {}
            ).get("vt_config", 0.9)

            # Separate column of interest
            column = df.loc[:, [feature]]
            # Rest of the df without the column of interest
            everything_but_column = df.loc[
                :, [x for x in df.columns.values if x not in [feature]]
            ]

            if encoder == "ohe":
                column = _one_hot_encode(column, feature)
            if encoder == "hash":
                column = _hash_encode(column, feature)

            if dimension_reduction == "pca":
                try:
                    column = _pca(column, feature, n_components)
                except Exception as e:
                    i = 0
            if dimension_reduction == "vt":
                column = _variance_threshold(column, feature, variance_threshold)

            df = pd.concat([column, everything_but_column], axis=1)

    return df


def _hash_encode(column, feature):
    #hash_bits = column[feature].nunique()
    hash_bits = 128

    def hash_fn(x):
        tmp = [0 for _ in range(hash_bits)]
        for val in x.values:
            if val is not None:
                hasher = hashlib.new("sha1")
                hasher.update(bytes(str(val), "utf-8"))
                tmp[int(hasher.hexdigest(), 16) % hash_bits] += 1
        return pd.Series(tmp, index=new_cols)

    new_cols = [f"{feature}_{d}" for d in range(hash_bits)]

    column = column.apply(hash_fn, axis=1)

    return column


def _one_hot_encode(column, feature):
    """
    One-hot encodes the given column. Since one-hot encoding
    assumes a finite feature space and the category names are defined at
    training time, new categories (e.g. new IP addresses) will be ignored.
    """
    save_index = list(column.index)
    column = column[feature].values.reshape(-1, 1)

    if feature not in AppContext.encoders:
        ohe_config = {"categories": "auto", "handle_unknown": "ignore"}
        AppContext.encoders[feature] = OneHotEncoder(**ohe_config)
        column = AppContext.encoders[feature].fit_transform(column).toarray()
    else:
        column = AppContext.encoders[feature].transform(column).toarray()

    new_cols = [f"{feature}_{d}" for d in range(len(column.T))]

    column = pd.DataFrame(data=column, index=save_index, columns=new_cols)

    return column


def _pca(column, feature, n_components):
    save_index = column.index
    # Check if n_components is too big
    n_components = (
        len(column.T) if int(n_components) > len(column.T) else int(n_components)
    )
    new_cols = [f"{feature}_{d}" for d in range(n_components)]

    if feature not in AppContext.dms:
        pca_config = {"n_components": int(n_components)}
        AppContext.dms[feature] = PCA(**pca_config)
        column = AppContext.dms[feature].fit_transform(column)
    else:
        column = AppContext.dms[feature].transform(column)

    column = pd.DataFrame(data=column, index=save_index, columns=new_cols)

    return column


def _variance_threshold(column, feature, variance_threshold):
    save_index = column.index

    if feature not in AppContext.dms:
        vt_config = {"threshold": variance_threshold}
        AppContext.dms[feature] = VarianceThreshold(**vt_config)
        column = AppContext.dms[feature].fit_transform(column)
    else:
        column = AppContext.dms[feature].transform(column)

    new_cols = [f"{feature}_{d}" for d in range(len(column.T))]
    column = pd.DataFrame(data=column, index=save_index, columns=new_cols)

    return column
