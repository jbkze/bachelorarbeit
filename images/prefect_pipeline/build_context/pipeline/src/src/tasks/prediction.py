import pandas as pd

from prefect import task

from src.helpers.app_context import AppContext
from src.ml import (
    angle_based,
    clustering_based,
    connectivity_based,
    copula_based,
    deviation_based,
    histogram_based,
    isolation_forest,
    k_nearest_neighbor,
    local_correlation_integral,
    local_outlier_factor,
    locally_selective_combination,
    loda,
    median_absolute_deviation,
    minimum_covariance_determinant,
    one_class_svm,
    robust_random_cut_forest,
    stochastic_outlier_selection,
    subspace_outlier_detection,
    geo_fence,
    novelty_detection
)


@task()
def predict(df: pd.DataFrame, model) -> pd.DataFrame:
    options = {
        "angle_based": angle_based,
        "clustering_based": clustering_based,
        "connectivity_based": connectivity_based,
        "copula_based": copula_based,
        "deviation_based": deviation_based,
        "histogram_based": histogram_based,
        "isolation_forest": isolation_forest,
        "k_nearest_neighbor": k_nearest_neighbor,
        "local_correlation_integral": local_correlation_integral,
        "local_outlier_factor": local_outlier_factor,
        "locally_selective_combination": locally_selective_combination,
        "loda": loda,
        "median_absolute_deviation": median_absolute_deviation,
        "minimum_covariance_determinant": minimum_covariance_determinant,
        "one_class_svm": one_class_svm,
        "robust_random_cut_forest": robust_random_cut_forest,
        "stochastic_outlier_selection": stochastic_outlier_selection,
        "subspace_outlier_detection": subspace_outlier_detection,
        "geo_fence": geo_fence,
        "novelty_detection": novelty_detection
    }

    anomaly_score = options[AppContext.ml_algorithm].predict(model=model, data=df)

    df["anomaly_score"] = anomaly_score.values

    return df
