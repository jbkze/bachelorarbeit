from datetime import timedelta

from prefect import task

from src.helpers.app_context import AppContext


@task(max_retries=3, retry_delay=timedelta(seconds=3))
def elastic_health_check():
    es_client = AppContext.es_client
    try:
        return "green" in es_client.cat.health()
    except ConnectionError:
        return False
