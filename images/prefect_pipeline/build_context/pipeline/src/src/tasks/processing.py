import ipaddress
import pandas as pd

from prefect import task

from src.helpers.app_context import AppContext


@task()
def init_missing_columns(df: pd.DataFrame) -> pd.DataFrame:
    """Initialize all missing columns in the dataframe."""
    expected_columns = AppContext.feature_config.used_features

    for column in expected_columns:
        if column not in df:
            df[column] = None
    return df


@task()
def handle_null_values(df: pd.DataFrame) -> pd.DataFrame:
    """Replace null values with default values."""
    for feature, default in AppContext.feature_config.default_values.items():
        if feature in df:
            df[feature].fillna(default, inplace=True)

    # If no default value is specified, use 0
    df = df.fillna(0)

    return df


@task()
def drop_bad_flows(df: pd.DataFrame) -> pd.DataFrame:
    """Drop flows with measurement errors i.e. no IP address."""
    if "destination.ip" in df:
        df = df.dropna(subset=["destination.ip"])
    if "source.ip" in df:
        df = df.dropna(subset=["source.ip"])
    return df


@task()
def check_float_type(df: pd.DataFrame) -> pd.DataFrame:
    """Check that only type is float64"""
    df = df.astype('float64')
    #float_col = df.select_dtypes(exclude=['float64'])
    #for col in float_col.columns.values:
    #    print(col)
    #    df[col] = df[col].astype('float')
    return df
    

@task()
def ip_mac_transformation(df: pd.DataFrame) -> pd.DataFrame:
    """Make sure that all IP and MAC addresses have the same format."""

    def transform_ip(ip: str) -> str:
        try:
            ip = ipaddress.ip_address(ip)
        except ValueError:
            return ip
        if ip.version == 4:
            return str(ip.exploded).replace(".", "")
        if ip.version == 6:
            return str(ip.exploded).replace(":", "")

    def transform_mac(mac: str) -> str:
        try:
            if mac.capitalize() == "Broadcast":
                return "ffffffffffff"
        except:
            return "ffffffffffff"
        groups = mac.split(":")
        equalized_mac = "".join(map(lambda group: group.zfill(2), groups))
        return equalized_mac.lower()

    if "destination.ip" in df:
        df["destination.ip"] = df["destination.ip"].apply(transform_ip)
    if "source.ip" in df:
        df["source.ip"] = df["source.ip"].apply(transform_ip)
    if "destination.mac" in df:
        df["destination.mac"] = df["destination.mac"].apply(transform_mac)
    if "source.mac" in df:
        df["source.mac"] = df["source.mac"].apply(transform_mac)
    return df
