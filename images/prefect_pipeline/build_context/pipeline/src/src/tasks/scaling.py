import pandas as pd

from prefect import task
from sklearn.preprocessing import MinMaxScaler, MaxAbsScaler, StandardScaler

from src.helpers.app_context import AppContext


@task()
def scale(df: pd.DataFrame) -> pd.DataFrame:
    """Scale features with given type of scaler."""
    options = {"min_max": MinMaxScaler, "max_abs": MaxAbsScaler, "std": StandardScaler}

    for column, scaler_type in AppContext.feature_config.scaled_features.items():
        # Check if a scaler for that feature exists. If so, use that one...
        if column in AppContext.scalers:
            df[column] = AppContext.scalers[column].transform(
                df[column].values.reshape(-1, 1)
            )
        # otherwise create a new scaler of given type
        else:
            scaler = options.get(scaler_type, StandardScaler)()
            scaler = scaler.fit(df[column].values.reshape(-1, 1))
            df[column] = scaler.transform(df[column].values.reshape(-1, 1))
            # Save scaler (for prediction)
            AppContext.scalers[column] = scaler

    return df
