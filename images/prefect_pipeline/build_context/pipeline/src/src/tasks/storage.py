import dill
import prefect
import os

from hurry.filesize import size

from prefect import task

from src.helpers.app_context import AppContext


@task()
def save_df(df):
    df.to_pickle(AppContext.anomaly_score_field + ".df")
    return df


@task()
def save_model(model):
    """Save model and the relevant attributes of the AppContext."""
    logger = prefect.context.get("logger")

    to_pickle = {
        "model": model,
        "feature_config": AppContext.feature_config,
        "ml_algorithm": AppContext.ml_algorithm,
        "scalers": AppContext.scalers,
        "encoders": AppContext.encoders,
        "dms": AppContext.dms,
    }

    with open(AppContext.model_path, "ab") as dest:
        dill.dump(to_pickle, dest)

    filesize = size(os.path.getsize(AppContext.model_path))

    logger.info(f"Size of saved model: {filesize}")


@task()
def load_model():
    """
    Load the trained model and those attributes of the AppContext
    that must be the same during training and prediction.
    """
    with open(AppContext.model_path, "rb") as src:
        from_pickle = dill.load(src)

        AppContext.feature_config = from_pickle["feature_config"]
        AppContext.ml_algorithm = from_pickle["ml_algorithm"]
        AppContext.scalers = from_pickle["scalers"]
        AppContext.encoders = from_pickle["encoders"]
        AppContext.dms = from_pickle["dms"]

        model = from_pickle["model"]

        return model
