#!/bin/bash
# Create default tenant 
docker run -it --network prefect-server anemo_pipeline prefect server create-tenant --name default --slug default
# Create ANEMO project
docker run -it --network prefect-server anemo_pipeline prefect create project ANEMO
